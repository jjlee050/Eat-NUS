package com.eatnus.ui.home.customer.history_order.contract

import android.support.v7.widget.RecyclerView
import android.widget.TextView
import com.eatnus.model.Food
import com.eatnus.model.FoodBasket
import com.eatnus.model.Order
import java.util.HashMap

interface CustomerHistoryOrderPresenterContract {
    fun showPastOrders(currentOrderRecyclerView : RecyclerView): MutableList<Triple<Order, HashMap<Food, FoodBasket>, Pair<String, Float>>>
    fun retrieveMyBasket(basketID: Int, recyclerView: RecyclerView): Map<Food, FoodBasket>
}