package com.eatnus.ui.home.vendor.processing_order.contract

import android.support.v7.widget.RecyclerView
import android.widget.TextView
import com.eatnus.model.Food
import com.eatnus.model.FoodBasket
import com.eatnus.model.Order
import com.eatnus.ui.home.vendor.processing_order.adapter.VendorProcessingOrderAdapter
import java.util.*

//VendorIncomingOrderPresenterContract.kt
interface VendorProcessingOrderPresenterContract {
    fun showProcessingOrders(processingOrderRecyclerView : RecyclerView): MutableList<Triple<Order, HashMap<Food, FoodBasket>, Pair<String, Float>>>
    fun retrieveMyBasket(basketID: Int, recyclerView: RecyclerView): Map<Food, FoodBasket>
    fun doneProcessingOrder(order: Order)
}