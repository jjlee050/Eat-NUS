package com.eatnus.ui.home.vendor.processed_order.adapter

import android.support.constraint.ConstraintLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SimpleItemAnimator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.eatnus.R
import com.eatnus.model.Food
import com.eatnus.model.FoodBasket
import com.eatnus.model.Order
import com.eatnus.ui.home.vendor.processed_order.VendorProcessedOrderFragment
import com.eatnus.ui.home.vendor.processed_order.contract.VendorProcessedOrderPresenterContract
import com.eatnus.ui.order_food.confirm_order.adapter.BasketAdapter
import com.eatnus.utils.ui.MyProgressDialog
import java.text.DecimalFormat
import java.util.*

//VendorProcessedOrderAdapter.kt
class VendorProcessedOrderAdapter(private val items: MutableList<Triple<Order, HashMap<Food, FoodBasket>, Pair<String, Float>>>, private val fragment: VendorProcessedOrderFragment, private val incomingOrderActivity: AppCompatActivity, private val presenter: VendorProcessedOrderPresenterContract) : RecyclerView.Adapter<ViewHolder>() {
    private var isExpanded = false

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val decimalFormat = DecimalFormat("0.00")
        val order = items[position].first

        if(!order.isCash)
            holder.isCashImageView.visibility = View.INVISIBLE
        else
            holder.isCashImageView.visibility = View.VISIBLE

        holder.usernameTextView.text = items[position].third.first
        holder.priceTextView.text = "$ ${decimalFormat.format(items[position].third.second.toBigDecimal())}"
        holder.itemView.setOnClickListener{
            holder.orderIDTextView.text = "Order #${order.orderID}"
            (holder.myBasketRecyclerView.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false

            holder.myBasketRecyclerView.layoutManager = LinearLayoutManager(incomingOrderActivity, LinearLayoutManager.VERTICAL, false)
            holder.myBasketRecyclerView.adapter = BasketAdapter(items[position].second, fragment.context)

            if(!isExpanded) {
                isExpanded = true
                holder.basketLayout.visibility = View.VISIBLE
            } else {
                isExpanded = false
                holder.basketLayout.visibility = View.GONE
            }
            if(order.isTakeaway) {
                holder.takeawayTextView.text = "Takeaway: + ($${decimalFormat.format(order.takeawayFee.toBigDecimal())})"
            } else {
                holder.subLayout.visibility = View.GONE
                (holder.myBasketRecyclerView.layoutParams as LinearLayout.LayoutParams).bottomMargin = 16
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(fragment.context).inflate(R.layout.list_item_vendor_processed_order, parent, false)
        return ViewHolder(view)
    }

    fun orderCollected(position: Int){
        val order = items[position]
        items.remove(order)
        presenter.collectedOrder(order.first)
        notifyItemRemoved(position)
    }
}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view){
    // Holds the TextView that will add each animal to
    val mainLayout: ConstraintLayout = view.findViewById(R.id.mainLayout)
    val usernameTextView: TextView = view.findViewById(R.id.usernameTextView)
    val priceTextView: TextView = view.findViewById(R.id.priceTextView)
    val basketLayout: LinearLayout = view.findViewById(R.id.basketLayout)
    val subLayout: LinearLayout = view.findViewById(R.id.subLayout)
    val orderIDTextView: TextView = view.findViewById(R.id.orderIDTextView)
    val myBasketRecyclerView: RecyclerView = view.findViewById(R.id.myBasketRecyclerView)
    val takeawayTextView: TextView = view.findViewById(R.id.takeawayTextView)
    val isCashImageView: ImageView = view.findViewById(R.id.isCashImageView)
}