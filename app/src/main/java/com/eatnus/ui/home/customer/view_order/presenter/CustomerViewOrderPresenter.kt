package com.eatnus.ui.home.customer.view_order.presenter

import android.support.v7.widget.RecyclerView
import com.eatnus.model.Food
import com.eatnus.model.FoodBasket
import com.eatnus.model.Order
import com.eatnus.ui.home.customer.view_order.CustomerViewOrderActivity
import com.eatnus.ui.home.customer.view_order.contract.CustomerViewOrderPresenterContract
import com.eatnus.utils.OrderStatus
import com.eatnus.utils.api.EatNUSAPI
import com.eatnus.utils.helper.AppPreferencesHelper
import com.eatnus.utils.notification.EatNUSNotificationManager
import com.google.android.gms.tasks.Tasks
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import java.text.SimpleDateFormat
import java.util.*
import android.app.PendingIntent
import android.content.Intent
import com.eatnus.ui.reward.RewardActivity


class CustomerViewOrderPresenter (private val view: CustomerViewOrderActivity) : CustomerViewOrderPresenterContract {
  override fun acknowledgeOrder(order: Order, pts: Float) {
    val userDBSource = EatNUSAPI.getUser(AppPreferencesHelper().getUsername()!!)
    val lvlDBSource = EatNUSAPI.getLevels()
    val task = Tasks.whenAll(userDBSource, lvlDBSource)

    task.addOnSuccessListener {
      val userData = userDBSource.result
      val lvlData = lvlDBSource.result
      EatNUSAPI.getOrderRef(order.orderID.toString()).child("status")
        .setValue(OrderStatus.ORDER_ACKNOWLEDGED)
      //Change order ready time
      val dateFormat = SimpleDateFormat("HH:mm")
      val calendar = Calendar.getInstance()
      calendar.timeZone = TimeZone.getTimeZone("Asia/Singapore")

      EatNUSAPI.getOrderRef(order.orderID.toString()).child("collectedTime")
        .setValue(dateFormat.format(calendar.time))

      //Level up algo
      val userPts = userData.child("currentPts").value.toString().toInt()
      val userLvl = userData.child("currentLevel").value.toString().toInt()
      val totalUserPts = userPts + pts
      var isLvlUp = false
      userData.child("currentPts").ref.setValue(totalUserPts)
      lvlData.children.forEach {
        val lvl = it.key.toString().toInt()
        if (userLvl <= it.key.toString().toInt()) {
          val maxPts = it.child("maxPts").value.toString().toInt()
          val minPts = it.child("minPts").value.toString().toInt()
          if (totalUserPts >= minPts) {
            userData.child("currentLevel").ref.setValue(lvl)
            isLvlUp = true
          }
        }
      }

      if (isLvlUp) {
        val intent = Intent(view, RewardActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        val pendingIntent = PendingIntent.getActivity(view, 0, intent, 0)

        val mBuilder = EatNUSNotificationManager().createNotification(
          view,
          "0",
          "LEVEL UP!",
          "Congratulations, you have level up. Click to view more rewards.",
          "", pendingIntent, false,
          false
        )

        EatNUSNotificationManager().postNotifications(
          view,
          0,
          mBuilder.build()
        )
      }

      EatNUSAPI.getOrderRef(order.orderID.toString()).child("status")
        .setValue(OrderStatus.ORDER_SUCCESSFUL)
    }
  }

  override fun retrieveMyBasket(basketID: Int, recyclerView: RecyclerView): Map<Food, FoodBasket> {
    val basketMap = HashMap<Food, FoodBasket>()
    EatNUSAPI.getFoodBasketRef(basketID.toString())
      .addListenerForSingleValueEvent(object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) {
        }

        override fun onDataChange(p0: DataSnapshot) {
          p0.children.forEach {
            val foodBasket = FoodBasket()
            foodBasket.qty = it.child("qty").value.toString().toInt()
            foodBasket.foodID = it.child("foodID").value.toString().toInt()
            foodBasket.price = it.child("price").value.toString().toFloat()
            EatNUSAPI.getFoodRef(foodBasket.foodID.toString())
              .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                }

                override fun onDataChange(p0: DataSnapshot) {
                  val food = Food()
                  food.name = p0.child("name").value.toString()
                  basketMap.put(food, foodBasket)
                  recyclerView.adapter.notifyDataSetChanged()
                }
              })
          }
        }
      })
    return basketMap
  }
}