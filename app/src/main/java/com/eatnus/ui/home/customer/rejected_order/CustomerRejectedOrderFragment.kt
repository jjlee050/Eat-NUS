package com.eatnus.ui.home.customer.rejected_order

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ClipDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eatnus.R
import com.eatnus.ui.home.customer.rejected_order.adapter.CustomerRejectedOrderAdapter
import com.eatnus.ui.home.customer.rejected_order.contract.CustomerRejectedOrderPresenterContract
import com.eatnus.ui.home.customer.rejected_order.presenter.CustomerRejectedOrderPresenter
import com.eatnus.utils.ui.MyProgressDialog

class CustomerRejectedOrderFragment: Fragment() {
    private lateinit var customerRejectedOrderPresenter: CustomerRejectedOrderPresenterContract
    private var homeActivity: AppCompatActivity? = null
    private lateinit var rejectedOrderRecyclerView: RecyclerView
    private lateinit var emptyLayout: View
    private lateinit var loadDialog: MyProgressDialog

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is AppCompatActivity)
            this.homeActivity = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_customer_rejected_order, container, false)
    }

    override fun onViewCreated(rootView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(rootView, savedInstanceState)

        rejectedOrderRecyclerView = rootView.findViewById(R.id.rejectedOrderRecyclerView)
        val swipeRefreshLayout = rootView.findViewById<SwipeRefreshLayout>(R.id.swipeRefreshLayout)
        val itemDecor = DividerItemDecoration(activity?.applicationContext, ClipDrawable.HORIZONTAL)

        loadDialog = MyProgressDialog(homeActivity!!, rootView.findViewById(R.id.newton_cradle_loading))
        loadDialog.start()

        customerRejectedOrderPresenter = CustomerRejectedOrderPresenter(homeActivity!!, this@CustomerRejectedOrderFragment, loadDialog)

        rejectedOrderRecyclerView.addItemDecoration(itemDecor)
        rejectedOrderRecyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        rejectedOrderRecyclerView.adapter = CustomerRejectedOrderAdapter(customerRejectedOrderPresenter.showRejectedOrders(rejectedOrderRecyclerView), this@CustomerRejectedOrderFragment, homeActivity!!)

        emptyLayout = rootView.findViewById<View>(R.id.emptyLayout)
        closeViews()

        swipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE)
        swipeRefreshLayout.setOnRefreshListener {
            loadDialog.start()
            closeViews()
            rejectedOrderRecyclerView.adapter = CustomerRejectedOrderAdapter(customerRejectedOrderPresenter.showRejectedOrders(rejectedOrderRecyclerView), this@CustomerRejectedOrderFragment, homeActivity!!)
            swipeRefreshLayout.isRefreshing = false
        }
    }

    override fun onDetach() {
        super.onDetach()
        this.homeActivity = null
    }

    private fun closeViews() {
        if(emptyLayout.visibility == View.VISIBLE) {
            emptyLayout.visibility = View.GONE
        }
        if(rejectedOrderRecyclerView.visibility == View.VISIBLE) {
            rejectedOrderRecyclerView.visibility = View.GONE
        }
    }

    fun isListEmpty(isEmpty: Boolean){
        if(isEmpty){
            emptyLayout.visibility = View.VISIBLE
            rejectedOrderRecyclerView.visibility = View.GONE
        } else {
            emptyLayout.visibility = View.GONE
            rejectedOrderRecyclerView.visibility = View.VISIBLE
        }
    }
}