package com.eatnus.ui.home

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.design.internal.BottomNavigationItemView
import android.support.design.internal.BottomNavigationMenuView
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eatnus.R
import com.eatnus.ui.home.customer.current_order.CustomerCurrentOrderFragment
import com.eatnus.ui.home.customer.history_order.CustomerHistoryOrderFragment
import com.eatnus.ui.home.customer.rejected_order.CustomerRejectedOrderFragment
import com.eatnus.ui.home.vendor.history_order.VendorHistoryOrderFragment
import com.eatnus.ui.home.vendor.incoming_order.VendorIncomingOrderFragment
import com.eatnus.ui.home.vendor.processed_order.VendorProcessedOrderFragment
import com.eatnus.ui.home.vendor.processing_order.VendorProcessingOrderFragment
import com.eatnus.utils.helper.AppPreferencesHelper

class HomeFragment : Fragment() {
  private var homeActivity: AppCompatActivity? = null

  override fun onAttach(context: Context?) {
    super.onAttach(context)
    if (context is AppCompatActivity)
      homeActivity = context
  }

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
  ): View? {
    return inflater.inflate(R.layout.fragment_home_page,  container, false)
  }

  override fun onViewCreated(rootView: View, savedInstanceState: Bundle?) {
    super.onViewCreated(rootView, savedInstanceState)

    val mainBottomNavigationView = rootView.findViewById<BottomNavigationView>(R.id.bottomNavigationView)
    when {
      AppPreferencesHelper().getUserRole() == "vendor" -> {
        mainBottomNavigationView.inflateMenu(R.menu.menu_vendor_home_bottom_view)
      }
      AppPreferencesHelper().getUserRole() == "customer" -> {
        mainBottomNavigationView.inflateMenu(R.menu.menu_customer_home_bottom_view)
      }
      AppPreferencesHelper().getUserRole() == "admin" -> {
        mainBottomNavigationView.inflateMenu(R.menu.menu_admin_navigation_drawer)
      }
    }
    mainBottomNavigationView.foregroundGravity = Gravity.FILL
    disableShiftMode(mainBottomNavigationView)
    mainBottomNavigationView.setOnNavigationItemSelectedListener {
      lateinit var selectedFragment: Fragment
      when (it.itemId) {
        R.id.customerViewCurrentOrders -> selectedFragment = CustomerCurrentOrderFragment()
        R.id.customerViewRejectedOrders -> selectedFragment = CustomerRejectedOrderFragment()
        R.id.customerViewHistoryOrders -> selectedFragment = CustomerHistoryOrderFragment()
        R.id.vendorViewIncomingOrders -> selectedFragment = VendorIncomingOrderFragment()
        R.id.vendorViewProcessingOrders -> selectedFragment = VendorProcessingOrderFragment()
        R.id.vendorViewProcessedOrders -> selectedFragment = VendorProcessedOrderFragment()
        R.id.vendorViewHistoryOrders -> selectedFragment = VendorHistoryOrderFragment()
      }
      childFragmentManager.beginTransaction().replace(R.id.container, selectedFragment).commit()
      true
    }

    when {
      AppPreferencesHelper().getUserRole() == "customer" -> mainBottomNavigationView.selectedItemId =
          R.id.customerViewCurrentOrders
      AppPreferencesHelper().getUserRole() == "vendor" -> mainBottomNavigationView.selectedItemId =
          R.id.vendorViewIncomingOrders
      //AppPreferencesHelper().getUserRole() == "admin" -> mainNavigationView.inflateMenu(R.menu.menu_admin_navigation_drawer)
    }
  }

  @SuppressLint("RestrictedApi")
  fun disableShiftMode(view: BottomNavigationView) {
    val menuView = view.getChildAt(0) as BottomNavigationMenuView
    try {
      val shiftingMode = menuView.javaClass.getDeclaredField("mShiftingMode")
      shiftingMode.isAccessible = true
      shiftingMode.setBoolean(menuView, false)
      shiftingMode.isAccessible = false
      for (i in 0 until menuView.childCount) {
        val item = menuView.getChildAt(i) as BottomNavigationItemView

        item.setShiftingMode(false)
        // set once again checked value, so view will be updated

        item.setChecked(item.itemData.isChecked)
      }
    } catch (e: NoSuchFieldException) {
      Log.e("BNVHelper", "Unable to get shift mode field", e)
    } catch (e: IllegalAccessException) {
      Log.e("BNVHelper", "Unable to change value of shift mode", e)
    }

  }
}