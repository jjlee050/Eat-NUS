package com.eatnus.ui.home.vendor.processing_order

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ClipDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eatnus.R
import com.eatnus.utils.helper.SwipeToProcessCallback
import com.eatnus.ui.home.vendor.processing_order.adapter.VendorProcessingOrderAdapter
import com.eatnus.ui.home.vendor.processing_order.contract.VendorProcessingOrderPresenterContract
import com.eatnus.ui.home.vendor.processing_order.presenter.VendorProcessingOrderPresenter
import com.eatnus.utils.ui.MyProgressDialog

class VendorProcessingOrderFragment : Fragment() {
    private lateinit var processingOrderPresenter: VendorProcessingOrderPresenterContract
    private var homeActivity: AppCompatActivity? = null
    private lateinit var processingOrderRecyclerView: RecyclerView
    private lateinit var emptyLayout: View
    private lateinit var loadDialog: MyProgressDialog

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is AppCompatActivity)
            this.homeActivity = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_vendor_processing_order, container, false)
    }

    override fun onViewCreated(rootView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(rootView, savedInstanceState)
        processingOrderRecyclerView = rootView.findViewById(R.id.processingOrderRecyclerView)
        val itemDecor = DividerItemDecoration(homeActivity!!.applicationContext, ClipDrawable.HORIZONTAL)

        loadDialog = MyProgressDialog(homeActivity!!, rootView.findViewById(R.id.newton_cradle_loading))
        loadDialog.start()

        processingOrderPresenter = VendorProcessingOrderPresenter(homeActivity!!, this@VendorProcessingOrderFragment, loadDialog)

        processingOrderRecyclerView.addItemDecoration(itemDecor)
        processingOrderRecyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        processingOrderRecyclerView.adapter = VendorProcessingOrderAdapter(processingOrderPresenter.showProcessingOrders(processingOrderRecyclerView), this@VendorProcessingOrderFragment, homeActivity!!, processingOrderPresenter)

        emptyLayout = rootView.findViewById<View>(R.id.emptyLayout)
        closeViews()

        val swipeRefreshLayout = rootView.findViewById<SwipeRefreshLayout>(R.id.swipeRefreshLayout)
        swipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE)

        swipeRefreshLayout.setOnRefreshListener {
            loadDialog.start()
            closeViews()
            processingOrderRecyclerView.adapter = VendorProcessingOrderAdapter(processingOrderPresenter.showProcessingOrders(processingOrderRecyclerView), this@VendorProcessingOrderFragment, homeActivity!!, processingOrderPresenter)
            swipeRefreshLayout.isRefreshing = false
        }

        val processSwipeHandler = object : SwipeToProcessCallback(homeActivity!!) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                loadDialog.start()
                val adapter = processingOrderRecyclerView.adapter as VendorProcessingOrderAdapter
                adapter.doneProcessingOrder(viewHolder.adapterPosition)
                isListEmpty(processingOrderRecyclerView.adapter.itemCount <= 0)
            }
        }

        ItemTouchHelper(processSwipeHandler).attachToRecyclerView(processingOrderRecyclerView)

    }

    override fun onDetach() {
        super.onDetach()
        this.homeActivity = null
    }

    private fun closeViews() {
        if(emptyLayout.visibility == View.VISIBLE) {
            emptyLayout.visibility = View.GONE
        }
        if((processingOrderRecyclerView.visibility == View.VISIBLE) && (processingOrderRecyclerView.adapter.itemCount <= 0)) {
            processingOrderRecyclerView.visibility = View.GONE
        }
    }

    fun isListEmpty(isEmpty: Boolean){
        if(isEmpty){
            emptyLayout.visibility = View.VISIBLE
            processingOrderRecyclerView.visibility = View.GONE
        } else {
            emptyLayout.visibility = View.GONE
            processingOrderRecyclerView.visibility = View.VISIBLE
        }
    }
}
