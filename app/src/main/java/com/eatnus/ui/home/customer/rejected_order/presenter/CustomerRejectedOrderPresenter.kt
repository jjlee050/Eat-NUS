package com.eatnus.ui.home.customer.rejected_order.presenter

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import com.eatnus.model.Food
import com.eatnus.model.FoodBasket
import com.eatnus.model.Order
import com.eatnus.ui.home.customer.rejected_order.CustomerRejectedOrderFragment
import com.eatnus.ui.home.customer.rejected_order.contract.CustomerRejectedOrderPresenterContract
import com.eatnus.utils.OrderStatus
import com.eatnus.utils.api.EatNUSAPI
import com.eatnus.utils.helper.AppPreferencesHelper
import com.eatnus.utils.ui.MyAlertDialog
import com.eatnus.utils.ui.MyProgressDialog
import com.google.android.gms.tasks.Tasks
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import java.util.*

class CustomerRejectedOrderPresenter(activity: AppCompatActivity, fragment: CustomerRejectedOrderFragment, dialog: MyProgressDialog): CustomerRejectedOrderPresenterContract {
    private var loadDialog = dialog
    private val homeActivity = activity
    private val customerRejectedOrderFragment = fragment

    override fun showRejectedOrders(rejectedOrderRecyclerView : RecyclerView): MutableList<Triple<Order, HashMap<Food, FoodBasket>, Pair<String, Float>>> {
        val customerOrderList = mutableListOf<Triple<Order, HashMap<Food, FoodBasket>, Pair<String, Float>>>()
        val ordersDBSource = EatNUSAPI.getOrders()
        val stallDBSource = EatNUSAPI.getStalls()
        val foodBasketDBSource = EatNUSAPI.getFoodBaskets()
        val foodDBSource = EatNUSAPI.getFoods()
        val task = Tasks.whenAll(ordersDBSource, stallDBSource, foodBasketDBSource, foodDBSource)

        task.addOnSuccessListener {
            val ordersData = ordersDBSource.result
            ordersData.children.forEach {
                val order = Order()
                order.orderID = it.child("orderID").value.toString().toInt()
                order.stallID = it.child("stallID").value.toString().toInt()
                order.username = it.child("username").value.toString()
                order.dateOfConfirmation = it.child("dateOfConfirmation").value.toString()
                order.foodBasketID = it.child("foodBasketID").value.toString().toInt()
                order.isTakeaway = it.child("takeaway").value.toString().toBoolean()
                order.status = OrderStatus.valueOf(it.child("status").value.toString())
                order.isCash = it.child("cash").value.toString().toBoolean()
                order.takeawayFee = it.child("takeawayFee").value.toString().toFloat()
                order.googlePayOrderID = it.child("googlePayOrderID").value.toString()
                if ((OrderStatus.valueOf(it.child("status").value.toString()) == OrderStatus.ORDER_REJECTED)) {
                    val stallData = stallDBSource.result.child(order.stallID.toString())
                    val foodBasketData = foodBasketDBSource.result
                    val takeawayPrice = if(order.isTakeaway) {
                        order.takeawayFee
                    } else {
                        0.0f
                    }
                    val stallName = stallData.child("name").value.toString()
                    if (order.username.toLowerCase() == AppPreferencesHelper().getUsername()?.toLowerCase()) {
                        var totalPrice = takeawayPrice
                        val basketMap = HashMap<Food, FoodBasket>()
                        foodBasketData.child(order.foodBasketID.toString()).children.forEach {
                            val foodBasket = FoodBasket()
                            foodBasket.qty = it.child("qty").value.toString().toInt()
                            foodBasket.foodID = it.child("foodID").value.toString().toInt()
                            foodBasket.price = it.child("price").value.toString().toFloat()
                            val foodData = foodDBSource.result.child(foodBasket.foodID.toString())
                            val food = Food()
                            food.name = foodData.child("name").value.toString()
                            totalPrice += foodBasket.qty * foodBasket.price
                            basketMap.put(food, foodBasket)

                        }
                        customerOrderList.add(Triple(order, basketMap, Pair(stallName, totalPrice)))
                        rejectedOrderRecyclerView.adapter.notifyDataSetChanged()
                    }
                }
            }
            loadDialog.stop()
            customerRejectedOrderFragment.isListEmpty(customerOrderList.isEmpty())
        }
        return customerOrderList.asReversed()
    }

    override fun retrieveMyBasket(basketID: Int, recyclerView: RecyclerView): Map<Food, FoodBasket>{
        val basketMap = HashMap<Food, FoodBasket>()
        EatNUSAPI.getFoodBasketRef(basketID.toString()).addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                loadDialog.stop()
                MyAlertDialog().createSimpleErrorDialog(homeActivity, "Error", "Unable to retrieve user food basket.")
            }

            override fun onDataChange(p0: DataSnapshot) {
                p0.children.forEach {
                    val foodBasket = FoodBasket()
                    foodBasket.qty = it.child("qty").value.toString().toInt()
                    foodBasket.foodID = it.child("foodID").value.toString().toInt()
                    foodBasket.price = it.child("price").value.toString().toFloat()
                    EatNUSAPI.getFoodRef(foodBasket.foodID.toString()).addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onCancelled(p0: DatabaseError) {
                            loadDialog.stop()
                            MyAlertDialog().createSimpleErrorDialog(homeActivity, "Error", "Unable to retrieve user food basket.")
                        }

                        override fun onDataChange(p0: DataSnapshot) {
                            val food = Food()
                            food.name = p0.child("name").value.toString()
                            basketMap.put(food, foodBasket)
                            recyclerView.adapter.notifyDataSetChanged()
                        }
                    })
                }
                loadDialog.stop()
            }
        })
        return basketMap
    }
}