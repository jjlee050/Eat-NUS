package com.eatnus.ui.home.customer.view_order.contract

import android.support.v7.widget.RecyclerView
import android.widget.TextView
import com.eatnus.model.Food
import com.eatnus.model.FoodBasket
import com.eatnus.model.Order

interface CustomerViewOrderPresenterContract {
    fun acknowledgeOrder(order: Order, pts: Float)
    fun retrieveMyBasket(basketID: Int, recyclerView: RecyclerView): Map<Food, FoodBasket>
}