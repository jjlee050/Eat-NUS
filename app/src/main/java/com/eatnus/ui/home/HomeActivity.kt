package com.eatnus.ui.home

import android.content.Intent
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.eatnus.R
import com.eatnus.ui.login.LoginActivity
import com.eatnus.ui.vendor_settings.SettingsActivity
import com.eatnus.utils.api.EatNUSAPI
import com.eatnus.utils.helper.AppPreferencesHelper
import com.google.firebase.auth.FirebaseAuth
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import com.eatnus.ui.feedback.FeedbackActivity
import com.eatnus.ui.home.contract.HomePresenterContract
import com.eatnus.ui.home.contract.HomeViewContract
import com.eatnus.ui.home.presenter.HomePresenter
import com.eatnus.ui.reward.RewardActivity

//HomeActivity.kt
/*
 * View Class to handle customer home page activity.
 */
class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
  HomeViewContract {
  private lateinit var drawerLayout: DrawerLayout
  private lateinit var toggle: ActionBarDrawerToggle
  private lateinit var mainNavigationView: NavigationView
  private lateinit var homePresenter: HomePresenterContract

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_home_page)

    drawerLayout = findViewById(R.id.drawer_layout)
    toggle = ActionBarDrawerToggle(
      this@HomeActivity,
      drawerLayout,
      R.string.navigation_drawer_open,
      R.string.navigation_drawer_close
    )

    drawerLayout.addDrawerListener(toggle)
    toggle.syncState()

    supportActionBar?.title = resources.getString(R.string.drawer_menu_home) + when {
      EatNUSAPI.Debug -> "-Debug"
      else -> ""
    }
    supportActionBar?.setDisplayHomeAsUpEnabled(true)

    mainNavigationView = findViewById(R.id.navigation_drawer)
    when {
      AppPreferencesHelper().getUserRole() == "customer" -> mainNavigationView.inflateMenu(R.menu.menu_customer_navigation_drawer)
      AppPreferencesHelper().getUserRole() == "vendor" -> mainNavigationView.inflateMenu(R.menu.menu_vendor_navigation_drawer)
      AppPreferencesHelper().getUserRole() == "admin" -> mainNavigationView.inflateMenu(R.menu.menu_admin_navigation_drawer)
    }

    mainNavigationView.menu.getItem(0).isChecked = true
    mainNavigationView.itemIconTintList = null
    mainNavigationView.setNavigationItemSelectedListener(this)

    supportFragmentManager.beginTransaction().replace(R.id.container, HomeFragment()).commit()

    homePresenter = HomePresenter(this)
    println("Debug" + EatNUSAPI.Debug)
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    return when (item.itemId) {
      android.R.id.home -> {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
          drawerLayout.closeDrawer(GravityCompat.START)
        else
          drawerLayout.openDrawer(GravityCompat.START)
        true
      }
      else -> super.onOptionsItemSelected(item)
    }
  }

  override fun onNavigationItemSelected(item: MenuItem): Boolean {
    // update the main content by replacing fragments
    supportFragmentManager.popBackStack()
    drawerLayout.closeDrawer(GravityCompat.START)

    when {
      AppPreferencesHelper().getUserRole() == "customer" -> when (item.itemId) {
        R.id.home -> mainNavigationView.menu.getItem(0).isChecked = true
        R.id.orderFood -> homePresenter.orderFood(this)
        R.id.rewards -> startActivity(Intent(this@HomeActivity, RewardActivity::class.java))
        R.id.feedback -> startActivity(Intent(this@HomeActivity, FeedbackActivity::class.java))
        R.id.logout -> logout()
      }
      AppPreferencesHelper().getUserRole() == "vendor" -> when (item.itemId) {
        R.id.home -> mainNavigationView.menu.getItem(0).isChecked = true
        R.id.settings -> startActivity(Intent(this@HomeActivity, SettingsActivity::class.java))
        R.id.feedback -> startActivity(Intent(this@HomeActivity, FeedbackActivity::class.java))
        R.id.logout -> logout()
      }
      else -> when (item.itemId) {
        R.id.home -> mainNavigationView.menu.getItem(0).isChecked = true
        R.id.logout -> logout()
      }
    }
    return true
  }

  private fun logout() {
    FirebaseAuth.getInstance().signOut()

    val helper = AppPreferencesHelper()
    helper.setAccessToken("")
    EatNUSAPI.updateToken("")

    helper.clearPreferences()

    intent = Intent(this@HomeActivity, LoginActivity::class.java)
    startActivity(intent)
    finish()
  }

}
