package com.eatnus.ui.home.customer.current_order

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ClipDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eatnus.R
import com.eatnus.ui.home.customer.current_order.adapter.CustomerCurrentOrderAdapter
import com.eatnus.ui.home.customer.current_order.contract.CustomerCurrentOrderPresenterContract
import com.eatnus.ui.home.customer.current_order.presenter.CustomerCurrentOrderPresenter
import com.eatnus.utils.ui.MyProgressDialog

class CustomerCurrentOrderFragment: Fragment() {
    private lateinit var customerHistoryOrderPresenter: CustomerCurrentOrderPresenterContract
    private var homeActivity: AppCompatActivity? = null
    private lateinit var currentOrderRecyclerView: RecyclerView
    private lateinit var emptyLayout: View
    private lateinit var loadDialog: MyProgressDialog

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is AppCompatActivity)
            this.homeActivity = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_customer_current_order, container, false)
    }

    override fun onViewCreated(rootView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(rootView, savedInstanceState)

        currentOrderRecyclerView = rootView.findViewById(R.id.currentOrderRecyclerView)
        val swipeRefreshLayout = rootView.findViewById<SwipeRefreshLayout>(R.id.swipeRefreshLayout)
        val itemDecor = DividerItemDecoration(activity?.applicationContext, ClipDrawable.HORIZONTAL)

        loadDialog = MyProgressDialog(homeActivity!!, rootView.findViewById(R.id.newton_cradle_loading))
        loadDialog.start()

        customerHistoryOrderPresenter = CustomerCurrentOrderPresenter(homeActivity!!, this@CustomerCurrentOrderFragment, loadDialog)

        currentOrderRecyclerView.addItemDecoration(itemDecor)
        currentOrderRecyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        currentOrderRecyclerView.adapter = CustomerCurrentOrderAdapter(customerHistoryOrderPresenter.showCurrentOrders(currentOrderRecyclerView), this@CustomerCurrentOrderFragment, homeActivity!!)

        emptyLayout = rootView.findViewById<View>(R.id.emptyLayout)
        closeViews()

        swipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE)
        swipeRefreshLayout.setOnRefreshListener {
            loadDialog.start()
            closeViews()
            currentOrderRecyclerView.adapter = CustomerCurrentOrderAdapter(customerHistoryOrderPresenter.showCurrentOrders(currentOrderRecyclerView), this@CustomerCurrentOrderFragment, homeActivity!!)
            swipeRefreshLayout.isRefreshing = false
        }
    }

    override fun onDetach() {
        super.onDetach()
        this.homeActivity = null
    }

    private fun closeViews() {
        if(emptyLayout.visibility == View.VISIBLE) {
            emptyLayout.visibility = View.GONE
        }
        if(currentOrderRecyclerView.visibility == View.VISIBLE) {
            currentOrderRecyclerView.visibility = View.GONE
        }
    }

    fun isListEmpty(isEmpty: Boolean){
        if(isEmpty){
            emptyLayout.visibility = View.VISIBLE
            currentOrderRecyclerView.visibility = View.GONE
        } else {
            emptyLayout.visibility = View.GONE
            currentOrderRecyclerView.visibility = View.VISIBLE
        }
    }
}