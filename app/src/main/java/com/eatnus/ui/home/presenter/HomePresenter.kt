package com.eatnus.ui.home.presenter

import android.app.Activity
import android.content.Intent
import android.support.v4.app.FragmentActivity
import com.eatnus.ui.home.contract.HomePresenterContract
import com.eatnus.ui.home.contract.HomeViewContract
import com.eatnus.ui.order_food.OrderFoodActivity

class HomePresenter(mHomeView: HomeViewContract): HomePresenterContract{
    private var homeView: HomeViewContract = mHomeView

    /**
     * Redirect to the select food court page.
     */
    override fun orderFood(activity: FragmentActivity){
        val orderActivity = Intent(activity, OrderFoodActivity::class.java)
        activity.startActivity(orderActivity)
        activity.finish()
    }
}