package com.eatnus.ui.home.vendor.processed_order

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ClipDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eatnus.R
import com.eatnus.ui.home.vendor.processed_order.adapter.VendorProcessedOrderAdapter
import com.eatnus.ui.home.vendor.processed_order.contract.VendorProcessedOrderPresenterContract
import com.eatnus.ui.home.vendor.processed_order.presenter.VendorProcessedOrderPresenter
import com.eatnus.ui.home.vendor.processing_order.adapter.VendorProcessingOrderAdapter
import com.eatnus.utils.helper.SwipeToProcessCallback
import com.eatnus.utils.ui.MyProgressDialog

class VendorProcessedOrderFragment : Fragment() {
    private lateinit var processedOrderPresenter: VendorProcessedOrderPresenterContract
    private var homeActivity: AppCompatActivity? = null
    private lateinit var processedOrderRecyclerView: RecyclerView
    private lateinit var emptyLayout: View
    private lateinit var loadDialog: MyProgressDialog

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is AppCompatActivity)
            this.homeActivity = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_vendor_processed_order, container, false)
    }

    override fun onViewCreated(rootView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(rootView, savedInstanceState)

        processedOrderRecyclerView = rootView.findViewById(R.id.processedOrderRecyclerView)
        val itemDecor = DividerItemDecoration(homeActivity?.applicationContext, ClipDrawable.HORIZONTAL)

        loadDialog = MyProgressDialog(homeActivity!!, rootView.findViewById(R.id.newton_cradle_loading))
        loadDialog.start()

        processedOrderPresenter = VendorProcessedOrderPresenter(homeActivity!!, this@VendorProcessedOrderFragment, loadDialog)

        processedOrderRecyclerView.addItemDecoration(itemDecor)
        processedOrderRecyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        processedOrderRecyclerView.adapter = VendorProcessedOrderAdapter(processedOrderPresenter.showProcessedOrders(processedOrderRecyclerView), this@VendorProcessedOrderFragment, homeActivity!!, processedOrderPresenter)

        emptyLayout = rootView.findViewById<View>(R.id.emptyLayout)
        closeViews()

        val swipeRefreshLayout = rootView.findViewById<SwipeRefreshLayout>(R.id.swipeRefreshLayout)
        swipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE)
        swipeRefreshLayout.setOnRefreshListener {
            loadDialog.start()
            closeViews()
            processedOrderRecyclerView.adapter = VendorProcessedOrderAdapter(processedOrderPresenter.showProcessedOrders(processedOrderRecyclerView), this@VendorProcessedOrderFragment, homeActivity!!, processedOrderPresenter)
            swipeRefreshLayout.isRefreshing = false
        }


        val collectedSwipeHandler = object : SwipeToProcessCallback(homeActivity!!) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                loadDialog.start()
                val adapter = processedOrderRecyclerView.adapter as VendorProcessedOrderAdapter
                adapter.orderCollected(viewHolder.adapterPosition)
                isListEmpty(processedOrderRecyclerView.adapter.itemCount <= 0)
            }
        }

        ItemTouchHelper(collectedSwipeHandler).attachToRecyclerView(processedOrderRecyclerView)
    }

    override fun onDetach() {
        super.onDetach()
        this.homeActivity = null
    }

    private fun closeViews() {
        if(emptyLayout.visibility == View.VISIBLE) {
            emptyLayout.visibility = View.GONE
        }
        if(processedOrderRecyclerView.visibility == View.VISIBLE) {
            processedOrderRecyclerView.visibility = View.GONE
        }
    }

    fun isListEmpty(isEmpty: Boolean){
        if(isEmpty){
            emptyLayout.visibility = View.VISIBLE
            processedOrderRecyclerView.visibility = View.GONE
        } else {
            emptyLayout.visibility = View.GONE
            processedOrderRecyclerView.visibility = View.VISIBLE
        }
    }
}
