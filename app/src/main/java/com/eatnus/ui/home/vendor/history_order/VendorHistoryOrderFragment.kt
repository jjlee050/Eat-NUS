package com.eatnus.ui.home.vendor.history_order

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ClipDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eatnus.R
import com.eatnus.ui.home.vendor.history_order.adapter.VendorHistoryOrderAdapter
import com.eatnus.ui.home.vendor.history_order.contract.VendorHistoryOrderPresenterContract
import com.eatnus.ui.home.vendor.history_order.presenter.VendorHistoryOrderPresenter
import com.eatnus.utils.ui.MyProgressDialog

class VendorHistoryOrderFragment: Fragment() {
    private lateinit var vendorHistoryOrderPresenter: VendorHistoryOrderPresenterContract
    private var homeActivity: AppCompatActivity? = null
    private lateinit var historyOrderRecyclerView: RecyclerView
    private lateinit var emptyLayout: View
    private lateinit var loadDialog: MyProgressDialog

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is AppCompatActivity)
            this.homeActivity = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_vendor_history_order, container, false)
    }

    override fun onViewCreated(rootView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(rootView, savedInstanceState)

        historyOrderRecyclerView = rootView.findViewById(R.id.historyOrderRecyclerView)
        val swipeRefreshLayout = rootView.findViewById<SwipeRefreshLayout>(R.id.swipeRefreshLayout)
        val itemDecor = DividerItemDecoration(activity?.applicationContext, ClipDrawable.HORIZONTAL)

        loadDialog = MyProgressDialog(homeActivity!!, rootView.findViewById(R.id.newton_cradle_loading))
        loadDialog.start()

        vendorHistoryOrderPresenter = VendorHistoryOrderPresenter(homeActivity!!, this@VendorHistoryOrderFragment, loadDialog)

        historyOrderRecyclerView.addItemDecoration(itemDecor)
        historyOrderRecyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        historyOrderRecyclerView.adapter = VendorHistoryOrderAdapter(vendorHistoryOrderPresenter.showPastOrders(historyOrderRecyclerView), this@VendorHistoryOrderFragment, vendorHistoryOrderPresenter)

        emptyLayout = rootView.findViewById<View>(R.id.emptyLayout)
        closeViews()

        swipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE)
        swipeRefreshLayout.setOnRefreshListener {
            loadDialog.start()
            closeViews()
            historyOrderRecyclerView.adapter = VendorHistoryOrderAdapter(vendorHistoryOrderPresenter.showPastOrders(historyOrderRecyclerView), this@VendorHistoryOrderFragment, vendorHistoryOrderPresenter)
            swipeRefreshLayout.isRefreshing = false
        }
    }

    override fun onDetach() {
        super.onDetach()
        this.homeActivity = null
    }

    private fun closeViews() {
        if(emptyLayout.visibility == View.VISIBLE) {
            emptyLayout.visibility = View.GONE
        }
        if(historyOrderRecyclerView.visibility == View.VISIBLE) {
            historyOrderRecyclerView.visibility = View.GONE
        }
    }

    fun isListEmpty(isEmpty: Boolean){
        if(isEmpty){
            emptyLayout.visibility = View.VISIBLE
            historyOrderRecyclerView.visibility = View.GONE
        } else {
            emptyLayout.visibility = View.GONE
            historyOrderRecyclerView.visibility = View.VISIBLE
        }
    }
}