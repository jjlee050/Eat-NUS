package com.eatnus.ui.home.contract

import android.support.v4.app.FragmentActivity

interface HomePresenterContract {
    /**
     * Redirect to the select food court page.
     */
    fun orderFood(activity: FragmentActivity)
}