package com.eatnus.ui.home.vendor.incoming_order

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ClipDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eatnus.R
import com.eatnus.ui.home.vendor.incoming_order.adapter.VendorIncomingOrderAdapter
import com.eatnus.utils.helper.SwipeToProcessCallback
import com.eatnus.utils.helper.SwipeToRejectCallback
import com.eatnus.ui.home.vendor.incoming_order.contract.VendorIncomingOrderPresenterContract
import com.eatnus.ui.home.vendor.incoming_order.presenter.VendorIncomingOrderPresenter
import com.eatnus.utils.ui.MyProgressDialog

class VendorIncomingOrderFragment : Fragment() {
    private lateinit var incomingOrderPresenter: VendorIncomingOrderPresenterContract
    private var homeActivity: AppCompatActivity? = null
    private lateinit var incomingOrderRecyclerView: RecyclerView
    private lateinit var emptyLayout: View
    private lateinit var loadDialog: MyProgressDialog

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is AppCompatActivity)
            this.homeActivity = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_vendor_incoming_order, container, false)
    }

    override fun onViewCreated(rootView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(rootView, savedInstanceState)
        incomingOrderRecyclerView = rootView.findViewById(R.id.incomingOrderRecyclerView)
        val itemDecor = DividerItemDecoration(homeActivity!!.applicationContext, ClipDrawable.HORIZONTAL)

        loadDialog = MyProgressDialog(homeActivity!!, rootView.findViewById(R.id.newton_cradle_loading))
        loadDialog.start()

        incomingOrderPresenter = VendorIncomingOrderPresenter(homeActivity!!, this@VendorIncomingOrderFragment, loadDialog)

        incomingOrderRecyclerView.addItemDecoration(itemDecor)
        incomingOrderRecyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        incomingOrderRecyclerView.adapter = VendorIncomingOrderAdapter(incomingOrderPresenter.showIncomingOrders(incomingOrderRecyclerView), this@VendorIncomingOrderFragment, homeActivity!!, incomingOrderPresenter)

        emptyLayout = rootView.findViewById<View>(R.id.emptyLayout)
        closeViews()

        val swipeRefreshLayout = rootView.findViewById<SwipeRefreshLayout>(R.id.swipeRefreshLayout)
        swipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE)
        swipeRefreshLayout.setOnRefreshListener {
            loadDialog.start()
            closeViews()
            incomingOrderRecyclerView.adapter = VendorIncomingOrderAdapter(incomingOrderPresenter.showIncomingOrders(incomingOrderRecyclerView), this@VendorIncomingOrderFragment, homeActivity!!, incomingOrderPresenter)
            swipeRefreshLayout.isRefreshing = false
        }

        val rejectSwipeHandler = object : SwipeToRejectCallback(homeActivity!!) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                loadDialog.start()
                val adapter = incomingOrderRecyclerView.adapter as VendorIncomingOrderAdapter
                adapter.rejectOrder(viewHolder.adapterPosition, loadDialog)
                isListEmpty(incomingOrderRecyclerView.adapter.itemCount <= 0)
            }
        }

        val processSwipeHandler = object : SwipeToProcessCallback(homeActivity!!) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                loadDialog.start()
                val adapter = incomingOrderRecyclerView.adapter as VendorIncomingOrderAdapter
                adapter.processOrder(viewHolder.adapterPosition)
                isListEmpty(incomingOrderRecyclerView.adapter.itemCount <= 0)
            }
        }

        ItemTouchHelper(rejectSwipeHandler).attachToRecyclerView(incomingOrderRecyclerView)
        ItemTouchHelper(processSwipeHandler).attachToRecyclerView(incomingOrderRecyclerView)

    }

    override fun onDetach() {
        super.onDetach()
        this.homeActivity = null
    }

    private fun closeViews() {
        if(emptyLayout.visibility == View.VISIBLE) {
            emptyLayout.visibility = View.GONE
        }
        if((incomingOrderRecyclerView.visibility == View.VISIBLE) && (incomingOrderRecyclerView.adapter.itemCount <= 0)) {
            incomingOrderRecyclerView.visibility = View.GONE
        }
    }

    fun isListEmpty(isEmpty: Boolean){
        if(isEmpty){
            emptyLayout.visibility = View.VISIBLE
            incomingOrderRecyclerView.visibility = View.GONE
        } else {
            emptyLayout.visibility = View.GONE
            incomingOrderRecyclerView.visibility = View.VISIBLE
        }
    }
}
