package com.eatnus.ui.home.customer.current_order.adapter

import android.content.Intent
import android.support.constraint.ConstraintLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.eatnus.R
import com.eatnus.model.Food
import com.eatnus.model.FoodBasket
import com.eatnus.model.Order
import com.eatnus.ui.home.customer.current_order.CustomerCurrentOrderFragment
import com.eatnus.ui.home.customer.view_order.CustomerViewOrderActivity
import java.text.DecimalFormat
import java.util.HashMap

class CustomerCurrentOrderAdapter(private val items: MutableList<Triple<Order, HashMap<Food, FoodBasket>, Pair<String, Float>>>, private val fragment: CustomerCurrentOrderFragment, private val incomingOrderActivity: AppCompatActivity) : RecyclerView.Adapter<ViewHolder>() {

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val decimalFormat = DecimalFormat("0.00")
        val order = items[position].first

        holder.stallNameTextView.text = items[position].third.first
        holder.priceTextView.text = "$ ${decimalFormat.format(items[position].third.second.toBigDecimal())}"
        holder.itemView.setOnClickListener {
            val intent = Intent(incomingOrderActivity, CustomerViewOrderActivity()::class.java)
            intent.putExtra("currentOrder", order)
            intent.putExtra("myBasket", items[position].second)
            intent.putExtra("stallName", holder.stallNameTextView.text.toString())
            intent.putExtra("price", decimalFormat.format(items[position].third.second.toBigDecimal()))
            incomingOrderActivity.startActivity(intent)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(fragment.context).inflate(R.layout.list_item_customer_current_order, parent, false)
        return ViewHolder(view)
    }

}
class ViewHolder (view: View) : RecyclerView.ViewHolder(view){
    // Holds the TextView that will add each animal to
    val mainLayout: ConstraintLayout = view.findViewById(R.id.mainLayout)
    val stallNameTextView: TextView = view.findViewById(R.id.stallNameTextView)
    val priceTextView: TextView = view.findViewById(R.id.priceTextView)
}