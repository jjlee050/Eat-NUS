package com.eatnus.ui.home.vendor.incoming_order.contract

import android.support.v7.widget.RecyclerView
import android.widget.TextView
import com.eatnus.model.Food
import com.eatnus.model.FoodBasket
import com.eatnus.model.Order
import java.util.*

//VendorIncomingOrderPresenterContract.kt
interface VendorIncomingOrderPresenterContract {
  fun showIncomingOrders(incomingOrderRecyclerView: RecyclerView): MutableList<Triple<Order, HashMap<Food, FoodBasket>, Pair<String, Float>>>
  fun retrieveMyBasket(basketID: Int, recyclerView: RecyclerView): Map<Food, FoodBasket>
  fun processOrder(order: Order)
  fun rejectOrder(order: Order)
  fun refundOrder(order: Order): String?
}