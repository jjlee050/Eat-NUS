package com.eatnus.ui.home.vendor.incoming_order.presenter

import android.os.AsyncTask
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.widget.TextView
import android.widget.Toast
import com.eatnus.App
import com.eatnus.model.Food
import com.eatnus.model.FoodBasket
import com.eatnus.model.Order
import com.eatnus.ui.home.vendor.incoming_order.VendorIncomingOrderFragment
import com.eatnus.ui.home.vendor.incoming_order.contract.VendorIncomingOrderPresenterContract
import com.eatnus.ui.home.vendor.processed_order.VendorProcessedOrderFragment
import com.eatnus.ui.order_food.payment.PaymentSelectFragment
import com.eatnus.ui.order_food.payment.contract.PaymentSelectPresenterContract
import com.eatnus.utils.OrderStatus
import com.eatnus.utils.api.EatNUSAPI
import com.eatnus.utils.helper.AppPreferencesHelper
import com.eatnus.utils.ui.MyAlertDialog
import com.eatnus.utils.ui.MyProgressDialog
import com.google.android.gms.tasks.Tasks
import com.google.firebase.database.*
import com.stripe.Stripe
import com.stripe.exception.APIConnectionException
import com.stripe.exception.APIException
import com.stripe.exception.AuthenticationException
import com.stripe.exception.InvalidRequestException
import java.util.*
import com.stripe.model.Refund
import kotlin.math.roundToInt

class VendorIncomingOrderPresenter(
  activity: AppCompatActivity,
  fragment: VendorIncomingOrderFragment,
  dialog: MyProgressDialog
) : VendorIncomingOrderPresenterContract {
  private var loadDialog = dialog
  private val homeActivity = activity
  private val vendorIncomingOrderFragment = fragment

  override fun showIncomingOrders(incomingOrderRecyclerView: RecyclerView): MutableList<Triple<Order, HashMap<Food, FoodBasket>, Pair<String, Float>>> {
    val vendorOrderList =
      mutableListOf<Triple<Order, HashMap<Food, FoodBasket>, Pair<String, Float>>>()
    val ordersDBSource = EatNUSAPI.getOrders()
    val stallDBSource = EatNUSAPI.getStalls()
    val foodBasketDBSource = EatNUSAPI.getFoodBaskets()
    val foodDBSource = EatNUSAPI.getFoods()
    val task = Tasks.whenAll(ordersDBSource, stallDBSource, foodBasketDBSource, foodDBSource)

    task.addOnSuccessListener {
      val ordersData = ordersDBSource.result
      ordersData.children.forEach {
        val order = Order()
        order.orderID = it.child("orderID").value.toString().toInt()
        order.stallID = it.child("stallID").value.toString().toInt()
        order.username = it.child("username").value.toString()
        order.dateOfConfirmation = it.child("dateOfConfirmation").value.toString()
        order.foodBasketID = it.child("foodBasketID").value.toString().toInt()
        order.isTakeaway = it.child("takeaway").value.toString().toBoolean()
        order.takeawayFee = it.child("takeawayFee").value.toString().toFloat()
        order.googlePayOrderID = it.child("googlePayOrderID").value.toString()
        if (OrderStatus.valueOf(it.child("status").value.toString()) == OrderStatus.ORDER_SENT) {
          val stallData = stallDBSource.result.child(order.stallID.toString())
          val foodBasketData = foodBasketDBSource.result
          val takeawayPrice = if (order.isTakeaway) {
            order.takeawayFee
          } else {
            0.0f
          }
          val stallName = stallData.child("name").value.toString()
          if (stallData.child("username").value.toString().toLowerCase() == AppPreferencesHelper().getUsername()?.toLowerCase()) {
            var totalPrice = takeawayPrice
            val basketMap = HashMap<Food, FoodBasket>()
            foodBasketData.child(order.foodBasketID.toString()).children.forEach {
              val foodBasket = FoodBasket()
              foodBasket.qty = it.child("qty").value.toString().toInt()
              foodBasket.foodID = it.child("foodID").value.toString().toInt()
              foodBasket.price = it.child("price").value.toString().toFloat()

              val foodData = foodDBSource.result.child(foodBasket.foodID.toString())
              val food = Food()
              food.name = foodData.child("name").value.toString()
              totalPrice += foodBasket.qty * foodBasket.price
              basketMap.put(food, foodBasket)

            }
            vendorOrderList.add(Triple(order, basketMap, Pair(order.username, totalPrice)))
            incomingOrderRecyclerView.adapter.notifyDataSetChanged()
          }
        }
      }
      loadDialog.stop()
      vendorIncomingOrderFragment.isListEmpty(vendorOrderList.isEmpty())
    }
    return vendorOrderList.asReversed()
  }

  override fun retrieveMyBasket(basketID: Int, recyclerView: RecyclerView): Map<Food, FoodBasket> {
    val basketMap = HashMap<Food, FoodBasket>()
    EatNUSAPI.getFoodBasketRef(basketID.toString())
      .addListenerForSingleValueEvent(object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) {
          loadDialog.stop()
          MyAlertDialog().createSimpleErrorDialog(
            homeActivity,
            "Error",
            "Unable to retrieve user food basket."
          )
        }

        override fun onDataChange(p0: DataSnapshot) {
          p0.children.forEach {
            val foodBasket = FoodBasket()
            foodBasket.qty = it.child("qty").value.toString().toInt()
            foodBasket.foodID = it.child("foodID").value.toString().toInt()
            foodBasket.price = it.child("price").value.toString().toFloat()
            EatNUSAPI.getFoodRef(foodBasket.foodID.toString())
              .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                  loadDialog.stop()
                  MyAlertDialog().createSimpleErrorDialog(
                    homeActivity,
                    "Error",
                    "Unable to retrieve user food basket."
                  )
                }

                override fun onDataChange(p0: DataSnapshot) {
                  val food = Food()
                  food.name = p0.child("name").value.toString()
                  basketMap.put(food, foodBasket)
                  recyclerView.adapter.notifyDataSetChanged()
                }
              })
          }
          loadDialog.stop()
        }
      })
    return basketMap
  }

  override fun processOrder(order: Order) {
    EatNUSAPI.getOrderRef(order.orderID.toString()).child("status")
      .ref.setValue(OrderStatus.ORDER_PROCESSING)
    loadDialog.stop()
  }

  override fun rejectOrder(order: Order) {
    EatNUSAPI.getOrderRef(order.orderID.toString()).child("status")
      .ref.setValue(OrderStatus.ORDER_REJECTED)
    EatNUSAPI.getOrderRef(order.orderID.toString()).child("rejectReasons")
      .ref.setValue(order.rejectReasons)
    loadDialog.stop()
  }

  override fun refundOrder(order: Order): String? {
    //This is for Google wallet payment refund
    if (order.googlePayOrderID.isNotEmpty()) {
      try {
        Stripe.apiKey = EatNUSAPI.StripeSecretKey
        val params = HashMap<String, Any>()
        params["charge"] = order.googlePayOrderID
        val additionalInfo = HashMap<String, Any>()
        additionalInfo["reason for rejection"] = order.rejectReasons
        params["metadata"] = additionalInfo
        return Refund.create(params).id
      } catch (e: AuthenticationException) {
        e.printStackTrace()
        return ""
      } catch (e: InvalidRequestException) {
        e.printStackTrace()
        return ""
      } catch (e: APIConnectionException) {
        e.printStackTrace()
        return ""
      } catch (e: APIException) {
        e.printStackTrace()
        return ""
      }
    }
    return "NA"
  }

  class RefundUser(
    presenter: VendorIncomingOrderPresenterContract,
    order: Order,
    loadDialog: MyProgressDialog
  ) : AsyncTask<String, Void, String>() {
    private val vendorIncomingOrderPresenter = presenter
    private val order = order
    private val loadDialog = loadDialog
    override fun doInBackground(vararg p0: String?): String {
      return vendorIncomingOrderPresenter.refundOrder(order)!!
    }

    override fun onPostExecute(result: String?) {
      println(result)
      if (!result.isNullOrEmpty()) {
        Toast.makeText(App.context, "Your order has been refunded successfully.", Toast.LENGTH_LONG)
          .show()
        if(result != "NA")
          vendorIncomingOrderPresenter.rejectOrder(order)
      } else {
        Toast.makeText(App.context, "Your order cannot refund successfully.", Toast.LENGTH_LONG).show()
        loadDialog.stop()
      }
    }

    override fun onCancelled() {
      super.onCancelled()
      loadDialog.stop()
    }
  }
}