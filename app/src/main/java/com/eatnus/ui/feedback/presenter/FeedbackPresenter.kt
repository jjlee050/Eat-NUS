package com.eatnus.ui.feedback.presenter

import android.util.Log
import com.eatnus.model.Feedback
import com.eatnus.ui.feedback.FeedbackActivity
import com.eatnus.ui.feedback.contract.FeedbackPresenterContract
import com.eatnus.utils.api.EatNUSAPI
import com.eatnus.utils.helper.AppPreferencesHelper
import com.google.android.gms.tasks.Tasks
import kotlinx.android.synthetic.main.activity_feedback.*
import java.text.SimpleDateFormat
import java.util.*

class FeedbackPresenter(private val view: FeedbackActivity) : FeedbackPresenterContract {
  override fun createFeedback() {
    val title = view.titleSpinner.selectedItem.toString()
    val desc = view.descriptionEditText.text.toString()

    val calendar = Calendar.getInstance()
    calendar.timeZone = TimeZone.getTimeZone("Asia/Singapore")

    //
    var feedback = Feedback(1, AppPreferencesHelper().getUsername()!!, title, desc, SimpleDateFormat("dd/MM/yyyy HH:mm").format(calendar.time))
    val feedbackDBSource = EatNUSAPI.getFeedbacks()
    val task = Tasks.whenAll(feedbackDBSource)
    task.addOnSuccessListener {
      val feedbackData = feedbackDBSource.result

      feedbackData.children.forEach {
        feedback.feedbackID = Integer.valueOf(it.key!!.toInt()) + 1
      }

      EatNUSAPI.getFeedbacksRef().child(feedback.feedbackID.toString()).setValue(feedback)
      view.showSuccessfulMessage()
    }.addOnFailureListener {
      Log.e("Eat@NUS-Feedback", it.toString())
      view.showErrorMessage()
    }
  }
}