package com.eatnus.ui.feedback

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import android.widget.Toast
import com.eatnus.R
import com.eatnus.ui.feedback.presenter.FeedbackPresenter
import com.eatnus.utils.helper.AppPreferencesHelper
import kotlinx.android.synthetic.main.activity_feedback.*
import java.util.*

class FeedbackActivity: AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_feedback)

    var list: MutableList<String> = mutableListOf()
    when (AppPreferencesHelper().getUserRole()) {
        "customer" -> list = Arrays.asList("Issue with viewing order", "Issue with payment", "Issue with ordering", "Issue with rewards", "Suggestions")
        "vendor" -> list = Arrays.asList("Issue with viewing order", "Issue with processing order", "Issue with settings", "Suggestions")
    }
    list.sort()
    titleSpinner.adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list)

    val feedbackPresenter = FeedbackPresenter(this)
    submitButton.setOnClickListener {
      feedbackPresenter.createFeedback()
    }
  }

  fun showSuccessfulMessage() {
    Toast.makeText(this, "Your feedback has been sent successfully.", Toast.LENGTH_LONG).show()
  }

  fun showErrorMessage() {
    Toast.makeText(this, "Error in sending feedback.", Toast.LENGTH_LONG).show()
  }
}