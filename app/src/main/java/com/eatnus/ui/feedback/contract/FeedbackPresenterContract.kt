package com.eatnus.ui.feedback.contract

interface FeedbackPresenterContract {
  fun createFeedback()
}