package com.eatnus.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import com.eatnus.R
import com.eatnus.ui.home.HomeActivity
import com.eatnus.ui.login.LoginActivity
import com.eatnus.utils.api.EatNUSAPI
import com.eatnus.utils.helper.AppPreferencesHelper
import com.google.firebase.auth.FirebaseAuth

//SplashActivity.kt
/**
 * This is the splash screen page.
 *
 * @author Joseph Lee
 */
class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val customerHomePageActivity = Intent(this@SplashActivity,  HomeActivity::class.java)
        when {
            (AppPreferencesHelper().getUserRole() == "customer") && (EatNUSAPI.validate() == "Successful") && (FirebaseAuth.getInstance().currentUser != null) -> {
                startActivity(customerHomePageActivity)
                finish()
            }
            ((AppPreferencesHelper().getUserRole() == "vendor") || (AppPreferencesHelper().getUserRole() == "admin")) && (FirebaseAuth.getInstance().currentUser != null) -> {
                startActivity(customerHomePageActivity)
                finish()
            }
            else -> {
                val loginActivity = Intent(applicationContext,
                        LoginActivity::class.java)
                startActivity(loginActivity)
                finish()
            }
        }
    }
}
