package com.eatnus.ui.reward.profile.presenter

import com.eatnus.model.Coupon
import com.eatnus.model.Customer
import com.eatnus.ui.reward.profile.RewardProfileFragment
import com.eatnus.ui.reward.profile.adapter.RewardProfileAdapter
import com.eatnus.ui.reward.profile.contract.RewardProfilePresenterContract
import com.eatnus.utils.api.EatNUSAPI
import com.eatnus.utils.helper.AppPreferencesHelper
import com.google.android.gms.tasks.Tasks
import kotlinx.android.synthetic.main.fragment_reward_profile.*
import kotlin.math.roundToInt

class RewardProfilePresenter(private val view: RewardProfileFragment) :
  RewardProfilePresenterContract {

  override fun retrieveUserLevel() {
    val userDBSource = EatNUSAPI.getUser(AppPreferencesHelper().getUsername()!!)
    val lvlDBSource = EatNUSAPI.getLevels()
    val task = Tasks.whenAll(userDBSource, lvlDBSource)
    task.addOnSuccessListener {
      val userData = userDBSource.result
      val lvlData = lvlDBSource.result
      val customer = Customer()
      customer.currentLevel = userData.child("currentLevel").value.toString().toInt()
      customer.currentPts = userData.child("currentPts").value.toString().toInt()
      val minPts =
        lvlData.child(customer.currentLevel.toString()).child("minPts").value.toString().toFloat()
      val maxPts =
        lvlData.child(customer.currentLevel.toString()).child("maxPts").value.toString().toFloat()

      view.levelTextView.text = "Level ${customer.currentLevel}"
      view.expTextView.text = "(${(maxPts - customer.currentPts).roundToInt()} more points to level up)"
      when (customer.currentLevel) {
        1 -> view.userLevelProgressBar.animateProgress(1000, 0, ((customer.currentPts / maxPts) * 100).roundToInt())
        else -> view.userLevelProgressBar.animateProgress(1000, 0, (((customer.currentPts - minPts) / minPts) * 100).roundToInt())
      }
      val list = mutableListOf<Coupon>();
      userData.child("myCoupons").children.forEach {
        val coupon = Coupon()

        coupon.id = it.key.toString().toInt()
        coupon.title = it.child("title").value.toString()
        coupon.desc = it.child("desc").value.toString()
        coupon.ptsRequired = it.child("ptsRequired").value.toString().toInt()
        coupon.expiryDate = it.child("expiryDate").value.toString()
        list.add(coupon)
      }
      view.myCouponRecyclerView.adapter = RewardProfileAdapter(list, view)
      view.loadDialog.stop()
      view.isListEmpty(list.isEmpty())
    }
  }
}