package com.eatnus.ui.reward.profile.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.eatnus.R
import com.eatnus.model.Coupon
import com.eatnus.ui.reward.profile.RewardProfileFragment

class RewardProfileAdapter(
  private var items: List<Coupon>,
  private var fragment: RewardProfileFragment
) : RecyclerView.Adapter<ViewHolder>() {

  // Gets the number of animals in the list
  override fun getItemCount(): Int {
    return items.size
  }

  // Inflates the item views
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val view = LayoutInflater.from(fragment.context).inflate(R.layout.list_item_reward_profile, parent, false)
    return ViewHolder(view)
  }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    holder.couponTitleTextView.text = items[position].title
    holder.couponDescTextView.text = items[position].desc
    holder.couponExpiryDateTextView.text = "(Expires on ${items[position].expiryDate})"
  }
}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view){
  // Holds the TextView that will add each animal to
  val couponTitleTextView: TextView = view.findViewById(R.id.titleTextView)
  val couponDescTextView: TextView = view.findViewById(R.id.descTextView)
  val couponExpiryDateTextView: TextView = view.findViewById(R.id.expiryDateTextView)
}
