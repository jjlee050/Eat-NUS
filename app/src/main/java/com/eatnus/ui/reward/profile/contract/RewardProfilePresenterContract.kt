package com.eatnus.ui.reward.profile.contract

interface RewardProfilePresenterContract {
  fun retrieveUserLevel()
}