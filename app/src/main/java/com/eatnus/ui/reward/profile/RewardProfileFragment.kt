package com.eatnus.ui.reward.profile

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eatnus.R
import com.eatnus.ui.reward.profile.contract.RewardProfilePresenterContract
import com.eatnus.ui.reward.profile.presenter.RewardProfilePresenter
import com.eatnus.utils.ui.MyProgressDialog
import kotlinx.android.synthetic.main.fragment_reward_profile.*

class RewardProfileFragment: Fragment() {
  private var rewardActivity: AppCompatActivity? = null
  lateinit var loadDialog: MyProgressDialog

  override fun onAttach(context: Context?) {
    super.onAttach(context)
    if (context is AppCompatActivity)
      rewardActivity = context
  }

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    return inflater.inflate(R.layout.fragment_reward_profile, container, false)
  }

  override fun onViewCreated(rootView: View, savedInstanceState: Bundle?) {
    super.onViewCreated(rootView, savedInstanceState)

    myCouponRecyclerView.layoutManager = LinearLayoutManager(rewardActivity, LinearLayoutManager.VERTICAL, false)

    loadDialog = MyProgressDialog(rewardActivity!!, rootView.findViewById(R.id.newton_cradle_loading))
    loadDialog.start()

    closeViews()

    val rewardProfilePresenter: RewardProfilePresenterContract = RewardProfilePresenter(this)
    rewardProfilePresenter.retrieveUserLevel()

    swipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE)
    swipeRefreshLayout.setOnRefreshListener {
      loadDialog.start()
      closeViews()
      rewardProfilePresenter.retrieveUserLevel()
      swipeRefreshLayout.isRefreshing = false
    }
  }

  override fun onDetach() {
    super.onDetach()
    rewardActivity = null
  }


  private fun closeViews() {
    if(emptyLayout.visibility == View.VISIBLE) {
      emptyLayout.visibility = View.GONE
    }
    if(myCouponRecyclerView.visibility == View.VISIBLE) {
      myCouponRecyclerView.visibility = View.GONE
    }
  }

  fun isListEmpty(isEmpty: Boolean){
    if(isEmpty){
      emptyLayout.visibility = View.VISIBLE
      myCouponRecyclerView.visibility = View.GONE
    } else {
      emptyLayout.visibility = View.GONE
      myCouponRecyclerView.visibility = View.VISIBLE
    }
  }
}