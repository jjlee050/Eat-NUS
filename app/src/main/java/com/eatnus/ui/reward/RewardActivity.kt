package com.eatnus.ui.reward

import android.os.Bundle
import android.os.PersistableBundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.eatnus.R
import com.eatnus.ui.reward.adapter.RewardsPagerAdapter
import com.eatnus.utils.ui.MyViewPager

class RewardActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_reward)

    val tabLayout = findViewById<TabLayout>(R.id.tab_layout)
    val viewPager = findViewById<MyViewPager>(R.id.pager)
    val adapter: RewardsPagerAdapter

    viewPager.setSwipeable(true)

    tabLayout.tabGravity = TabLayout.GRAVITY_FILL

    //Profile Tab
    tabLayout.addTab(tabLayout.newTab().setIcon(resources.getDrawable(R.drawable.ic_profile_reward)).setText("Profile"))
    //Store Tab
    tabLayout.addTab(tabLayout.newTab().setIcon(resources.getDrawable(R.drawable.ic_store_reward)).setText("Store"))
    adapter = RewardsPagerAdapter(supportFragmentManager, tabLayout.tabCount)
    viewPager.adapter = adapter

    viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
    tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
      override fun onTabSelected(tab: TabLayout.Tab) {
        viewPager.adapter = adapter
        viewPager.currentItem = tab.position
      }

      override fun onTabUnselected(tab: TabLayout.Tab) {

      }

      override fun onTabReselected(tab: TabLayout.Tab) {

      }
    })
  }

  override fun onOptionsItemSelected(item: MenuItem?): Boolean {
    when (item?.itemId) {
      android.R.id.home -> {
        super.onBackPressed()
        return true
      }
    }
    return super.onOptionsItemSelected(item)
  }
}