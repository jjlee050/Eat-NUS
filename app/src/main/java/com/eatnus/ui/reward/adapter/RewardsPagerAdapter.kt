package com.eatnus.ui.reward.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.eatnus.ui.home.vendor.processing_order.VendorProcessingOrderFragment
import com.eatnus.ui.reward.profile.RewardProfileFragment
import com.eatnus.ui.reward.store.RewardStoreFragment


class RewardsPagerAdapter(fm: FragmentManager, numOfTabs: Int) : FragmentStatePagerAdapter(fm) {
    private var mNumOfTabs: Int = numOfTabs

    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> return RewardProfileFragment()
            1 -> return RewardStoreFragment()
        }
        return null
    }


    override fun getCount(): Int {
        return mNumOfTabs
    }
}
