package com.eatnus.ui.reward.store.presenter

import com.eatnus.model.Coupon
import com.eatnus.ui.reward.store.RewardStoreFragment
import com.eatnus.ui.reward.store.contract.RewardStorePresenterContract
import com.eatnus.utils.api.EatNUSAPI
import com.eatnus.utils.helper.AppPreferencesHelper
import com.google.android.gms.tasks.Tasks
import kotlinx.android.synthetic.main.fragment_reward_store.*
import java.text.SimpleDateFormat
import java.util.*

class RewardStorePresenter(private val view: RewardStoreFragment): RewardStorePresenterContract {
  override fun retrieveCoupons() : MutableList<Coupon>{
    val list = mutableListOf<Coupon>()
    val couponDBSource = EatNUSAPI.getCoupons()
    val userDBSource = EatNUSAPI.getUser(AppPreferencesHelper().getUsername()!!)
    val task = Tasks.whenAll(couponDBSource, userDBSource)
    task.addOnSuccessListener {
      val couponsData = couponDBSource.result
      val userData = userDBSource.result
      val myCouponList = mutableListOf<Int>()
      userData.child("myCoupons").children.forEach {
        myCouponList.add(it.key.toString().toInt())
      }
      couponsData.children.forEach {
        val coupon = Coupon()
        coupon.id = it.key.toString().toInt()
        coupon.title = it.child("title").value.toString()
        coupon.desc = it.child("desc").value.toString()
        coupon.ptsRequired = it.child("ptsRequired").value.toString().toInt()
        coupon.expiryDate = it.child("expiryDate").value.toString()

        val df = SimpleDateFormat("dd/MM/yyyy")
        if ((coupon.ptsRequired <= userData.child("currentPts").value.toString().toInt()) && ((df.parse(coupon.expiryDate)).after(Date())) && (!myCouponList.contains(coupon.id)))
          list.add(coupon)
        view.storeRecyclerView.adapter.notifyDataSetChanged()
      }
      view.loadDialog.stop()
      view.isListEmpty(list.isEmpty())
    }
    return list
  }

  override fun redeemCoupon(coupon: Coupon) {
    val df = SimpleDateFormat("dd/MM/yyyy")
    var calendar = GregorianCalendar()
    calendar.timeZone = TimeZone.getTimeZone("Asia/Singapore")
    calendar.time = Date()
    //Expires in 3 months
    calendar.add(Calendar.MONTH, 3)
    coupon.expiryDate = df.format(calendar.time)
    EatNUSAPI.getUserRef(AppPreferencesHelper().getUsername()!!).child("myCoupons").child(coupon.id.toString()).setValue(coupon)
  }
}