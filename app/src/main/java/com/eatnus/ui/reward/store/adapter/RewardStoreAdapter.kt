package com.eatnus.ui.reward.store.adapter

import android.support.v7.app.AlertDialog
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.eatnus.R
import com.eatnus.model.Coupon
import com.eatnus.ui.reward.store.RewardStoreFragment
import com.eatnus.ui.reward.store.contract.RewardStorePresenterContract

class RewardStoreAdapter(
  private var items: MutableList<Coupon>,
  private var fragment: RewardStoreFragment,
  private var presenter: RewardStorePresenterContract
) : RecyclerView.Adapter<ViewHolder>() {

  // Gets the number of animals in the list
  override fun getItemCount(): Int {
    return items.size
  }

  // Inflates the item views
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val view = LayoutInflater.from(fragment.context).inflate(R.layout.list_item_reward_store, parent, false)
    return ViewHolder(view)
  }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    holder.couponTitleTextView.text = items[position].title
    holder.couponDescTextView.text = items[position].desc
    holder.couponExpiryDateTextView.text = "(Expires on ${items[position].expiryDate})"
    holder.storeCouponCardView.setOnClickListener {
      presenter.redeemCoupon(items[position])
      items.remove(items[position])
      notifyItemRemoved(position)
      Toast.makeText(fragment.activity!!, "Coupon redeemed successfully", Toast.LENGTH_LONG).show()
    }

  }
}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view){
  // Holds the TextView that will add each animal to
  val storeCouponCardView: CardView = view.findViewById(R.id.storeCouponCardView)
  val couponTitleTextView: TextView = view.findViewById(R.id.titleTextView)
  val couponDescTextView: TextView = view.findViewById(R.id.descTextView)
  val couponExpiryDateTextView: TextView = view.findViewById(R.id.expiryDateTextView)

}
