package com.eatnus.ui.reward.store.contract

import com.eatnus.model.Coupon

interface RewardStorePresenterContract {
  fun retrieveCoupons() : MutableList<Coupon>
  fun redeemCoupon(coupon: Coupon)
}