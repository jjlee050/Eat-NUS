package com.eatnus.ui.reward.store

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eatnus.R
import com.eatnus.ui.reward.store.adapter.RewardStoreAdapter
import com.eatnus.ui.reward.store.contract.RewardStorePresenterContract
import com.eatnus.ui.reward.store.presenter.RewardStorePresenter
import com.eatnus.utils.ui.MyProgressDialog
import kotlinx.android.synthetic.main.fragment_reward_store.*

class RewardStoreFragment: Fragment() {
  private var rewardActivity: AppCompatActivity? = null
  lateinit var loadDialog: MyProgressDialog

  override fun onAttach(context: Context?) {
    super.onAttach(context)
    if (context is AppCompatActivity)
      rewardActivity = context
  }
  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    return inflater.inflate(R.layout.fragment_reward_store, container, false)
  }

  override fun onViewCreated(rootView: View, savedInstanceState: Bundle?) {
    super.onViewCreated(rootView, savedInstanceState)

    val rewardStorePresenter: RewardStorePresenterContract = RewardStorePresenter(this)
    loadDialog = MyProgressDialog(rewardActivity!!, rootView.findViewById(R.id.newton_cradle_loading))
    loadDialog.start()

    storeRecyclerView.layoutManager = LinearLayoutManager(rewardActivity, LinearLayoutManager.VERTICAL, false)
    storeRecyclerView.adapter = RewardStoreAdapter(rewardStorePresenter.retrieveCoupons(), this, rewardStorePresenter)

    closeViews()

    swipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE)
    swipeRefreshLayout.setOnRefreshListener {
      loadDialog.start()
      closeViews()
      storeRecyclerView.adapter = RewardStoreAdapter(rewardStorePresenter.retrieveCoupons(), this, rewardStorePresenter)
      swipeRefreshLayout.isRefreshing = false
    }
  }

  override fun onDetach() {
    super.onDetach()
    rewardActivity = null
  }


  private fun closeViews() {
    if(emptyLayout.visibility == View.VISIBLE) {
      emptyLayout.visibility = View.GONE
    }
    if(storeRecyclerView.visibility == View.VISIBLE) {
      storeRecyclerView.visibility = View.GONE
    }
  }

  fun isListEmpty(isEmpty: Boolean){
    if (isEmpty) {
      emptyLayout.visibility = View.VISIBLE
      storeRecyclerView.visibility = View.GONE
    } else {
      emptyLayout.visibility = View.GONE
      storeRecyclerView.visibility = View.VISIBLE
    }
  }
}