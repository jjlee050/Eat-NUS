package com.eatnus.ui.vendor_settings

import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.preference.*
import android.text.TextUtils
import android.view.MenuItem
import android.support.v4.app.NavUtils
import com.eatnus.R
import com.eatnus.utils.api.EatNUSAPI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import java.util.*
import android.preference.MultiSelectListPreference
import com.eatnus.App


/**
 * A [PreferenceActivity] that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 *
 * See [Android Design: Settings](http://developer.android.com/design/patterns/settings.html)
 * for design guidelines and the [Settings API Guide](http://developer.android.com/guide/topics/ui/settings.html)
 * for more information on developing a Settings UI.
 */
class SettingsActivity : AppCompatPreferenceActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActionBar()
    }

    /**
     * Set up the [android.app.ActionBar], if the API is available.
     */
    private fun setupActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onMenuItemSelected(featureId: Int, item: MenuItem): Boolean {
        when (item.itemId) {
          android.R.id.home -> {
            if (!super.onMenuItemSelected(featureId, item))
              NavUtils.navigateUpFromSameTask(this)
            else if (onIsMultiPane())
              finishFromChild(this)
            return true
          }
        }
        return super.onMenuItemSelected(featureId, item)
    }

    override fun onDestroy() {
        super.onDestroy()
        PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit()
    }

    /**
     * {@inheritDoc}
     */
    override fun onIsMultiPane(): Boolean {
        return isXLargeTablet(this)
    }

    /**
     * {@inheritDoc}
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    override fun onBuildHeaders(target: List<PreferenceActivity.Header>) {
        loadHeadersFromResource(R.xml.pref_headers, target)
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    override fun isValidFragment(fragmentName: String): Boolean {
        return PreferenceFragment::class.java.name == fragmentName
                || GeneralPreferenceFragment::class.java.name == fragmentName
                || StoreHoursDetailsPreferenceFragment::class.java.name == fragmentName
                || FinancialInfoPreferenceFragment::class.java.name == fragmentName
                || FoodDetailsPreferenceFragment::class.java.name == fragmentName
                || StallInactivityPeriodPreferenceFragment::class.java.name == fragmentName
    }

    companion object {
        /**
         * A preference value change listener that updates the preference's summary
         * to reflect its new value.
         */
        private val sBindPreferenceSummaryToValueListener = Preference.OnPreferenceChangeListener { preference, value ->
            val stringValue = value.toString()
            val email = FirebaseAuth.getInstance().currentUser?.email
            val username = email?.substring(0, email.indexOf("@"))

            if (preference is ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                val listPreference = preference
                val index = listPreference.findIndexOfValue(stringValue)

                // Set the summary to reflect the new value.
                preference.setSummary(
                        if (index >= 0)
                            listPreference.entries[index]
                        else
                            null)

            } else if (preference is RingtonePreference) {
                // For ringtone preferences, look up the correct display value
                // using RingtoneManager.
                if (TextUtils.isEmpty(stringValue)) {
                    // Empty values correspond to 'silent' (no ringtone).
                    preference.setSummary(R.string.pref_ringtone_silent)

                } else {
                    val ringtone = RingtoneManager.getRingtone(
                            preference.getContext(), Uri.parse(stringValue))

                    if (ringtone == null) {
                        // Clear the summary if there was a lookup error.
                        preference.setSummary(null)
                    } else {
                        // Set the summary to reflect the new ringtone display
                        // name.
                        val name = ringtone.getTitle(preference.getContext())
                        preference.setSummary(name)
                    }
                }

            } else if(preference.key.contains(username.toString())) {
                    val foodID = preference.key.substring(preference.key.indexOf("_") + 1)
                    EatNUSAPI.getFoodRef(foodID).child("total_qty").setValue(stringValue)
                    preference.summary = "Quantity: $stringValue"
            } else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                when(preference.key) {
                    "vendor_name" -> {
                        preference.summary = stringValue
                        EatNUSAPI.getStallsRef().addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onCancelled(p0: DatabaseError) {
                                println(p0)
                            }

                            override fun onDataChange(p0: DataSnapshot) {
                                p0.children.forEach {
                                    if ((it.child("username").value.toString().toLowerCase() == username) && (stringValue.isNotEmpty())) {
                                        it.child("name").ref.setValue(stringValue)
                                    }
                                }
                            }

                        })
                    }
                    "vendor_phone" -> {
                        preference.summary = stringValue
                        EatNUSAPI.getStallsRef().addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onCancelled(p0: DatabaseError) {
                                println(p0)
                            }

                            override fun onDataChange(p0: DataSnapshot) {
                                p0.children.forEach {
                                    if ((it.child("username").value.toString().toLowerCase() == username) && (stringValue.isNotEmpty())) {
                                        it.child("phone").ref.setValue(stringValue)
                                    }
                                }
                            }
                        })
                    }
                    "allow_cash" -> {
                        EatNUSAPI.getStallsRef().addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onCancelled(p0: DatabaseError) {
                                println(p0)
                            }

                            override fun onDataChange(p0: DataSnapshot) {
                                p0.children.forEach {
                                    if ((it.child("username").value.toString().toLowerCase() == username) && (stringValue.isNotEmpty())) {
                                        it.child("allow_cash").ref.setValue(stringValue.toBoolean())
                                    }
                                }
                            }
                        })
                    }
                    "takeaway_price" -> {
                        preference.summary = "$ $stringValue"
                        EatNUSAPI.getStallsRef().addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onCancelled(p0: DatabaseError) {
                                println(p0)
                            }

                            override fun onDataChange(p0: DataSnapshot) {
                                p0.children.forEach {
                                    if ((it.child("username").value.toString().toLowerCase() == username) && (stringValue.isNotEmpty())) {
                                        it.child("takeaway_price").ref.setValue("%.2f".format(stringValue.toFloat()))
                                    }
                                }
                            }
                        })
                    }
                    "store_opening_hours" -> {
                        preference.summary = stringValue
                        EatNUSAPI.getStallsRef().addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onCancelled(p0: DatabaseError) {
                                println(p0)
                            }

                            override fun onDataChange(p0: DataSnapshot) {
                                p0.children.forEach {
                                    if ((it.child("username").value.toString().toLowerCase() == username) && (stringValue.isNotEmpty())) {
                                        it.child("operating_start").ref.setValue(stringValue)
                                    }
                                }
                            }
                        })
                    }
                    "store_closing_hours" -> {
                        preference.summary = stringValue
                        EatNUSAPI.getStallsRef().addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onCancelled(p0: DatabaseError) {
                                println(p0)
                            }

                            override fun onDataChange(p0: DataSnapshot) {
                                p0.children.forEach {
                                    if ((it.child("username").value.toString().toLowerCase() == username) && (stringValue.isNotEmpty())) {
                                        it.child("operating_end").ref.setValue(stringValue)
                                    }
                                }
                            }
                        })
                    }
                    "list_unable_periods" -> {
                        // For multi select list preferences we should show a list of the selected options
                        val listPreference = preference as MultiSelectListPreference
                        val values = listPreference.entries
                        val options = StringBuilder()
                        val set = TreeSet<String>(value as HashSet<String>)
                        for (stream in set) {
                            val index = listPreference.findIndexOfValue(stream)

                            if (index >= 0) {
                                if (options.isNotEmpty()) {
                                    options.append(", ")
                                }
                                options.append(values[index])
                            }
                        }

                        preference.summary = options
                        val periodList = mutableListOf<String>()
                        for(period in value.toMutableList()) {
                            if(listPreference.findIndexOfValue(period) >= 0)
                                periodList.add(period)
                        }
                        periodList.sort()
                        EatNUSAPI.getStallsRef().addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onCancelled(p0: DatabaseError) {
                                println(p0)
                            }

                            override fun onDataChange(p0: DataSnapshot) {
                                p0.children.forEach {
                                    if ((it.child("username").value.toString().toLowerCase() == username) && (periodList.isNotEmpty())){
                                        it.child("inactivity_periods").ref.setValue(periodList)
                                    }
                                }
                            }
                        })
                    }
                }
            }
            true
        }

        /**
         * Helper method to determine if the device has an extra-large screen. For
         * example, 10" tablets are extra-large.
         */
        private fun isXLargeTablet(context: Context): Boolean {
            return context.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK >= Configuration.SCREENLAYOUT_SIZE_XLARGE
        }

        /**
         * Binds a preference's summary to its value. More specifically, when the
         * preference's value is changed, its summary (line of text below the
         * preference title) is updated to reflect the value. The summary is also
         * immediately updated upon calling this method. The exact display format is
         * dependent on the type of preference.

         * @see .sBindPreferenceSummaryToValueListener
         */
        internal fun bindPreferenceSummaryToValue(preference: Preference) {
            // Set the listener to watch for value changes.
            preference.onPreferenceChangeListener = sBindPreferenceSummaryToValueListener

            if(preference is SwitchPreference){
                /*sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                        PreferenceManager
                                .getDefaultSharedPreferences(preference.context)
                                .getBoolean(preference.key, ""))*/
            } else if(preference is MultiSelectListPreference) {
                /*sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                        PreferenceManager
                                .getDefaultSharedPreferences(preference.context)
                                .getStringSet(preference.key, HashSet<String>()))*/
            } else {
                // Trigger the listener immediately with the preference's
                // current value.
                /*sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                        PreferenceManager
                                .getDefaultSharedPreferences(preference.context)
                                .getString(preference.key, ""))*/
            }
        }
    }
}
