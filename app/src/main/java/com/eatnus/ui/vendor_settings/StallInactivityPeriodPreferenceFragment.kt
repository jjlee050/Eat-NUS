package com.eatnus.ui.vendor_settings

import android.annotation.TargetApi
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.preference.MultiSelectListPreference
import android.preference.PreferenceFragment
import android.view.MenuItem
import com.eatnus.R
import com.eatnus.utils.api.EatNUSAPI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import java.text.SimpleDateFormat
import java.util.*

/**
 * This fragment shows data and sync preferences only. It is used when the
 * activity is showing a two-pane settings UI.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
class StallInactivityPeriodPreferenceFragment : PreferenceFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        addPreferencesFromResource(R.xml.pref_stall_inactivity_period)
        setHasOptionsMenu(true)

        SettingsActivity.bindPreferenceSummaryToValue(findPreference("list_unable_periods"))

        val email = FirebaseAuth.getInstance().currentUser?.email
        val username = email?.substring(0, email.indexOf("@"))

        EatNUSAPI.getStallsRef().addListenerForSingleValueEvent(object: ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                println(p0)
            }

            override fun onDataChange(p0: DataSnapshot) {
                p0.children.forEach {
                    if(it.child("username").value.toString().toLowerCase() == username) {
                        val operatingEnd = it.child("operating_end").value.toString()
                        val operatingStart = it.child("operating_start").value.toString()
                        var periodList: List<String> = ArrayList()
                        if (it.child("inactivity_periods").exists())
                            periodList = it.child("inactivity_periods").value as MutableList<String>

                        val sdf = SimpleDateFormat("HH:mm")
                        val timingStart = sdf.parse(operatingStart)
                        val timingEnd = sdf.parse(operatingEnd)
                        val inactivityPeriodList = mutableListOf<String>()
                        val calendarStart = Calendar.getInstance(TimeZone.getTimeZone("Asia/Singapore"))
                        calendarStart.time = timingStart

                        do {
                            calendarStart.add(Calendar.HOUR_OF_DAY, 1)
                            if(calendarStart.time != timingEnd)
                                inactivityPeriodList.add(sdf.format(calendarStart.time))
                        } while (calendarStart.time.before(timingEnd))

                        (findPreference("list_unable_periods") as MultiSelectListPreference).title = "Inactivity period(s)"
                        (findPreference("list_unable_periods") as MultiSelectListPreference).entries = inactivityPeriodList.toTypedArray()
                        (findPreference("list_unable_periods") as MultiSelectListPreference).entryValues = inactivityPeriodList.toTypedArray()
                        (findPreference("list_unable_periods") as MultiSelectListPreference).values = periodList.toSet()

                        val listPreference = (findPreference("list_unable_periods") as MultiSelectListPreference)
                        for(period in periodList) {
                            //listPreference.entries[listPreference.findIndexOfValue(period)].
                        }
                        listPreference.setDefaultValue(periodList)
                        val options = StringBuilder()
                        val set = TreeSet<String>(listPreference.values)
                        set.forEach {
                            val index = listPreference.findIndexOfValue(it)
                            if (index >= 0) {
                                if (options.isNotEmpty()) {
                                    options.append(", ")
                                }
                                options.append(it)
                            }
                        }
                        (findPreference("list_unable_periods") as MultiSelectListPreference).summary = options

                        return@forEach
                    }
                }
            }

        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            startActivity(Intent(activity, SettingsActivity::class.java))
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
