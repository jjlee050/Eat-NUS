package com.eatnus.ui.vendor_settings

import android.annotation.TargetApi
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.preference.EditTextPreference
import android.preference.PreferenceFragment
import android.text.InputType
import android.view.MenuItem
import com.eatnus.R
import com.eatnus.utils.api.EatNUSAPI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener

/**
 * This fragment shows general preferences only. It is used when the
 * activity is showing a two-pane settings UI.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
class FoodDetailsPreferenceFragment : PreferenceFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.pref_food_details)
        setHasOptionsMenu(true)

        val email = FirebaseAuth.getInstance().currentUser?.email
        val username = email?.substring(0, email.indexOf("@"))

        //Need to add food from this stall then show its owner
        EatNUSAPI.getStallsRef().addListenerForSingleValueEvent(object: ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                println(p0)
            }

            override fun onDataChange(p0: DataSnapshot) {
                p0.children.forEach {
                    if(it.child("username").value.toString().toLowerCase() == username) {
                        val stallID = it.key.toString()
                        EatNUSAPI.getFoode().addListenerForSingleValueEvent(object: ValueEventListener {
                            override fun onCancelled(p0: DatabaseError) {
                                println(p0)
                            }

                            override fun onDataChange(p0: DataSnapshot) {
                                p0.children.forEach {
                                    if(it.child("stall_id").value.toString() == stallID) {
                                        val qty = it.child("total_qty").value.toString()
                                        val name = it.child("name").value.toString()
                                        val foodPreference = EditTextPreference(preferenceScreen.context)
                                        foodPreference.editText.inputType = InputType.TYPE_CLASS_NUMBER
                                        foodPreference.editText.maxLines = 1
                                        foodPreference.editText.setSingleLine(true)
                                        foodPreference.editText.setSelectAllOnFocus(true)
                                        foodPreference.key = username + "_" + it.key.toString()
                                        foodPreference.title = name
                                        foodPreference.summary = "Quantity: $qty"
                                        foodPreference.setDefaultValue(qty)
                                        preferenceScreen.addPreference(foodPreference)
                                        SettingsActivity.bindPreferenceSummaryToValue(findPreference(foodPreference.key))
                                        foodPreference.dialogTitle = "Set food quantity"
                                    }
                                }
                            }
                        })
                        return@forEach
                    }
                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            startActivity(Intent(activity, SettingsActivity::class.java))
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}