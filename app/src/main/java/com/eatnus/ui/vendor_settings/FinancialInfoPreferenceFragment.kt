package com.eatnus.ui.vendor_settings

import android.annotation.TargetApi
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceFragment
import android.preference.SwitchPreference
import android.view.MenuItem
import com.eatnus.R
import com.eatnus.utils.api.EatNUSAPI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener

/**
 * This fragment shows notification preferences only. It is used when the
 * activity is showing a two-pane settings UI.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
class FinancialInfoPreferenceFragment : PreferenceFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        addPreferencesFromResource(R.xml.pref_financial_info)
        setHasOptionsMenu(true)

        SettingsActivity.bindPreferenceSummaryToValue(findPreference("allow_cash"))
        SettingsActivity.bindPreferenceSummaryToValue(findPreference("takeaway_price"))

        val email = FirebaseAuth.getInstance().currentUser?.email
        val username = email?.substring(0, email.indexOf("@"))

        EatNUSAPI.getStallsRef().addListenerForSingleValueEvent(object: ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                println(p0)
            }

            override fun onDataChange(p0: DataSnapshot) {
                p0.children.forEach{
                    if(it.child("username").value.toString().toLowerCase() == username) {
                        val switch: SwitchPreference = findPreference("allow_cash") as SwitchPreference
                        switch.setDefaultValue(it.child("allow_cash").value.toString().toBoolean())
                        switch.isChecked = it.child("allow_cash").value.toString().toBoolean()
                        findPreference("takeaway_price").summary = "$ ${it.child("takeaway_price").value.toString()}"
                        findPreference("takeaway_price").setDefaultValue(it.child("takeaway_price").value.toString())
                        return@forEach
                    }
                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            startActivity(Intent(activity, SettingsActivity::class.java))
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}