package com.eatnus.ui.vendor_settings

import android.annotation.TargetApi
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceFragment
import android.view.MenuItem
import com.eatnus.R
import com.eatnus.utils.api.EatNUSAPI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener

/**
 * This fragment shows general preferences only. It is used when the
 * activity is showing a two-pane settings UI.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
class GeneralPreferenceFragment : PreferenceFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        addPreferencesFromResource(R.xml.pref_profile)
        setHasOptionsMenu(true)

        SettingsActivity.bindPreferenceSummaryToValue(findPreference("vendor_name"))
        SettingsActivity.bindPreferenceSummaryToValue(findPreference("vendor_phone"))

        val email = FirebaseAuth.getInstance().currentUser?.email
        val username = email?.substring(0, email.indexOf("@"))
        //Attempt to place default value here first.
        EatNUSAPI.getStallsRef().addListenerForSingleValueEvent(object: ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                println(p0)
            }

            override fun onDataChange(p0: DataSnapshot) {
                p0.children.forEach{
                    if(it.child("username").value.toString().toLowerCase() == username) {
                        findPreference("vendor_phone").summary = it.child("phone").value.toString()
                        findPreference("vendor_name").summary = it.child("name").value.toString()
                        findPreference("vendor_phone").setDefaultValue(it.child("phone").value.toString())
                        findPreference("vendor_name").setDefaultValue(it.child("name").value.toString())
                        return@forEach
                    }
                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            startActivity(Intent(activity, SettingsActivity::class.java))
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}