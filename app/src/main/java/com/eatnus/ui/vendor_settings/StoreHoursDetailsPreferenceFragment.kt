package com.eatnus.ui.vendor_settings

import android.annotation.TargetApi
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.preference.MultiSelectListPreference
import android.preference.PreferenceFragment
import android.view.MenuItem
import com.eatnus.R
import com.eatnus.utils.api.EatNUSAPI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import java.text.SimpleDateFormat
import java.util.*


/**
 * This fragment shows data and sync preferences only. It is used when the
 * activity is showing a two-pane settings UI.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
class StoreHoursDetailsPreferenceFragment : PreferenceFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        addPreferencesFromResource(R.xml.pref_store_hours_details)
        setHasOptionsMenu(true)

        SettingsActivity.bindPreferenceSummaryToValue(findPreference("store_opening_hours"))
        SettingsActivity.bindPreferenceSummaryToValue(findPreference("store_closing_hours"))

        val email = FirebaseAuth.getInstance().currentUser?.email
        val username = email?.substring(0, email.indexOf("@"))

        EatNUSAPI.getStallsRef().addListenerForSingleValueEvent(object: ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                println(p0)
            }

            override fun onDataChange(p0: DataSnapshot) {
                p0.children.forEach {
                    if(it.child("username").value.toString().toLowerCase() == username) {
                        val operatingEnd = it.child("operating_end").value.toString()
                        val operatingStart = it.child("operating_start").value.toString()
                        findPreference("store_opening_hours").summary = operatingStart
                        findPreference("store_opening_hours").setDefaultValue(operatingStart)
                        findPreference("store_closing_hours").summary = operatingEnd
                        findPreference("store_closing_hours").setDefaultValue(operatingEnd)
                        return@forEach
                    }
                }
            }

        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            startActivity(Intent(activity, SettingsActivity::class.java))
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
