package com.eatnus.ui.order_food.available_stall.contract

import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import android.widget.TextView
import com.eatnus.model.Stall

interface AvailableStallPresenterContract {
    fun retrieveAvailableStall(availableStallRecyclerView: RecyclerView, amenitiesID: Int): MutableList<Stall>

    fun retrieveStallQueue(stall: Stall, imageView: ImageView, textView: TextView)
}