package com.eatnus.ui.order_food.confirm_order.contract

import com.eatnus.model.Food
import com.eatnus.model.FoodBasket

interface ConfirmOrderPresenterContract {
    fun calculateTotal(myBasket: Map<Food, FoodBasket>): Float
}