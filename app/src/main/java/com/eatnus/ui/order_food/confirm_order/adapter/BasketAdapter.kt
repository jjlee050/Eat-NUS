package com.eatnus.ui.order_food.confirm_order.adapter

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.eatnus.R
import com.eatnus.model.Food
import com.eatnus.model.FoodBasket
import java.math.RoundingMode
import java.text.DecimalFormat

class BasketAdapter(private val foodItems: Map<Food, FoodBasket>, val context: Context?) : RecyclerView.Adapter<ViewHolder>() {

    override fun getItemCount(): Int {
        return foodItems.size
    }

    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_item_confirm_order, parent, false)
        return ViewHolder(view)
    }

    // Binds each animal in the ArrayList to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var decimalFormat = DecimalFormat("0.00")
        holder.foodNameTextView.text = foodItems.keys.toList()[position].name
        holder.qtyTextView.text = foodItems.values.toList()[position].qty.toString() + " x"
        holder.priceTextView.text = "$ " + decimalFormat.format(foodItems.values.toList()[position].price.toBigDecimal().setScale(2, RoundingMode.UP))
    }

}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view){
    val foodNameTextView: TextView = view.findViewById(R.id.foodNameTextView)
    val priceTextView: TextView = view.findViewById(R.id.priceTextView)
    val qtyTextView: TextView = view.findViewById(R.id.qtyTextView)
}