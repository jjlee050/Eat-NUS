package com.eatnus.ui.order_food.payment.presenter

import android.content.Intent
import android.support.v4.content.res.TypedArrayUtils.getString
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.eatnus.R
import com.eatnus.model.Order
import com.eatnus.ui.home.HomeActivity
import com.eatnus.ui.order_food.confirm_order.ConfirmOrderFragment
import com.eatnus.ui.order_food.payment.PaymentSelectFragment
import com.eatnus.ui.order_food.payment.contract.PaymentSelectPresenterContract
import com.eatnus.utils.OrderStatus
import com.eatnus.utils.api.EatNUSAPI
import com.eatnus.utils.notification.EatNUSNotificationManager
import com.eatnus.utils.ui.MyAlertDialog
import com.eatnus.utils.ui.MyProgressDialog
import com.google.firebase.database.DataSnapshot
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.android.gms.wallet.*
import com.stripe.Stripe
import com.stripe.exception.*
import com.stripe.model.Charge
import java.util.HashMap

class PaymentSelectPresenter(
  paymentSelectView: PaymentSelectFragment,
  confirmOrderFragment: ConfirmOrderFragment,
  orderFoodActivity: AppCompatActivity,
  dialog: MyProgressDialog,
  client: PaymentsClient
) : PaymentSelectPresenterContract {
  private var view = paymentSelectView
  private var confirmOrderView = confirmOrderFragment
  private var orderActivity = orderFoodActivity
  private var loadDialog = dialog
  private val mPaymentsClient = client

  override fun allowCashPayment(stallID: Int) {
    val stallDBSource: Task<DataSnapshot> = EatNUSAPI.getStall(stallID.toString())
    val task = Tasks.whenAll(stallDBSource)
    task.addOnSuccessListener {
      val stallData = stallDBSource.result
      if (stallData.child("allow_cash").value.toString().toBoolean()) {
        view.cashBtn.visibility = View.VISIBLE
      } else {
        view.cashBtn.visibility = View.GONE
      }
      loadDialog.stop()
    }
    task.addOnFailureListener {
      loadDialog.stop()
      MyAlertDialog().createSimpleErrorDialog(
        orderActivity,
        "Error",
        "Unable to retrieve store details."
      )
    }
  }

  override fun payByCash(order: Order, total: String) {
    val stallDBSource: Task<DataSnapshot> = EatNUSAPI.getStall(order.stallID.toString())
    val task = Tasks.whenAll(stallDBSource)
    task.addOnSuccessListener {
      val stallData = stallDBSource.result
      val amenityDBSource = EatNUSAPI.getAmenity(stallData.child("amenities_id").value.toString())
      val newTask = Tasks.whenAll(amenityDBSource)
      newTask.addOnSuccessListener {
        val amenityData = amenityDBSource.result
        val mBuilder = EatNUSNotificationManager()
          .createNotification(
            confirmOrderView.context!!, "0",
            "Your order has been sent successfully.",
            "Please pay ${total.trim()} to ${stallData.child("name").value} at ${amenityData.child("name").value}",
            "", false, true
          )
        EatNUSNotificationManager().postNotifications(
          confirmOrderView.context!!,
          order.orderID,
          mBuilder.build()
        )
        updateStatusViaCash(order.orderID)
      }
      newTask.addOnFailureListener {ex ->
        loadDialog.stop()
        MyAlertDialog().createSimpleErrorDialog(
          orderActivity,
          "Error",
          "Unable to retrieve amenities details."
        )
        Log.e("Eat@NUSError", ex.toString())
      }
    }
  }

  override fun payByGoogleWallet(total: String, orderID: Int, tokenId: String): String? {
    val debug = EatNUSAPI.Debug
    try {
      val params = HashMap<String, Any>()
      Stripe.apiKey = EatNUSAPI.StripeSecretKey
      if (debug) {
        params["amount"] = total
        params["currency"] = "sgd"
        params["description"] = "Order #$orderID from ${view.getString(R.string.app_name)}"
        params["source"] = "tok_visa"
      } else {
        params["amount"] = total
        params["currency"] = "sgd"
        params["description"] = "Order #$orderID from ${view.getString(R.string.app_name)}"
        params["source"] = tokenId
      }
      return Charge.create(params).id
    } catch (e: AuthenticationException) {
      e.printStackTrace()
      return ""
    } catch (e: InvalidRequestException) {
      e.printStackTrace()
      return ""
    } catch (e: APIConnectionException) {
      e.printStackTrace()
      return ""
    } catch (e: CardException) {
      e.printStackTrace()
      return ""
    } catch (e: APIException) {
      e.printStackTrace()
      return ""
    }
  }

  override fun allowGoogleWalletPayment() {
    val request = IsReadyToPayRequest.newBuilder()
      .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
      .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
      .build()
    val mTask = mPaymentsClient.isReadyToPay(request)
    mTask.addOnCompleteListener { task ->
      try {
        val result = task.getResult(ApiException::class.java)
        if (result == true) {
          //show Google as payment option
          view.googleWalletButton.visibility = View.VISIBLE
        } else {
          //hide Google as payment option
          view.googleWalletButton.visibility = View.GONE
        }
      } catch (exception: ApiException) {
      }
    }
  }

  override fun updateStatusViaCash(orderID: Int) {
    EatNUSAPI.getOrderRef(orderID.toString()).child("status").ref.setValue(OrderStatus.ORDER_SENT)
    EatNUSAPI.getOrderRef(orderID.toString()).child("cash").ref.setValue(true)
    redirectMainPage()
  }

  override fun updateStatusViaGooglePay(orderID: Int, chargeId: String) {
    EatNUSAPI.getOrderRef(orderID.toString()).child("status").ref.setValue(OrderStatus.ORDER_SENT)
    EatNUSAPI.getOrderRef(orderID.toString()).child("cash").ref.setValue(false)
    EatNUSAPI.getOrderRef(orderID.toString()).child("googlePayOrderID").ref.setValue(chargeId)
    redirectMainPage()
  }

  override fun updateStatusViaDBSPaylah(orderID: Int, dbsRefId: String) {
    EatNUSAPI.getOrderRef(orderID.toString()).child("status").ref.setValue(OrderStatus.ORDER_SENT)
    EatNUSAPI.getOrderRef(orderID.toString()).child("cash").ref.setValue(false)
    EatNUSAPI.getOrderRef(orderID.toString()).child("dbsRefID").ref.setValue(dbsRefId)
    redirectMainPage()
  }

  override fun redirectMainPage() {
    //Clear all the fragment and redirect back to main page.
    for (i in 1..confirmOrderView.childFragmentManager.backStackEntryCount) {
      confirmOrderView.childFragmentManager.popBackStack()
    }
    for (i in 1..orderActivity.supportFragmentManager!!.backStackEntryCount) {
      orderActivity.supportFragmentManager?.popBackStack()
    }

    loadDialog.stop()

    val customerHomePageActivity = Intent(orderActivity, HomeActivity::class.java)
    orderActivity.startActivity(customerHomePageActivity)
    orderActivity.finish()
  }
}