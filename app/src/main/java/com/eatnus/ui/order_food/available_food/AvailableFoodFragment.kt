package com.eatnus.ui.order_food.available_food

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import com.eatnus.R
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks
import com.github.ksoichiro.android.observablescrollview.ScrollState
import com.github.ksoichiro.android.observablescrollview.ScrollUtils
import com.nineoldandroids.view.ViewHelper
import android.widget.TextView
import android.graphics.drawable.ClipDrawable
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.*
import com.eatnus.model.Stall
import com.eatnus.ui.order_food.available_food.adapter.FoodAdapter
import com.eatnus.ui.order_food.available_food.contract.AvailableFoodPresenterContract
import com.eatnus.ui.order_food.available_food.presenter.AvailableFoodPresenter
import com.eatnus.utils.ui.MyAlertDialog
import com.eatnus.utils.ui.MyProgressDialog

/**
 * A simple [Fragment] subclass.
 */
class AvailableFoodFragment : Fragment(), ObservableScrollViewCallbacks {
    private lateinit var orderFoodPresenter: AvailableFoodPresenterContract
    private var orderFoodActivity: AppCompatActivity? = null
    private lateinit var loadDialog: MyProgressDialog
    private var amenitiesID: Int = 0
    private var stall = Stall()

    private lateinit var mImageView: ImageView
    private lateinit var mToolbarView: Toolbar
    private lateinit var mScrollView: ObservableScrollView
    private var mParallaxImageHeight: Int = 0

    private lateinit var menuItem: MenuItem

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is AppCompatActivity)
            this.orderFoodActivity = context
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onScrollChanged(mScrollView.currentScrollY, false, false)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_available_food, container, false)
    }

    override fun onViewCreated(rootView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(rootView, savedInstanceState)

        this.amenitiesID = arguments!!.getInt("amenitiesID")

        stall.id = arguments!!.getInt("stallID")
        stall.name = arguments!!.getString("stallName")
        stall.phone = arguments!!.getString("stallPhone")

        loadDialog = MyProgressDialog(orderFoodActivity!!, rootView.findViewById(R.id.newton_cradle_loading))
        loadDialog.start()

        (activity as AppCompatActivity).supportActionBar?.hide()
        mImageView = rootView.findViewById(R.id.image)
        mToolbarView = rootView.findViewById(R.id.toolbar)

        mToolbarView.title = stall.name

        val foodStallNameTextView: TextView = rootView.findViewById(R.id.foodStallNameTextView)
        foodStallNameTextView.text = stall.name

        val foodStallPhoneTextView: TextView = rootView.findViewById(R.id.foodStallPhoneTextView)
        foodStallPhoneTextView.text = stall.phone
        orderFoodPresenter = AvailableFoodPresenter(orderFoodActivity!!, this@AvailableFoodFragment, loadDialog)

        mToolbarView.setBackgroundColor(ScrollUtils.getColorWithAlpha(0.0f, resources.getColor(R.color.colorPrimary)));
        mToolbarView.setTitleTextColor(ScrollUtils.getColorWithAlpha(0.0f, resources.getColor(R.color.colorWhite)))

        mScrollView = rootView.findViewById(R.id.scroll)
        mScrollView.setScrollViewCallbacks(this)

        mParallaxImageHeight = resources.getDimensionPixelSize(R.dimen.parallax_image_height)

        orderFoodPresenter.retrieveStallQueue(stall, rootView.findViewById(R.id.queueTimingImageView), rootView.findViewById(R.id.queueTextView))

        mToolbarView.navigationIcon = resources.getDrawable(R.drawable.abc_ic_ab_back_material)
        mToolbarView.setNavigationOnClickListener {
            orderFoodActivity!!.onBackPressed()
        }

        menuItem = mToolbarView.menu.add(0,0,0,"Confirm")
        menuItem.setIcon(R.drawable.ic_check_light).
                setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)


        val itemDecor = DividerItemDecoration(activity, ClipDrawable.HORIZONTAL)

        val foodMenuRecyclerView = rootView.findViewById<RecyclerView>(R.id.foodMenuRecyclerView)

        foodMenuRecyclerView.addItemDecoration(itemDecor)
        foodMenuRecyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        foodMenuRecyclerView.adapter = FoodAdapter(orderFoodPresenter.retrieveAvailableFood(foodMenuRecyclerView, stall.id), this@AvailableFoodFragment, orderFoodActivity!!)

        menuItem.setOnMenuItemClickListener {
            when(it.itemId) {
                0 -> {
                    val myBasket = (foodMenuRecyclerView.adapter as FoodAdapter).basketTable
                    if(myBasket.size > 0) {
                        loadDialog.start()
                        orderFoodPresenter.confirmOrder(myBasket)
                    }
                    else {
                        MyAlertDialog().createSimpleErrorDialog(orderFoodActivity!!, "Error", "Please select a menu item.")
                    }
                }
            }

            true
        }
    }

    override fun onScrollChanged(scrollY: Int, firstScroll: Boolean, dragging: Boolean) {
        val baseColor = resources.getColor(R.color.colorPrimary)
        val whiteColor = resources.getColor(R.color.colorWhite)
        val alpha = Math.min(1.00f, (scrollY.toFloat() / mParallaxImageHeight))
        mToolbarView.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, baseColor))
        mToolbarView.setTitleTextColor(ScrollUtils.getColorWithAlpha(alpha, whiteColor))
        ViewHelper.setTranslationY(mImageView, (scrollY / 10).toFloat())
    }

    override fun onDownMotionEvent() {
    }

    override fun onUpOrCancelMotionEvent(scrollState: ScrollState?) {
    }

    override fun onDetach() {
        super.onDetach()
        this.orderFoodActivity = null
    }

    override fun onDestroyView() {
        (activity as AppCompatActivity).supportActionBar?.show()
        super.onDestroyView()
    }

}