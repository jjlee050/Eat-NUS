package com.eatnus.ui.order_food.available_amenities

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ClipDrawable.HORIZONTAL
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eatnus.R
import android.support.v7.widget.DividerItemDecoration
import com.eatnus.ui.order_food.available_amenities.adapter.AmenitiesAdapter
import com.eatnus.ui.order_food.available_amenities.contract.AvailableAmenitiesPresenterContract
import com.eatnus.ui.order_food.available_amenities.presenter.AvailableAmenitiesPresenter
import com.eatnus.utils.ui.MyProgressDialog

/**
 * A simple [Fragment] subclass.
 */
class AvailableAmenitiesFragment : Fragment() {
    private lateinit var orderFoodPresenter: AvailableAmenitiesPresenterContract
    private var orderFoodActivity: AppCompatActivity? = null
    private lateinit var availableAmenitiesRecyclerView: RecyclerView
    private lateinit var emptyLayout: View
    private lateinit var loadDialog: MyProgressDialog

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is AppCompatActivity)
            this.orderFoodActivity = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_available_amenities, container, false)
    }

    override fun onViewCreated(rootView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(rootView, savedInstanceState)

        loadDialog = MyProgressDialog(orderFoodActivity!!, rootView.findViewById(R.id.newton_cradle_loading))
        loadDialog.start()

        orderFoodPresenter = AvailableAmenitiesPresenter(orderFoodActivity!!, this@AvailableAmenitiesFragment, loadDialog)
        availableAmenitiesRecyclerView = rootView.findViewById(R.id.availableAmenitiesRecyclerView)
        val itemDecor = DividerItemDecoration(activity, HORIZONTAL)

        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.title_order_food_1)

        emptyLayout = rootView.findViewById<View>(R.id.emptyLayout)
        closeViews()

        availableAmenitiesRecyclerView.addItemDecoration(itemDecor)
        availableAmenitiesRecyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        availableAmenitiesRecyclerView.adapter = AmenitiesAdapter(orderFoodPresenter.retrieveAvailableAmenities(availableAmenitiesRecyclerView), this@AvailableAmenitiesFragment)

        val swipeRefreshLayout = rootView.findViewById<SwipeRefreshLayout>(R.id.swipeRefreshLayout)
        swipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE)
        swipeRefreshLayout.setOnRefreshListener {
            loadDialog.start()
            closeViews()
            availableAmenitiesRecyclerView.adapter = AmenitiesAdapter(orderFoodPresenter.retrieveAvailableAmenities(availableAmenitiesRecyclerView), this@AvailableAmenitiesFragment)
            swipeRefreshLayout.isRefreshing = false
        }
    }

    override fun onDetach() {
        super.onDetach()
        this.orderFoodActivity = null
    }

    private fun closeViews() {
        if(emptyLayout.visibility == View.VISIBLE) {
            emptyLayout.visibility = View.GONE
        }
        if(availableAmenitiesRecyclerView.visibility == View.VISIBLE) {
            availableAmenitiesRecyclerView.visibility = View.GONE
        }
    }

    fun isListEmpty(isEmpty: Boolean){
        if(isEmpty){
            emptyLayout.visibility = View.VISIBLE
            availableAmenitiesRecyclerView.visibility = View.GONE
        } else {
            emptyLayout.visibility = View.GONE
            availableAmenitiesRecyclerView.visibility = View.VISIBLE
        }
    }
}
