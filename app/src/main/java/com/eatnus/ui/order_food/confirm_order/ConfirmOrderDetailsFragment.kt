package com.eatnus.ui.order_food.confirm_order

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.*
import com.eatnus.R
import com.eatnus.model.Food
import com.eatnus.model.FoodBasket
import com.eatnus.model.Order
import com.eatnus.ui.order_food.confirm_order.contract.ConfirmOrderDetailsPresenterContract
import com.eatnus.ui.order_food.confirm_order.presenter.ConfirmOrderDetailsPresenter
import com.eatnus.utils.OrderStatus
import com.eatnus.utils.helper.AppPreferencesHelper
import java.text.DecimalFormat
import java.util.*
import android.app.TimePickerDialog
import android.widget.*
import com.eatnus.utils.ui.MyAlertDialog
import com.eatnus.utils.ui.MyProgressDialog

class ConfirmOrderDetailsFragment : Fragment() {
  private lateinit var confirmOrderDetailsPresenter: ConfirmOrderDetailsPresenterContract
  private lateinit var orderFoodActivity: AppCompatActivity
  private lateinit var loadDialog: MyProgressDialog
  private var total: Float = 0f
  internal var takeawayPrice: Float = 0.0f
  private lateinit var totalTextView: TextView
  private lateinit var takeawayTextView: TextView
  private lateinit var myBasket: Map<Food, FoodBasket>

  private val helper = AppPreferencesHelper()

  private lateinit var descriptionEditText: EditText
  private lateinit var takeawaySwitch: Switch
  private lateinit var laterCollectionSwitch: Switch
  private lateinit var timeOfCollectionEditText: EditText

  private lateinit var confirmOrderFragment: ConfirmOrderFragment
  fun newInstance(
    activity: AppCompatActivity,
    totalTextView: TextView,
    takeawayTextView: TextView,
    confirmOrderFragment: ConfirmOrderFragment
  ): Fragment {
    val fragment = ConfirmOrderDetailsFragment()
    val args = arguments!!

    this.orderFoodActivity = activity
    this.totalTextView = totalTextView
    this.takeawayTextView = takeawayTextView
    this.total = args.getFloat("total")
    this.myBasket = args.getSerializable("myBasket") as HashMap<Food, FoodBasket>
    fragment.arguments = args

    this.confirmOrderFragment = confirmOrderFragment

    return this@ConfirmOrderDetailsFragment
  }

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_confirm_order_details, container, false)
  }

  override fun onViewCreated(rootView: View, savedInstanceState: Bundle?) {
    super.onViewCreated(rootView, savedInstanceState)

    setHasOptionsMenu(true)
    (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.title_order_food_3)

    loadDialog =
        MyProgressDialog(orderFoodActivity, rootView.findViewById(R.id.newton_cradle_loading))
    loadDialog.start()

    confirmOrderDetailsPresenter =
        ConfirmOrderDetailsPresenter(this, confirmOrderFragment, orderFoodActivity, loadDialog)
    val decimalFormat = DecimalFormat("0.00")
    takeawaySwitch = rootView.findViewById(R.id.takeawaySwitch)
    descriptionEditText = rootView.findViewById(R.id.descriptionEditText)
    laterCollectionSwitch = rootView.findViewById(R.id.laterCollectionSwitch)
    timeOfCollectionEditText = rootView.findViewById(R.id.timeOfCollectionEditText)
    timeOfCollectionEditText.setOnClickListener {
      val timezone = TimeZone.getTimeZone("Asia/Singapore")
      val mcurrentTime = Calendar.getInstance(timezone)
      val mselectedTime = Calendar.getInstance(timezone)
      mcurrentTime.timeInMillis = System.currentTimeMillis()
      mselectedTime.timeInMillis = System.currentTimeMillis()
      val hour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
      val minute = mcurrentTime.get(Calendar.MINUTE)
      val mTimePicker: TimePickerDialog
      mTimePicker = TimePickerDialog(
        this@ConfirmOrderDetailsFragment.context,
        TimePickerDialog.OnTimeSetListener { timePicker, selectedHour, selectedMinute ->
          run {
            mselectedTime.set(Calendar.HOUR_OF_DAY, selectedHour)
            mselectedTime.set(Calendar.MINUTE, selectedMinute)
            val diff = ((mselectedTime.timeInMillis - mcurrentTime.timeInMillis) / 1000)
            val mins = diff / (60)
            if (mins >= 15) {
              if (selectedMinute > 9) {
                timeOfCollectionEditText.setText(selectedHour.toString() + ":" + selectedMinute)
              } else {
                timeOfCollectionEditText.setText(selectedHour.toString() + ":0" + selectedMinute)
              }
            } else {
              MyAlertDialog().createSimpleErrorDialog(
                this@ConfirmOrderDetailsFragment,
                "Error",
                "Please select a timing with 15 mins apart from current time and selected time."
              )
            }
          }
        },
        hour,
        minute,
        true
      )//Yes 24 hour time
      mTimePicker.setTitle("Select Time")
      mTimePicker.show()
    }
    confirmOrderDetailsPresenter.getTakeawayPrice(myBasket.keys.toList()[0].stallID)
    takeawayTextView.text = "Takeaway: + ($0.00)"

    takeawaySwitch.setOnCheckedChangeListener { buttonView, isChecked ->
      if (isChecked) {
        takeawayTextView.text = "Takeaway: + ($" + decimalFormat.format(takeawayPrice) + ")"
        totalTextView.text = "Total: $" + decimalFormat.format(total + takeawayPrice)
      } else {
        takeawayTextView.text = "Takeaway: + ($0.00)"
        totalTextView.text = "Total: $" + decimalFormat.format(total)
      }
    }
    laterCollectionSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
      if (isChecked) {
        timeOfCollectionEditText.visibility = View.VISIBLE
      } else {
        timeOfCollectionEditText.visibility = View.INVISIBLE
      }
    }
  }

  override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
    super.onCreateOptionsMenu(menu, inflater)
    inflater?.inflate(R.menu.menu_confirm_order, menu)
  }

  override fun onOptionsItemSelected(item: MenuItem?): Boolean {
    when (item?.itemId) {
      R.id.checkout -> {
        if ((laterCollectionSwitch.isChecked) && (timeOfCollectionEditText.text.isEmpty())) {
          MyAlertDialog().createSimpleErrorDialog(
            orderFoodActivity,
            "Error",
            "Please select the later timing of collection."
          )
        } else {
          loadDialog.start()
          val calendar = GregorianCalendar(TimeZone.getTimeZone("Asia/Singapore"))
          val currentDate: String =
            calendar.get(Calendar.DATE).toString() + "/" + (calendar.get(Calendar.MONTH) + 1) + "/" + calendar.get(
              Calendar.YEAR
            )
          val currentOrder = Order(
            0, myBasket.keys.toList()[0].stallID, helper.getUsername().toString(), 0,
            descriptionEditText.text.toString(),
            timeOfCollectionEditText.text.toString(),
            laterCollectionSwitch.isChecked,
            takeawaySwitch.isChecked,
            currentDate, "",
            "", OrderStatus.ORDER_CONFIRMED,
            false, "", "","", takeawayPrice
          )

          confirmOrderDetailsPresenter.confirmOrder(
            myBasket,
            currentOrder,
            totalTextView.text.toString().substring(totalTextView.text.toString().indexOf(":") + 1)
          )
        }
      }
    }
    return super.onOptionsItemSelected(item)
  }

  override fun onDestroyView() {
    super.onDestroyView()
    setHasOptionsMenu(false)
  }

}