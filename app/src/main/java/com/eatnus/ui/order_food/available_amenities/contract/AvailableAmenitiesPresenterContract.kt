package com.eatnus.ui.order_food.available_amenities.contract

import android.support.v7.widget.RecyclerView
import com.eatnus.model.Amenities

interface AvailableAmenitiesPresenterContract {

    fun retrieveAvailableAmenities(availableAmenitiesRecyclerView: RecyclerView): MutableList<Amenities>
}