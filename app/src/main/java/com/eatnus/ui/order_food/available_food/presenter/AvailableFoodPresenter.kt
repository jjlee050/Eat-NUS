package com.eatnus.ui.order_food.available_food.presenter

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.eatnus.R
import com.eatnus.model.Food
import com.eatnus.model.FoodBasket
import com.eatnus.model.Stall
import com.eatnus.ui.order_food.available_food.contract.AvailableFoodPresenterContract
import com.eatnus.ui.order_food.available_food.AvailableFoodFragment
import com.eatnus.ui.order_food.confirm_order.ConfirmOrderFragment
import com.eatnus.utils.api.EatNUSAPI
import com.eatnus.utils.ui.MyAlertDialog
import com.eatnus.utils.ui.MyProgressDialog
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.database.*

class AvailableFoodPresenter(activity: AppCompatActivity, fragment: AvailableFoodFragment, dialog: MyProgressDialog): AvailableFoodPresenterContract {
    private val activity = activity
    private val fragment = fragment
    private val loadDialog = dialog
    override fun retrieveAvailableFood(foodMenuRecyclerView: RecyclerView, stallID: Int): MutableList<Food> {
        val foodList: MutableList<Food> = mutableListOf()
        val foodsDBTask: Task<DataSnapshot> = EatNUSAPI.getFoods()
        val task: Task<Void> = Tasks.whenAll(foodsDBTask)
        task.addOnSuccessListener {
            val foodsData: DataSnapshot = foodsDBTask.result
            foodsData.children.forEach{
                if (it.child("stall_id").value.toString().toInt() == stallID) {
                    val food = Food()
                    food.stallID = stallID
                    food.id = Integer.parseInt(it.key)
                    food.name = it.child("name").value.toString()
                    food.desc = it.child("desc").value.toString()
                    food.totalQty = it.child("total_qty").value.toString().toInt()
                    food.price = it.child("price").value.toString().toFloat()
                    foodList.add(food)
                }
            }
            loadDialog.stop()
            foodMenuRecyclerView.adapter.notifyDataSetChanged()
        }
        task.addOnFailureListener {
            loadDialog.stop()
            MyAlertDialog().createSimpleErrorDialog(activity, "Error", "Unable to retrieve food from the stall")
        }
        return foodList
    }

    override fun retrieveStallQueue(stall: Stall, imageView: ImageView, textView: TextView) {
        val stallID = stall.id
        var numInQueue = 0
        val ordersDBTask = EatNUSAPI.getOrders()
        val task = Tasks.whenAll(ordersDBTask)
        task.addOnSuccessListener {
            val ordersData = ordersDBTask.result
            ordersData.children.forEach{
                //Must also check for food order status
                if(stallID == it.child("stallID").value.toString().toInt()){
                    numInQueue++
                }
            }
            if(numInQueue > 5){
                imageView.visibility = View.VISIBLE
                textView.visibility = View.VISIBLE
            } else {
                imageView.visibility = View.GONE
                textView.visibility = View.GONE
            }
        }
    }

    override fun confirmOrder(myBasket: HashMap<Food, FoodBasket>) {
        val fragmentManager = fragment.fragmentManager
        loadDialog.stop()
        val confirmOrderFragment = ConfirmOrderFragment()
        val args = Bundle()
        args.putSerializable("myBasket", myBasket)

        confirmOrderFragment.arguments = args
        fragmentManager?.
                beginTransaction()?.
                replace(R.id.container, confirmOrderFragment)?.
                addToBackStack(null)?.
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)?.
                commit()
    }
}