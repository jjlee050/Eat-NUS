package com.eatnus.ui.order_food.confirm_order.presenter

import com.eatnus.model.Food
import com.eatnus.model.FoodBasket
import com.eatnus.ui.order_food.confirm_order.contract.ConfirmOrderPresenterContract

class ConfirmOrderPresenter: ConfirmOrderPresenterContract {
    override fun calculateTotal(myBasket: Map<Food, FoodBasket>): Float {
        var total = 0.0f
        for (basket in myBasket)
            total += (basket.value.qty * basket.key.price)
        return total
    }
}