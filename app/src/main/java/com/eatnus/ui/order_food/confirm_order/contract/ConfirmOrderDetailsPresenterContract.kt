package com.eatnus.ui.order_food.confirm_order.contract

import com.eatnus.model.Food
import com.eatnus.model.FoodBasket
import com.eatnus.model.Order

interface ConfirmOrderDetailsPresenterContract {
    fun confirmOrder(myBasket: Map<Food, FoodBasket>, currentOrder: Order, total: String)

    fun updateQty(myBasket: Map<Food, FoodBasket>)

    fun redirectPaymentPage()

    fun getTakeawayPrice(stallID: Int)
}