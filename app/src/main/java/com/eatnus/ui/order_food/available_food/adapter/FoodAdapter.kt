package com.eatnus.ui.order_food.available_food.adapter

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.eatnus.R
import com.eatnus.model.Food
import com.eatnus.model.FoodBasket
import com.eatnus.ui.order_food.available_food.AvailableFoodFragment
import com.eatnus.utils.ui.MyAlertDialog
import java.math.RoundingMode
import java.text.DecimalFormat

class FoodAdapter(private val items: MutableList<Food>, val fragment: AvailableFoodFragment, val orderFoodActivity: AppCompatActivity) : RecyclerView.Adapter<ViewHolder>() {
    val basketTable: HashMap<Food, FoodBasket> = HashMap()

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return items.size
    }

    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(fragment.context).inflate(R.layout.list_item_available_food, parent, false)
        return ViewHolder(view)
    }

    // Binds each animal in the ArrayList to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var decimalFormat = DecimalFormat("0.00")
        holder.foodNameTextView.text = items[position].name + " (Max: " + items[position].totalQty + ")"
        holder.priceTextView.text = "$ " + decimalFormat.format(items[position].price.toBigDecimal().setScale(2, RoundingMode.UP))
        holder.descriptionTextView.text = items[position].desc

        holder.minusQtyButton.setOnClickListener {
            holder.qtyEditText.setText((holder.qtyEditText?.text.toString().toInt() - 1).toString())
            basketTable[items[position]] = FoodBasket(0, items[position].id, holder.qtyEditText.text.toString().toInt(), items[position].price)
            if(holder.qtyEditText.text.toString().toInt() <= 0)
                basketTable.remove(items[position])

            disableButtons(holder, holder.qtyEditText.text.toString(), position)
        }

        holder.plusQtyButton.setOnClickListener {
            holder.qtyEditText.setText((holder.qtyEditText.text.toString().toInt() + 1).toString())

            basketTable[items[position]] = FoodBasket(0, items[position].id, holder.qtyEditText.text.toString().toInt(), items[position].price)

            disableButtons(holder, holder.qtyEditText.text.toString(), position)
        }
        holder.qtyEditText.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (!p0.toString().isEmpty()) {
                    if ((p0.toString().toInt() >= 0) && (p0.toString().toInt() <= items[position].totalQty)) {
                        disableButtons(holder, p0.toString(), position)
                        basketTable[items[position]] = FoodBasket(0, items[position].id, p0.toString().toInt(), items[position].price)
                    } else {
                        holder.qtyEditText.setText("0")
                        MyAlertDialog().createSimpleErrorDialog(fragment, "Error", "Please select the quantity of food within 0 and max quantity.")
                    }
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        if (items[position].totalQty <= 0) {
            disableSelection(holder)
        }

        disableButtons(holder, holder.qtyEditText.text.toString(), position)
    }

    private fun disableSelection(holder: ViewHolder){
        holder.itemView.isEnabled = false
        holder.itemView.alpha = 0.4f
    }

    private fun disableButtons(holder: ViewHolder, qtyText: String, position: Int) {
        holder.minusQtyButton.isEnabled = qtyText.toInt() > 0
        holder.plusQtyButton.isEnabled = qtyText.toInt() < items[position].totalQty
    }

}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view){
    val foodNameTextView: TextView = view.findViewById(R.id.foodNameTextView)
    val priceTextView: TextView = view.findViewById(R.id.priceTextView)
    val descriptionTextView: TextView = view.findViewById(R.id.descriptionTextView)
    val qtyEditText: EditText = view.findViewById(R.id.qtyEditText)
    val minusQtyButton: Button = view.findViewById(R.id.minusQtyButton)
    val plusQtyButton: Button = view.findViewById(R.id.plusQtyButton)
}