package com.eatnus.ui.order_food.payment

import android.support.v4.app.Fragment
import android.view.View
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.eatnus.R
import android.widget.ImageButton
import com.eatnus.ui.order_food.confirm_order.ConfirmOrderFragment
import android.support.v7.app.AppCompatActivity
import com.eatnus.model.Order
import com.eatnus.ui.order_food.payment.contract.PaymentSelectPresenterContract
import com.eatnus.ui.order_food.payment.presenter.PaymentSelectPresenter
import com.eatnus.utils.ui.MyProgressDialog
import com.google.android.gms.wallet.*
import java.util.*
import com.google.android.gms.wallet.AutoResolveHelper
import android.app.Activity
import android.content.pm.ApplicationInfo
import android.graphics.Bitmap
import android.os.AsyncTask
import android.os.Handler
import android.support.v7.app.AlertDialog
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import android.widget.Toast
import com.eatnus.App
import com.eatnus.ui.order_food.OrderFoodActivity
import com.eatnus.ui.order_food.payment.PaymentSelectFragment.Companion.order
import com.eatnus.ui.order_food.payment.PaymentSelectFragment.Companion.total
import com.eatnus.utils.api.EatNUSAPI
import com.google.android.gms.wallet.WalletConstants
import com.google.android.gms.wallet.CardRequirements
import com.google.android.gms.wallet.TransactionInfo
import com.google.android.gms.wallet.PaymentDataRequest
import com.google.android.gms.wallet.PaymentMethodTokenizationParameters
import com.google.gson.JsonObject
import com.koushikdutta.ion.Ion

/**
 * View Class to handle login payment selection page.
 * Currently only the cash payment button is working,
 * in the future have to link to get total amount to pay!
 *
 * @author Jeremy Low & Joseph Lee
 */
class PaymentSelectFragment : Fragment() {
  companion object {
    const val LOAD_PAYMENT_DATA_REQUEST_CODE = 999
    lateinit var order: Order
    var total: String = ""
  }

  internal lateinit var paymentSelectPresenter: PaymentSelectPresenterContract
  internal lateinit var cashBtn: ImageButton
  internal lateinit var googleWalletButton: ImageButton
  private lateinit var confirmOrderFragment: ConfirmOrderFragment
  private lateinit var orderFoodActivity: AppCompatActivity
  private lateinit var loadDialog: MyProgressDialog
  private lateinit var mPaymentsClient: PaymentsClient
  internal lateinit var webView: WebView
  internal lateinit var alertDialogBuilder: AlertDialog.Builder
  internal val loginUrl = "https://www.dbs.com/sandbox/api/sg/v1/oauth/authorize?client_id="+EatNUSAPI.dbsClientId+"&redirect_uri=https://us-central1-eat-nus-b1102.cloudfunctions.net/app/dbsOAuth&scope=Read&response_type=code&state=0399"

  fun newInstance(
    confirmOrderFragment: ConfirmOrderFragment,
    orderFoodActivity: AppCompatActivity
  ): Fragment {
    val fragment = PaymentSelectFragment()
    val args = arguments!!
    this.confirmOrderFragment = confirmOrderFragment
    this.orderFoodActivity = orderFoodActivity
    order = args.getParcelable("order")
    total = args.getString("total")
    fragment.arguments = args
    return this@PaymentSelectFragment
  }

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    val rootView = inflater.inflate(R.layout.fragment_payment_select, container, false)

    (activity as AppCompatActivity).supportActionBar?.title = "Select payment method"

    loadDialog =
        MyProgressDialog(orderFoodActivity, rootView.findViewById(R.id.newton_cradle_loading))
    loadDialog.start()

    mPaymentsClient = Wallet.getPaymentsClient(
      orderFoodActivity,
      Wallet.WalletOptions.Builder().setEnvironment(WalletConstants.ENVIRONMENT_TEST).build()
    )
    paymentSelectPresenter = PaymentSelectPresenter(
      this,
      confirmOrderFragment,
      orderFoodActivity,
      loadDialog,
      mPaymentsClient
    )

    cashBtn = rootView.findViewById(R.id.btn_cash)
    googleWalletButton = rootView.findViewById(R.id.btn_googlewallet)
    val payLahButton = rootView.findViewById<Button>(R.id.btn_paylah)
    paymentSelectPresenter.allowCashPayment(order.stallID)
    paymentSelectPresenter.allowGoogleWalletPayment()

    cashBtn.setOnClickListener {
      loadDialog.start()
      //create notification
      paymentSelectPresenter.payByCash(order, total)
    }

    googleWalletButton.setOnClickListener {
      loadDialog.start()
      val paymentRequest = createPaymentDataRequest(total.trim().substring(1))

      (orderFoodActivity as OrderFoodActivity).paymentSelectPresenter = paymentSelectPresenter
      (orderFoodActivity as OrderFoodActivity).orderID = order.orderID
      (orderFoodActivity as OrderFoodActivity).loadDialog = loadDialog
      AutoResolveHelper.resolveTask(
        mPaymentsClient.loadPaymentData(paymentRequest),
        orderFoodActivity as Activity,
        LOAD_PAYMENT_DATA_REQUEST_CODE
      )
      // LOAD_PAYMENT_DATA_REQUEST_CODE is a constant integer of your choice,
      // similar to what you would use in startActivityForResult
    }

    payLahButton.setOnClickListener {
      webView = WebView(context)
      webView.settings.javaScriptEnabled = true
      webView.addJavascriptInterface(MyJavaScriptInterface(this), "HtmlViewer")
      webView.loadUrl(loginUrl)
      webView.webViewClient = object : WebViewClient() {
        override fun onPageFinished(view: WebView?, url: String?) {
          super.onPageFinished(view, url)
          println("Started URL: $url")
          if (url!!.contains("https://us-central1-eat-nus-b1102.cloudfunctions.net/app/dbsOAuth?")) {
            view!!.loadUrl("javascript:window.HtmlViewer.showHTML('&lt;html&gt;'+document.getElementsByTagName('html')[0].innerHTML+'&lt;/html&gt;')")
          }
        }
      }

      alertDialogBuilder = AlertDialog.Builder(context!!)
      alertDialogBuilder.setView(webView).show()
    }

    return rootView
  }

  private fun createPaymentDataRequest(total: String): PaymentDataRequest {
    val request = PaymentDataRequest.newBuilder()
      .setTransactionInfo(
        TransactionInfo.newBuilder()
          .setTotalPriceStatus(WalletConstants.TOTAL_PRICE_STATUS_FINAL)
          .setTotalPrice(total)
          .setCurrencyCode("SGD")
          .build()
      )
      .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
      .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
      .setCardRequirements(
        CardRequirements.newBuilder()
          .addAllowedCardNetworks(
            Arrays.asList(
              WalletConstants.CARD_NETWORK_AMEX,
              WalletConstants.CARD_NETWORK_DISCOVER,
              WalletConstants.CARD_NETWORK_VISA,
              WalletConstants.CARD_NETWORK_MASTERCARD
            )
          )
          .build()
      )

    request.setPaymentMethodTokenizationParameters(createTokenizationParameters())
    return request.build()
  }

  private fun createTokenizationParameters(): PaymentMethodTokenizationParameters {
    return PaymentMethodTokenizationParameters.newBuilder()
      .setPaymentMethodTokenizationType(WalletConstants.PAYMENT_METHOD_TOKENIZATION_TYPE_PAYMENT_GATEWAY)
      .addParameter("gateway", "stripe")
      .addParameter("stripe:publishableKey", EatNUSAPI.StripePublishableKey)
      .addParameter("stripe:version", "5.1.0")
      .build()
  }
}

private class TestTask(private val fragment: PaymentSelectFragment): AsyncTask<String, Void, String>() {
  override fun doInBackground(vararg params: String?): String {
    val onlineCheckObj = JsonObject()
    val dataObj = JsonObject()
    dataObj.addProperty("merchantId", "168700800013")
    //Test phone number
    dataObj.addProperty("eWalletId", "33764785")
    dataObj.addProperty("merchantTransactionReference", order.orderID.toString())
    dataObj.addProperty("amount", total.substring(2).toFloat())
    onlineCheckObj.add("onlineCheckout",dataObj)

    var dbsRefId = "Fail"
    val response = Ion
      .with(App.context)
      .load("https://www.dbs.com/sandbox/api/sg/v1/eWallet/onlineCheckout")
      .addHeader("clientId", EatNUSAPI.dbsClientId.trim())
      .addHeader("accessToken", params[0]!!.trim())
      .setJsonObjectBody(onlineCheckObj)
      .asJsonObject().get()
    // U need to change to blaze plan then can test. (Outwork neetwork connection)
    println("Response: $response")
    return if (response.has("status"))
      response["bankTransactionReference"].asString
    else
      "Fail"
  }

  //Result is bankTransactionReference
  override fun onPostExecute(result: String) {
    super.onPostExecute(result)
    if (result == "Fail") {
      Toast.makeText(fragment.context, "Transaction fail", Toast.LENGTH_LONG).show()
      fragment.webView.loadUrl(fragment.loginUrl)
      return
    }
    fragment.paymentSelectPresenter.updateStatusViaDBSPaylah(order.orderID, result)
  }

}

//for extracting html source code and saving token to SharedPrefs
private class MyJavaScriptInterface(private val fragment: PaymentSelectFragment) {
  @JavascriptInterface
  fun showHTML(html: String) {
    //code to use html content here
    Handler().post {
      var splitTokens = html.split("<body>")
      val token = splitTokens[1]
      splitTokens = token.split("</body>")
      val dbsToken = splitTokens[0]
      println(dbsToken)
      TestTask(fragment).execute(dbsToken)
    }
  }
}
