package com.eatnus.ui.order_food

import android.app.Activity
import android.content.Intent
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import com.eatnus.App
import com.eatnus.R
import com.eatnus.ui.home.HomeActivity
import com.eatnus.ui.order_food.available_amenities.AvailableAmenitiesFragment
import com.eatnus.ui.order_food.payment.PaymentSelectFragment
import com.google.android.gms.wallet.AutoResolveHelper
import com.google.android.gms.wallet.PaymentData
import com.stripe.android.model.Token
import com.eatnus.ui.order_food.payment.contract.PaymentSelectPresenterContract
import com.eatnus.utils.ui.MyProgressDialog
import kotlin.math.roundToInt

class OrderFoodActivity : AppCompatActivity() {
  private val fragmentManager = supportFragmentManager
  lateinit var paymentSelectPresenter: PaymentSelectPresenterContract
  lateinit var loadDialog: MyProgressDialog
  var orderID: Int = 0

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_order_food)

    supportActionBar?.setDisplayHomeAsUpEnabled(true)

    val availableAmenitiesFragment = AvailableAmenitiesFragment()
    fragmentManager.beginTransaction().replace(R.id.container, availableAmenitiesFragment).commit()
  }

  override fun onOptionsItemSelected(item: MenuItem?): Boolean {
    when (item?.itemId) {
      android.R.id.home -> {
        onBackPressed()
        return true
      }
    }
    return super.onOptionsItemSelected(item)
  }

  override fun onBackPressed() {
    if (fragmentManager.backStackEntryCount >= 1) {
      fragmentManager.popBackStackImmediate()
    } else {
      super.onBackPressed()
      val customerHomePageActivity = Intent(this, HomeActivity::class.java)
      startActivity(customerHomePageActivity)
      finish()
    }
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    println("Result: $resultCode")
    when (requestCode) {
      PaymentSelectFragment.LOAD_PAYMENT_DATA_REQUEST_CODE -> when (resultCode) {
        Activity.RESULT_OK -> {
          val paymentData = PaymentData.getFromIntent(data!!)
          // You can get some data on the user's card, such as the brand and last 4 digits
          val info = paymentData!!.cardInfo
          // You can also pull the user address from the PaymentData object.
          val address = paymentData.shippingAddress
          // This is the raw JSON string version of your Stripe token.
          val rawToken = paymentData.paymentMethodToken?.token

          // Now that you have a Stripe token object, charge that by using the id
          val stripeToken = Token.fromString(rawToken)
          println("Card $info Stripe Token $rawToken")
          if (stripeToken != null) {
            // This chargeToken function is a call to your own server, which should then connect
            // to Stripe's API to finish the charge.
            //chargeToken(stripeToken!!.getId())
            println("Stripe Token $stripeToken.id")

            // Set your secret key: remember to change this to your live secret key in production
            // See your keys here: https://dashboard.stripe.com/account/apikeys

            // Token is created using Checkout or Elements!
            // Get the payment token ID submitted by the form:

            ChargeUser(paymentSelectPresenter, orderID, loadDialog).execute(stripeToken.id)
          }
        }
        Activity.RESULT_CANCELED -> {
          loadDialog.stop()
        }
        AutoResolveHelper.RESULT_ERROR -> {
          val status = AutoResolveHelper.getStatusFromIntent(data)
          Log.e("Stripe Error Code", status.toString())
        }

      }// Do nothing.
    }
    super.onActivityResult(requestCode, resultCode, data)
  }

  //Test
  class ChargeUser(
    presenter: PaymentSelectPresenterContract,
    orderID: Int,
    loadDialog: MyProgressDialog
  ) : AsyncTask<String, Void, String>() {
    private val paymentSelectPresenter = presenter
    private val orderID = orderID
    private val loadDialog = loadDialog
    override fun doInBackground(vararg p0: String?): String {
      return paymentSelectPresenter.payByGoogleWallet(
        (PaymentSelectFragment.total.trim().substring(1).toFloat() * 100).roundToInt().toString(),
        orderID,
        p0[0]!!
      )!!
    }

    override fun onPostExecute(result: String?) {
      println(result)
      if (!result.isNullOrEmpty()) {
        Toast.makeText(App.context, "Your order has been sent successfully.", Toast.LENGTH_LONG)
          .show()
        paymentSelectPresenter.updateStatusViaGooglePay(orderID, result!!)
      } else {
        Toast.makeText(App.context, "Your order is unsuccessful.", Toast.LENGTH_LONG).show()
        loadDialog.stop()
      }
    }

    override fun onCancelled() {
      super.onCancelled()
      loadDialog.stop()
    }
  }
}
