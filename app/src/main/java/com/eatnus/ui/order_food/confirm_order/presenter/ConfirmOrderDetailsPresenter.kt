package com.eatnus.ui.order_food.confirm_order.presenter

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.eatnus.R
import com.eatnus.model.Food
import com.eatnus.model.FoodBasket
import com.eatnus.model.Order
import com.eatnus.ui.order_food.confirm_order.ConfirmOrderDetailsFragment
import com.eatnus.ui.order_food.confirm_order.ConfirmOrderFragment
import com.eatnus.ui.order_food.confirm_order.contract.ConfirmOrderDetailsPresenterContract
import com.eatnus.ui.order_food.payment.PaymentSelectFragment
import com.eatnus.utils.api.EatNUSAPI
import com.eatnus.utils.ui.MyAlertDialog
import com.eatnus.utils.ui.MyProgressDialog
import com.google.android.gms.tasks.Tasks
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.ValueEventListener

class ConfirmOrderDetailsPresenter(confirmOrderView: ConfirmOrderDetailsFragment, fragment: ConfirmOrderFragment, activity: AppCompatActivity, dialog: MyProgressDialog): ConfirmOrderDetailsPresenterContract {
    private var confirmOrderView = confirmOrderView
    private var confirmOrderFragment = fragment
    private var orderFoodActivity = activity
    private lateinit var order: Order
    private var loadDialog = dialog
    private var total: String = ""

    override fun confirmOrder(myBasket: Map<Food, FoodBasket>, currentOrder: Order, total: String) {
        this.total = total
        var maxFoodBasketID = 1
        var maxOrderID = 1

        val foodBasketsDBSource = EatNUSAPI.getFoodBaskets()
        val ordersDBSource = EatNUSAPI.getOrders()
        val task = Tasks.whenAll(foodBasketsDBSource, ordersDBSource)
        //Retrieve latest key for foodBasket and order
        task.addOnSuccessListener {
            val foodBasketsData = foodBasketsDBSource.result
            val ordersData = ordersDBSource.result

            foodBasketsData.children.forEach{
                maxFoodBasketID = Integer.valueOf(it.key!!.toInt()) + 1
            }
            ordersData.children.forEach{
                maxOrderID = Integer.valueOf(it.key!!.toInt()) + 1
            }

            currentOrder.foodBasketID = maxFoodBasketID
            currentOrder.orderID = maxOrderID
            myBasket.values.forEach {
                it.foodBasketID = maxFoodBasketID
            }
            order = currentOrder

            //Add user basket into database
            EatNUSAPI.getFoodBasketRef(maxFoodBasketID.toString()).setValue(myBasket.values.toMutableList())
            EatNUSAPI.getOrderRef(maxOrderID.toString()).setValue(currentOrder)
            updateQty(myBasket)
        }
        task.addOnFailureListener {
            loadDialog.stop()
            MyAlertDialog().createSimpleErrorDialog(orderFoodActivity, "Error", "Unable to confirm order.")
        }
    }

    override fun updateQty(myBasket: Map<Food, FoodBasket>) {
        for(basket in myBasket){
            val foodID = basket.value.foodID
            val qty = basket.key.totalQty - basket.value.qty
            EatNUSAPI.getFoodRef(foodID.toString()).child("total_qty").setValue(qty)
        }
        redirectPaymentPage()
    }

    override fun getTakeawayPrice(stallID: Int) {
        val stallDbSource = EatNUSAPI.getStall(stallID.toString())
        val task = Tasks.whenAll(stallDbSource)
        task.addOnSuccessListener {
            val stallData = stallDbSource.result
            confirmOrderView.takeawayPrice = stallData.child("takeaway_price").value.toString().toFloat()
            loadDialog.stop()
        }
    }

    override fun redirectPaymentPage(){
        val fragmentManager = confirmOrderFragment.childFragmentManager

        loadDialog.stop()

        val fragment = PaymentSelectFragment()
        val args = Bundle()
        args.putParcelable("order", order)
        args.putString("total", total)
        fragment.arguments = args

        fragmentManager.
                beginTransaction()?.
                replace(R.id.child_fragment_container, fragment.newInstance(confirmOrderFragment, orderFoodActivity))?.
                addToBackStack(null)?.
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)?.
                commit()

    }

}