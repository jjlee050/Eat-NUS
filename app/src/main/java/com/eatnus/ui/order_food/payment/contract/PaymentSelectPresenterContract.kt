package com.eatnus.ui.order_food.payment.contract

import com.eatnus.model.Order

interface PaymentSelectPresenterContract {
  fun allowCashPayment(stallID: Int)

  fun payByCash(order: Order, total: String)

  fun payByGoogleWallet(total: String, orderID: Int, tokenId: String): String?

  //fun payByDBSPaylah(orderID): String?

  fun allowGoogleWalletPayment()

  fun updateStatusViaCash(orderID: Int)

  fun updateStatusViaGooglePay(orderID: Int, chargeId: String)

  fun updateStatusViaDBSPaylah(orderID: Int, dbsRefId: String)

  fun redirectMainPage()
}