package com.eatnus.ui.order_food.available_stall.presenter

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.eatnus.model.Stall
import com.eatnus.ui.order_food.available_stall.AvailableStallFragment
import com.eatnus.ui.order_food.available_stall.contract.AvailableStallPresenterContract
import com.eatnus.utils.api.EatNUSAPI
import com.eatnus.utils.ui.MyAlertDialog
import com.eatnus.utils.ui.MyProgressDialog
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.database.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class AvailableStallPresenter(activity: AppCompatActivity, fragment: AvailableStallFragment, dialog: MyProgressDialog): AvailableStallPresenterContract{
    private var loadDialog = dialog
    private val homeActivity = activity
    private val availableStallFragment = fragment

    override fun retrieveAvailableStall(availableStallRecyclerView: RecyclerView, amenitiesID: Int): MutableList<Stall> {
        val stallList: MutableList<Stall> = mutableListOf()
        val stallsDBTask: Task<DataSnapshot> = EatNUSAPI.getStalls()
        val foodsDBTask: Task<DataSnapshot> = EatNUSAPI.getFoods()
        val task: Task<Void> = Tasks.whenAll(stallsDBTask, foodsDBTask)
        task.addOnSuccessListener {
            val stallsData: DataSnapshot = stallsDBTask.result
            val foodsData: DataSnapshot = foodsDBTask.result

            stallsData.children.forEach {
                if (it.child("amenities_id").value.toString().toInt() == amenitiesID) {
                    val stall = Stall()
                    stall.id = Integer.parseInt(it.key)
                    stall.name = it.child("name").value.toString()
                    stall.phone = it.child("phone").value.toString()
                    stall.takeAwayPrice = it.child("takeaway_price").value.toString().toFloat()
                    stall.allowCash = it.child("allow_cash").value.toString().toBoolean()
                    stall.username = it.child("username").value.toString()
                    stall.operatingHrStart = it.child("operating_start").value.toString()
                    stall.operatingHrEnd = it.child("operating_end").value.toString()
                    if(it.child("inactivity_periods").exists()) {
                        stall.inactivityPeriodList = it.child("inactivity_periods").value as ArrayList<String>
                    }
                    val df = SimpleDateFormat("HH:mm")
                    df.timeZone = TimeZone.getTimeZone("Asia/Singapore")
                    //Operating Hr Time
                    var operatingStartHrs = df.parse(stall.operatingHrStart)
                    var operatingEndHrs = df.parse(stall.operatingHrEnd)
                    // Current Time
                    var currentDate = df.parse(df.format(Date()))
                    val calendarPeriod = Calendar.getInstance(TimeZone.getTimeZone("Asia/Singapore"))

                    //Check if it is current timing is within operating hours and outside of the inactivity period
                    if ((currentDate.after(operatingStartHrs)) && (currentDate.before(operatingEndHrs))) {
                        var gotFood = false
                        foodsData.children.forEach {
                            if (stall.id == it.child("stall_id").value.toString().toInt()) {
                                val qty = it.child("total_qty").value.toString().toInt()
                                if (qty > 0) {
                                    gotFood = true
                                }
                            }
                        }
                        if (gotFood) {
                            if (stall.inactivityPeriodList.isEmpty()) {
                                stallList.add(stall)
                                availableStallRecyclerView.adapter.notifyDataSetChanged()
                            } else {
                                var isWithinInactive = false
                                for (period in stall.inactivityPeriodList) {
                                    val inactivityPeriod = df.parse(period)
                                    calendarPeriod.time = inactivityPeriod
                                    calendarPeriod.add(Calendar.HOUR_OF_DAY, 1)
                                    if ((currentDate.after(inactivityPeriod)) && (currentDate.before(calendarPeriod.time))) {
                                        isWithinInactive = true
                                        break
                                    }
                                }
                                if (!isWithinInactive) {
                                    stallList.add(stall)
                                    availableStallRecyclerView.adapter.notifyDataSetChanged()
                                }
                            }
                        }
                    }
                }
            }
            loadDialog.stop()
            availableStallFragment.isListEmpty(stallList.isEmpty())
        }
        task.addOnFailureListener {
            loadDialog.stop()
            availableStallFragment.isListEmpty(stallList.isEmpty())
            MyAlertDialog().createSimpleErrorDialog(homeActivity, "Error", it.toString())
        }
        return stallList
    }


    override fun retrieveStallQueue(stall: Stall, imageView: ImageView, textView: TextView) {
        val stallID = stall.id
        var numInQueue = 0
        val ordersDBTask = EatNUSAPI.getOrders()
        val task = Tasks.whenAll(ordersDBTask)
        task.addOnSuccessListener {
            val ordersData = ordersDBTask.result
            ordersData.children.forEach{
                //Must also check for food order status
                if(stallID == it.child("stallID").value.toString().toInt()){
                    numInQueue++
                }
            }
            if(numInQueue > 5){
                imageView.visibility = View.VISIBLE
                textView.visibility = View.VISIBLE
            } else {
                imageView.visibility = View.GONE
                textView.visibility = View.GONE
            }
        }
    }
}