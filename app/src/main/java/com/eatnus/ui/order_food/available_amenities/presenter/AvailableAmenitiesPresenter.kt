package com.eatnus.ui.order_food.available_amenities.presenter

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import com.eatnus.model.Amenities
import com.eatnus.model.Stall
import com.eatnus.ui.order_food.available_amenities.AvailableAmenitiesFragment
import com.eatnus.ui.order_food.available_amenities.contract.AvailableAmenitiesPresenterContract
import com.eatnus.utils.api.EatNUSAPI
import com.eatnus.utils.ui.MyAlertDialog
import com.eatnus.utils.ui.MyProgressDialog
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.TaskCompletionSource
import com.google.android.gms.tasks.Tasks
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.text.SimpleDateFormat
import java.util.*

//AvailableAmenitiesPresenter.kt
class AvailableAmenitiesPresenter(activity: AppCompatActivity, fragment: AvailableAmenitiesFragment, dialog: MyProgressDialog): AvailableAmenitiesPresenterContract {
    private var loadDialog = dialog
    private var availableAmenitiesFragment = fragment
    private val homeActivity = activity

    override fun retrieveAvailableAmenities(availableAmenitiesRecyclerView: RecyclerView): MutableList<Amenities> {
        val amenitiesList: MutableList<Amenities> = mutableListOf()
        val amenitiesDBTask: Task<DataSnapshot> = EatNUSAPI.getAmenities()
        val stallsDBTask: Task<DataSnapshot> = EatNUSAPI.getStalls()

        val task: Task<Void> = Tasks.whenAll(amenitiesDBTask, stallsDBTask)
        task.addOnSuccessListener {
            val amenitiesData: DataSnapshot = amenitiesDBTask.result
            val stallsData: DataSnapshot = stallsDBTask.result
            amenitiesData.children.forEach {
                val amenities = Amenities()
                amenities.id = Integer.parseInt(it.key.toString())
                amenities.name = it.child("name").value.toString()
                amenities.loc = it.child("location").value.toString()

                stallsData.children.forEach {
                    val stall = Stall()
                    stall.id = Integer.parseInt(it.key)
                    stall.amenitiesId = it.child("amenities_id").value.toString().toInt()

                    if (amenities.id == stall.amenitiesId) {
                        stall.operatingHrStart = it.child("operating_start").value.toString()
                        stall.operatingHrEnd = it.child("operating_end").value.toString()
                        if (it.child("inactivity_periods").exists()) {
                            stall.inactivityPeriodList = it.child("inactivity_periods").value as ArrayList<String>
                        }
                        //Check if stall is within the operating hours and not inside inactivity period.
                        val df = SimpleDateFormat("HH:mm")
                        df.timeZone = TimeZone.getTimeZone("Asia/Singapore")
                        //Operating Hr Time
                        var operatingStartHrs = df.parse(stall.operatingHrStart)
                        var operatingEndHrs = df.parse(stall.operatingHrEnd)
                        // Current Time
                        var currentDate = df.parse(df.format(Date()))
                        val calendarPeriod = Calendar.getInstance(TimeZone.getTimeZone("Asia/Singapore"))
                        if ((currentDate.after(operatingStartHrs)) && (currentDate.before(operatingEndHrs))) {
                            if (stall.inactivityPeriodList.isEmpty()) {
                                if (!amenitiesList.contains(amenities)) {
                                    amenitiesList.add(amenities)
                                    availableAmenitiesRecyclerView.adapter.notifyDataSetChanged()
                                }
                            } else {
                                var isWithinInactive = false
                                for (period in stall.inactivityPeriodList) {
                                    val inactivityPeriod = df.parse(period)
                                    calendarPeriod.time = inactivityPeriod
                                    calendarPeriod.add(Calendar.HOUR_OF_DAY, 1)
                                    if ((currentDate.after(inactivityPeriod)) && (currentDate.before(calendarPeriod.time))) {
                                        isWithinInactive = true
                                        break
                                    }
                                }
                                if (!isWithinInactive) {
                                    if (!amenitiesList.contains(amenities)) {
                                        amenitiesList.add(amenities)
                                        availableAmenitiesRecyclerView.adapter.notifyDataSetChanged()
                                    }
                                }
                            }
                        }
                    }
                }
            }
            loadDialog.stop()
            availableAmenitiesFragment.isListEmpty(amenitiesList.isEmpty())
        }
        task.addOnFailureListener {
            loadDialog.stop()
            availableAmenitiesFragment.isListEmpty(amenitiesList.isEmpty())
            MyAlertDialog().createSimpleErrorDialog(homeActivity, "Error", it.toString())
        }

        return amenitiesList
    }
}