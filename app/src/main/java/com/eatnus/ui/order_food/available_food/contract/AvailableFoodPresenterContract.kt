package com.eatnus.ui.order_food.available_food.contract

import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import android.widget.TextView
import com.eatnus.model.Food
import com.eatnus.model.FoodBasket
import com.eatnus.model.Stall

interface AvailableFoodPresenterContract {
    fun retrieveAvailableFood(foodMenuRecyclerView: RecyclerView, stallID: Int): MutableList<Food>
    fun retrieveStallQueue(stall: Stall, imageView: ImageView, textView: TextView)
    fun confirmOrder(myBasket: HashMap<Food, FoodBasket>)
}