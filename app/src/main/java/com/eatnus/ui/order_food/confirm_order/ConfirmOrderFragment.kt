package com.eatnus.ui.order_food.confirm_order

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.TextView
import com.eatnus.R
import com.eatnus.model.Food
import com.eatnus.model.FoodBasket
import com.eatnus.ui.order_food.confirm_order.adapter.BasketAdapter
import com.eatnus.ui.order_food.confirm_order.presenter.ConfirmOrderPresenter
import java.text.DecimalFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class ConfirmOrderFragment : Fragment() {
    private var orderFoodActivity: AppCompatActivity? = null
    private var confirmOrderPresenter = ConfirmOrderPresenter()

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is AppCompatActivity)
            this.orderFoodActivity = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_confirm_order, container, false)
    }

    override fun onViewCreated(rootView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(rootView, savedInstanceState)

        val myBasket = arguments!!.getSerializable("myBasket") as HashMap<Food, FoodBasket>

        val myBasketRecyclerView = rootView.findViewById<RecyclerView>(R.id.myBasketRecyclerView)
        myBasketRecyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        myBasketRecyclerView.adapter = BasketAdapter(myBasket, this@ConfirmOrderFragment.context)

        val totalTextView = rootView.findViewById<TextView>(R.id.totalTextView)
        val takeawayTextView = rootView.findViewById<TextView>(R.id.takeawayTextView)

        val decimalFormat = DecimalFormat("0.00")

        totalTextView.text = "Total: $" + decimalFormat.format(confirmOrderPresenter.calculateTotal(myBasket))

        val childFragment = ConfirmOrderDetailsFragment()
        val args = Bundle()
        args.putSerializable("myBasket", myBasket)
        args.putFloat("total", confirmOrderPresenter.calculateTotal(myBasket))
        childFragment.arguments = args
        childFragmentManager.beginTransaction().replace(R.id.child_fragment_container, childFragment.newInstance(orderFoodActivity!!, totalTextView, takeawayTextView, this)).addToBackStack(null).setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).commit()
    }

    override fun onDetach() {
        super.onDetach()
        this.orderFoodActivity = null
    }

}
