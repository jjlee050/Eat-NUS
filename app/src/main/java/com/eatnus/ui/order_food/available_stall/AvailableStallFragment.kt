package com.eatnus.ui.order_food.available_stall

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ClipDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import com.eatnus.R
import com.eatnus.ui.order_food.available_stall.adapter.StallAdapter
import com.eatnus.ui.order_food.available_stall.contract.AvailableStallPresenterContract
import com.eatnus.ui.order_food.available_stall.presenter.AvailableStallPresenter
import com.eatnus.utils.ui.MyProgressDialog

/**
 * A simple [Fragment] subclass.
 */
class AvailableStallFragment : Fragment() {
    private lateinit var orderFoodPresenter: AvailableStallPresenterContract
    private var orderFoodActivity: AppCompatActivity? = null
    private lateinit var loadDialog: MyProgressDialog
    private lateinit var emptyLayout: View
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var availableStallRecyclerView: RecyclerView
    internal var amenitiesID: Int = 0

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is AppCompatActivity)
            this.orderFoodActivity = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_available_stall, container, false)
    }

    override fun onViewCreated(rootView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(rootView, savedInstanceState)

        amenitiesID = arguments!!.getInt("amenitiesID")

        loadDialog = MyProgressDialog(orderFoodActivity!!, rootView.findViewById(R.id.newton_cradle_loading))
        loadDialog.start()


        orderFoodPresenter = AvailableStallPresenter(orderFoodActivity!!, this@AvailableStallFragment, loadDialog)
        val itemDecor = DividerItemDecoration(activity, ClipDrawable.HORIZONTAL)

        availableStallRecyclerView = rootView.findViewById(R.id.availableStallRecyclerView)

        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.title_order_food_2)

        availableStallRecyclerView.addItemDecoration(itemDecor)
        availableStallRecyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        availableStallRecyclerView.adapter = StallAdapter(orderFoodPresenter.retrieveAvailableStall(availableStallRecyclerView, amenitiesID), this@AvailableStallFragment, orderFoodPresenter)

        emptyLayout = rootView.findViewById<View>(R.id.emptyLayout)
        closeViews()

        swipeRefreshLayout = rootView.findViewById(R.id.swipeRefreshLayout)
        swipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE)
        swipeRefreshLayout.setOnRefreshListener {
            loadDialog.start()
            closeViews()
            availableStallRecyclerView.adapter = StallAdapter(orderFoodPresenter.retrieveAvailableStall(availableStallRecyclerView, amenitiesID), this@AvailableStallFragment, orderFoodPresenter)
            swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun closeViews() {
        if(emptyLayout.visibility == View.VISIBLE) {
            emptyLayout.visibility = View.GONE
        }
        if(availableStallRecyclerView.visibility == View.VISIBLE) {
            availableStallRecyclerView.visibility = View.GONE
        }
    }

    fun isListEmpty(isEmpty: Boolean){
        if(isEmpty){
            emptyLayout.visibility = View.VISIBLE
            availableStallRecyclerView.visibility = View.GONE
        } else {
            emptyLayout.visibility = View.GONE
            availableStallRecyclerView.visibility = View.VISIBLE
        }
    }
}
