package com.eatnus.ui.order_food.available_stall.adapter

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.eatnus.R
import com.eatnus.model.Stall
import com.eatnus.ui.order_food.available_food.AvailableFoodFragment
import com.eatnus.ui.order_food.available_stall.AvailableStallFragment
import com.eatnus.ui.order_food.available_stall.contract.AvailableStallPresenterContract

class StallAdapter(private val items: MutableList<Stall>, private val fragment: AvailableStallFragment, private val orderFoodPresenter: AvailableStallPresenterContract) : RecyclerView.Adapter<ViewHolder>() {

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return items.size
    }

    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(fragment.context).inflate(R.layout.list_item_available_stall, parent, false)
        return ViewHolder(view)
    }

    // Binds each animal in the ArrayList to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.stallNameTextView.text = items[position].name
        holder.phoneTextView.text = items[position].phone
        holder.itemView?.setOnClickListener {
            val fragmentManager = fragment.fragmentManager
            val availableFoodFragment = AvailableFoodFragment()
            val bundle = Bundle()
            bundle.putInt("amenitiesID", fragment.amenitiesID)
            bundle.putInt("stallID", items[position].id)
            bundle.putString("stallName", items[position].name)
            bundle.putString("stallPhone", items[position].phone)
            availableFoodFragment.arguments = bundle
            fragmentManager?.
                    beginTransaction()?.
                    replace(R.id.container, availableFoodFragment)?.
                    addToBackStack(null)?.
                    setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)?.
                    commit()
        }

        orderFoodPresenter.retrieveStallQueue(items[position], holder.queueTimingImageView, holder.queueTextView)
    }

}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view){
    // Holds the TextView that will add each animal to
    val stallNameTextView: TextView = view.findViewById(R.id.stallNameTextView)
    val phoneTextView: TextView = view.findViewById(R.id.phoneTextView)
    val queueTimingImageView: ImageView = view.findViewById(R.id.queueTimingImageView)
    val queueTextView: TextView = view.findViewById(R.id.queueTextView)
}