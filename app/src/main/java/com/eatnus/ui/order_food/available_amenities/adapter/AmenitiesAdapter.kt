package com.eatnus.ui.order_food.available_amenities.adapter

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.eatnus.R
import com.eatnus.model.Amenities
import com.eatnus.ui.order_food.available_amenities.AvailableAmenitiesFragment
import com.eatnus.ui.order_food.available_stall.AvailableStallFragment

class AmenitiesAdapter(private val items: MutableList<Amenities>, private val fragment: AvailableAmenitiesFragment) : RecyclerView.Adapter<ViewHolder>() {

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return items.size
    }

    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(fragment.context).inflate(R.layout.list_item_available_amenities, parent, false)
        return ViewHolder(view)
    }

    // Binds each animal in the ArrayList to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.amenitiesNameTextView.text = items[position].name
        holder.locationTextView.text = items[position].loc
        holder.itemView?.setOnClickListener {
            val fragmentManager = fragment.fragmentManager
            val availableStallFragment = AvailableStallFragment()
            val bundle = Bundle()
            bundle.putInt("amenitiesID", items[position].id)
            availableStallFragment.arguments = bundle
            fragmentManager?.
                    beginTransaction()?.
                    replace(R.id.container, availableStallFragment)?.
                    addToBackStack(null)?.
                    setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)?.
                    commit()
        }
    }

}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view){
    // Holds the TextView that will add each animal to
    val amenitiesNameTextView: TextView = view.findViewById(R.id.amenitiesNameTextView)
    val locationTextView: TextView = view.findViewById(R.id.locationTextView)
}