package com.eatnus.ui.login.interactor

import android.text.TextUtils
import com.eatnus.ui.login.contract.LoginInteractorContract
import com.eatnus.ui.login.contract.listener.OnLoginFinishListener
import com.eatnus.ui.login.task.LoginCustomerTask
import com.eatnus.utils.helper.AppPreferencesHelper

//LoginInteractor.kt
/**
 * Interactor class to handle login vendor.
 *
 * @author Joseph Lee
 */
class LoginInteractor: LoginInteractorContract {
    private lateinit var listener: OnLoginFinishListener

    /**
     * Validate vendor and admin accordingly.
     */
    override fun login(name: String, password: String, listener: OnLoginFinishListener){
        this@LoginInteractor.listener = listener
        if(TextUtils.isEmpty(name)){
            listener.onEmptyUsernameError()
            return
        }
        if(TextUtils.isEmpty(password)){
            listener.onEmptyPasswordError()
            return
        }
        if((!name.contains("nusstu")) && (!name.contains("nusstf")) && (!name.contains("nusext"))){
            //Need to validate stall name and admin
            when {
                name.contains("vendor") -> {
                    AppPreferencesHelper().setUserRole("vendor")
                    listener.onSuccess()
                }
                name == "admin" -> {
                    AppPreferencesHelper().setUserRole("admin")
                    listener.onSuccess()
                }
                else -> {
                    listener.onPrefixError()
                    return
                }
            }
        } else {
            AppPreferencesHelper().setUserRole("customer")
            val mTask = LoginCustomerTask(name.substring(name.indexOf("\\") + 1), password, listener)
            mTask.execute()
        }
    }
}