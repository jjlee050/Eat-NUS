package com.eatnus.ui.login.contract

import com.eatnus.ui.login.contract.listener.OnLoginFinishListener

//LoginInteractorContract.kt
/**
 *
 * Contract for login vendor interactor class.
 *
 * @author Joseph Lee
 */
interface LoginInteractorContract {
    /**
     * Validate vendor and admin accordingly.
     */
    fun login(name: String, password: String, listener: OnLoginFinishListener)
}