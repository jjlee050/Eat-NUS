package com.eatnus.ui.login.contract

import android.support.v7.app.AppCompatActivity

//LoginPresenterContract.kt
/**
 * Contract for login vendor presenter class.
 *
 * @author Joseph Lee
 */
interface LoginPresenterContract {
    /**
     * To pass information to the interactor to handle login.
     */
    fun login(name: String, password: String)

    /**
     * Save user details into firebase if user is not created in frebase.
     */
    fun saveDetailsIntoFirebase(activity: AppCompatActivity, name: String, password: String)

    /**
     * Redirect user to main page.
     */
    fun redirect(activity: AppCompatActivity)
}