package com.eatnus.ui.login.task

import android.accounts.NetworkErrorException
import android.os.AsyncTask
import com.eatnus.ui.login.contract.listener.OnLoginFinishListener
import com.eatnus.utils.api.EatNUSAPI

//LoginCustomerTask.kt
/**
 * Async Task service to handle login of customer using IVLE authentication.
 */
class LoginCustomerTask(mName: String, mPassword: String, mListener: OnLoginFinishListener) : AsyncTask<Void, Void, String>() {
    private var name: String = mName
    private var password: String = mPassword
    private var listener: OnLoginFinishListener = mListener
    private var networkError: Boolean = false
    private var credError: Boolean = false

    override fun doInBackground(vararg params: Void): String? {
        try {
            return EatNUSAPI.login(name, password)
        } catch (e: NetworkErrorException) {
            networkError = true
            this.cancel(true)
        } catch (e: Exception) {
            credError = true
            this.cancel(true)
        }
        return null
    }

    override fun onPostExecute(authToken: String) {
        // On a successful authentication, call back into the Activity to
        // communicate the authToken (or null for an error)

        listener.onSuccess()
    }

    override fun onCancelled() {
        // If the action was cancelled (by the user touching the cancel
        // button in the progress dialog), then call back into the activity
        // to let it know.
        if (networkError) {
            listener.onNetworkError()
        }
        if (credError) {
            listener.onCredentialsError()
        }
    }
}