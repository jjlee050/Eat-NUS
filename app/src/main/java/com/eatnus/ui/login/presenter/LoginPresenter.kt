package com.eatnus.ui.login.presenter

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import com.eatnus.model.Customer
import com.eatnus.model.User
import com.eatnus.ui.home.HomeActivity
import com.eatnus.ui.login.interactor.LoginInteractor
import com.eatnus.ui.login.contract.LoginInteractorContract
import com.eatnus.ui.login.contract.LoginPresenterContract
import com.eatnus.ui.login.contract.LoginViewContract
import com.eatnus.ui.login.contract.listener.OnLoginFinishListener
import com.eatnus.utils.ui.MyProgressDialog
import com.eatnus.utils.api.EatNUSAPI
import com.eatnus.utils.helper.AppPreferencesHelper
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.iid.FirebaseInstanceId

//LoginPresenter.kt
/**
 * Presenter class to handle login vendor.
 *
 * @author Joseph Lee
 */
class LoginPresenter(mLoginView: LoginViewContract, dialog: MyProgressDialog) :
  LoginPresenterContract, OnLoginFinishListener {
  private var loginView: LoginViewContract = mLoginView
  private var loginInteractor: LoginInteractorContract = LoginInteractor()
  private var loadDialog = dialog
  /**
   * To pass information to the interactor to handle login.
   */
  override fun login(name: String, password: String) {
    loginInteractor.login(name, password, this)
  }

  /**
   * Pass to view to show error message when username is empty.
   */
  override fun onEmptyUsernameError() {
    loginView.showLoginError("Please enter a username.")
  }

  /**
   * Pass to view to show error message when password is empty.
   */
  override fun onEmptyPasswordError() {
    loginView.showLoginError("Please enter a password.")
  }

  /**
   * Pass to view to show error message when user enter incorrect prefix
   */
  override fun onPrefixError() {
    loginView.showLoginError("Invalid prefix. Please enter a valid prefix (e.g. nusstu, nusstf, nusext, vendor)")
  }

  /**
   * Pass to view to show error message when there is no internet connection.
   */
  override fun onNetworkError() {
    loginView.showLoginError("No internet connection. Please ensure your phone has a network connection.")
  }

  /**
   * Pass to view to show error message when user enter incorrect credentials.
   */
  override fun onCredentialsError() {
    loginView.showLoginError("Invalid credentials. Please enter correct username and password,")
  }

  /**
   * Pass to view to show message when user login successfully.
   */
  override fun onSuccess() {
    //This will not be shown.
    loginView.redirectUserToMainPage()
  }

  /**
   * Save user details into firebase if user is not created in frebase.
   */
  override fun saveDetailsIntoFirebase(
    activity: AppCompatActivity,
    name: String,
    password: String
  ) {
    val mAuth: FirebaseAuth = FirebaseAuth.getInstance()
    System.out.println(FirebaseInstanceId.getInstance().token)
    if (AppPreferencesHelper().getUserRole() == "customer") {
      mAuth.signInWithEmailAndPassword(name, password).addOnCompleteListener(activity) { task ->
        if (!task.isSuccessful) {
          mAuth.createUserWithEmailAndPassword(name, password)
        }
        val userRef = EatNUSAPI.getUserRef(AppPreferencesHelper().getUsername()!!)
        userRef.addListenerForSingleValueEvent(object : ValueEventListener {
          override fun onCancelled(p0: DatabaseError) {

          }

          override fun onDataChange(p0: DataSnapshot) {
            if (p0.exists())
              EatNUSAPI.getUserRef(AppPreferencesHelper().getUsername()!!).child("androidDeviceToken").setValue(
                FirebaseInstanceId.getInstance().token!!
              )
            else {
              val user = Customer(
                AppPreferencesHelper().getUsername()!!,
                name,
                FirebaseInstanceId.getInstance().token!!,
                0,
                1
              )
              EatNUSAPI.getUserRef(AppPreferencesHelper().getUsername()!!).setValue(user)
            }
            redirect(activity)
          }
        })
      }
    } else {
      mAuth.signInWithEmailAndPassword(name, password).addOnCompleteListener { task ->
        if (!task.isSuccessful) {
          onCredentialsError()
        } else {
          val userRef = EatNUSAPI.getUserRef(AppPreferencesHelper().getUsername()!!)
          userRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
              if (p0.exists())
                EatNUSAPI.getUserRef(AppPreferencesHelper().getUsername()!!).child("androidDeviceToken").setValue(
                  FirebaseInstanceId.getInstance().token!!
                )
              else {
                val user = User(
                  AppPreferencesHelper().getUsername()!!,
                  name,
                  FirebaseInstanceId.getInstance().token!!
                )
                EatNUSAPI.getUserRef(AppPreferencesHelper().getUsername()!!).setValue(user)
              }
              redirect(activity)
            }
          })
        }
      }
    }
  }

  /**
   * Redirect user to main page.
   */
  override fun redirect(activity: AppCompatActivity) {
    loadDialog.stop()
    val customerHomePageActivity = Intent(activity, HomeActivity::class.java)
    activity.startActivity(customerHomePageActivity)
    activity.finish()
  }
}