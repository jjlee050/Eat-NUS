package com.eatnus.ui.login.contract.listener

//OnLoginFinishListener.kt
/**
 * Listener interface to handle login business logic.
 *
 * @author Joseph Lee
 */
interface OnLoginFinishListener {
    /**
     * Pass to view to show error message when username is empty.
     */
    fun onEmptyUsernameError()
    /**
     * Pass to view to show error message when password is empty.
     */
    fun onEmptyPasswordError()
    /**
     * Pass to view to show error message when user enter incorrect prefix.
     */
    fun onPrefixError()
    /**
     * Pass to view to show error message when there is no internet connection.
     */
    fun onNetworkError()
    /**
     * Pass to view to show error message when user enter incorrect credentials.
     */
    fun onCredentialsError()
    /**
     * Pass to view to show message when user login successfully.
     */
    fun onSuccess()
}