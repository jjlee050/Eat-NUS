package com.eatnus.ui.login.contract

//LoginViewContract.kt
/**
 * Contract for login vendor view class.
 *
 * @author Joseph Lee
 */
interface LoginViewContract {
    /**
     * Centralized method to show login error in a toast.
     */
    fun showLoginError(msg: String)
    /**
     * Redirect user to the customer main page..
     */
    fun redirectUserToMainPage()
}