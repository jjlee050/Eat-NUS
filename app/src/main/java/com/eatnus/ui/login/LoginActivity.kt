package com.eatnus.ui.login

import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.eatnus.R
import com.eatnus.ui.login.contract.LoginPresenterContract
import com.eatnus.ui.login.contract.LoginViewContract
import com.eatnus.ui.login.presenter.LoginPresenter
import com.eatnus.utils.ui.MyProgressDialog
import com.eatnus.utils.helper.AppPreferencesHelper
import com.eatnus.utils.ui.MyAlertDialog
import kotlinx.android.synthetic.main.activity_login.*

//LoginActivity.kt
/**
 * View Class to handle login vendor activity page.
 *
 * @author Joseph Lee
 */
class LoginActivity : AppCompatActivity(), LoginViewContract, View.OnClickListener {
    private lateinit var loginPresenter: LoginPresenterContract
    private lateinit var loadDialog: MyProgressDialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        loadDialog = MyProgressDialog(this@LoginActivity, findViewById(R.id.newton_cradle_loading))

        loginPresenter = LoginPresenter(this, loadDialog)
        loginButton.setOnClickListener(this)
    }

    override fun onStart() {
            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val name = "Eat@NUS"
                val description = "Test"
                val importance = NotificationManager.IMPORTANCE_DEFAULT
                val channel = NotificationChannel("0", name, importance)
                channel.description = description
                // Register the channel with the system; you can't change the importance
                // or other notification behaviors after this
                val notificationManager = getSystemService(NotificationManager::class.java)
                notificationManager.createNotificationChannel(channel)
            }

        super.onStart()
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.loginButton -> {
                loadDialog.start()
                loginPresenter.login(usernameEditText.text.toString(), passwordEditText.text.toString())
            }
        }
    }

    /**
     * Centralized method to show login error in a toast.
     */
    override fun showLoginError(msg: String) {
        loadDialog.stop()
        MyAlertDialog().createSimpleErrorDialog(this, "Error", msg)
    }

    /**
     * Redirect user to the customer main page..
     */
    override fun redirectUserToMainPage() {
        val name = usernameEditText.text.toString()
        val username = name.substring(name.indexOf("\\") + 1)
        AppPreferencesHelper().setUsername(username)
        loginPresenter.saveDetailsIntoFirebase(this@LoginActivity, "$username@u.nus.edu", passwordEditText.text.toString())
    }
}