package com.eatnus.utils.ui

import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.WindowManager
import com.eatnus.R
import com.victor.loading.newton.NewtonCradleLoading

class MyProgressDialog {
    private var progressBarLoader: NewtonCradleLoading
    private lateinit var activity: AppCompatActivity
    private lateinit var fragment: Fragment

    constructor(activity: AppCompatActivity, loader: NewtonCradleLoading) {
        this.activity = activity
        this.progressBarLoader = loader
        progressBarLoader.setBackgroundColor(activity.resources.getColor(R.color.colorLoadingScreen))
        progressBarLoader.alpha = 0.6f
        progressBarLoader.background.alpha = 0
        progressBarLoader.bringToFront()
        progressBarLoader.setLoadingColor(activity.resources.getColor(R.color.colorPrimary))
    }

    fun start() {
        if (activity != null) {
            activity.window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
        progressBarLoader.start()
        progressBarLoader.visibility = View.VISIBLE
    }

    fun stop() {
        if (activity != null) {
            activity.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
        progressBarLoader.stop()
        progressBarLoader.visibility = View.GONE
    }
}