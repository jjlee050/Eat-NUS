package com.eatnus.utils.ui

import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity


class MyAlertDialog {
    fun createSimpleErrorDialog(activity: AppCompatActivity, title: String, message: String){
        if(!activity.isFinishing) {
            val alertDialog = AlertDialog.Builder(activity).create()
            alertDialog.setTitle(title)
            alertDialog.setMessage(message)
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "OK") { dialog, which ->
                dialog.dismiss()
            }
            alertDialog.show()
        }
    }

    fun createSimpleErrorDialog(fragment: Fragment, title: String, message: String){
        if(!fragment.isRemoving) {
            val alertDialog = AlertDialog.Builder(fragment.context!!).create()
            alertDialog.setTitle(title)
            alertDialog.setMessage(message)
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "OK") { dialog, which ->
                dialog.dismiss()
            }
            alertDialog.show()
        }
    }
}