package com.eatnus.utils.ui

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent

class MyViewPager: ViewPager {
    private var swipeable = false

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    // Call this method in your motion events when you want to disable or enable
    // It should work as desired.
    fun setSwipeable(swipeable: Boolean) {
        this.swipeable = swipeable;
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        return swipeable && super.onInterceptTouchEvent(ev)
    }

    override fun onTouchEvent(ev: MotionEvent?): Boolean {
        return swipeable && super.onTouchEvent(ev)
    }
}