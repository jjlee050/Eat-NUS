package com.eatnus.utils.helper

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.eatnus.App
import com.eatnus.R

//AppPreferencesHelper.kt
/**
 * Helper class to access shared preferences easily.
 *
 * @author Joseph Lee
 */
class AppPreferencesHelper: PreferencesHelper {
    private val mPrefs: SharedPreferences = App.context!!.getSharedPreferences("preferences", Context.MODE_PRIVATE)
    private val PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN"
    private val PREF_KEY_USERNAME = "PREF_KEY_USERNAME"
    private val PREF_KEY_USER_ROLE = "PREF_KEY_USER_ROLE"

    /**
     * Retrieve ivle access token in SharedPreferences.
     *
     * @return ivle access token inside shared preferences.
     */
    override fun getAccessToken(): String? {
        return mPrefs.getString(PREF_KEY_ACCESS_TOKEN, "")
    }

    override fun getUsername(): String? {
        return mPrefs.getString(PREF_KEY_USERNAME, "")
    }

    override fun getUserRole(): String? {
        return mPrefs.getString(PREF_KEY_USER_ROLE, "")
    }

    /**
     * Set ivle access token inside SharedPreferences.
     *
     * @return outcome of putting token inside shared preferences.
     */
    override fun setAccessToken(accessToken: String): Boolean {
        return mPrefs.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).commit()
    }

    override fun setUsername(username: String): Boolean {
        return mPrefs.edit().putString(PREF_KEY_USERNAME, username).commit()
    }

    override fun setUserRole(userRole: String): Boolean {
        return mPrefs.edit().putString(PREF_KEY_USER_ROLE, userRole).commit()
    }

    override fun clearPreferences(): Boolean {
        return mPrefs.edit().clear().commit()
    }
}

