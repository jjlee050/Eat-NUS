package com.eatnus.utils.helper

import android.content.Context
import android.database.sqlite.SQLiteOpenHelper
import android.database.sqlite.SQLiteDatabase
import android.content.ContentValues
import com.eatnus.model.FoodBasket
import java.util.*


class MySQLiteHelper(context: Context) : SQLiteOpenHelper(context, "EatNUSDB", null, 1) {

    companion object {
        val FOOD_BASKET = "food_basket"
    }

    override fun onCreate(db: SQLiteDatabase) {
        // SQL statement to create book table
        val CREATE_FOODBASKET_TABLE = "CREATE TABLE "+FOOD_BASKET+" ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "foodID INTEGER, " +
                "qty INTEGER)"

        // create books table
        db.execSQL(CREATE_FOODBASKET_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // Drop older books table if existed
        db.execSQL("DROP TABLE IF EXISTS " + FOOD_BASKET + ")")

        // create fresh books table
        this.onCreate(db)
    }
}