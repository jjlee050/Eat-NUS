package com.eatnus.utils.helper

//PreferencesHelper.kt
/**
 * Interface for AppPreferencesHelper.
 *
 * @author Joseph Lee
 */
interface PreferencesHelper {

    fun getAccessToken(): String?
    fun getUsername(): String?
    fun getUserRole(): String?

    fun setAccessToken(accessToken: String): Boolean?
    fun setUsername(username: String): Boolean?
    fun setUserRole(userRole: String): Boolean?

    fun clearPreferences(): Boolean
}
