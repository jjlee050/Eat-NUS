package com.eatnus.utils

enum class OrderStatus(val status: String) {
    //Starting
    ORDER_CONFIRMED("Order Confirmed"),
    ORDER_PAID("Order Paid"),
    ORDER_SENT("Order Sent"),
    //Processing
    ORDER_PROCESSING("Order Processing"),
    //Rejected
    ORDER_REJECTED("Order Rejected"),
    //Done
    ORDER_READY("Order Ready"),
    //Order Collected
    ORDER_COLLECTED("Order Collected"),
    //Acknowledged
    ORDER_ACKNOWLEDGED("Order Acknowledged"),
    //Either one of the status below is the endpoint
    ORDER_REFUNDED("Order Refunded"),
    ORDER_SUCCESSFUL("Order Successful")
}