package com.eatnus.utils.notification

import android.annotation.SuppressLint
import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.res.Resources
import android.graphics.BitmapFactory
import android.support.v4.app.Fragment
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.support.v4.content.ContextCompat
import android.view.View
import com.eatnus.R.*

class EatNUSNotificationManager : MyNotificationManager {

  override fun createNotification(
    context: Context,
    channelID: String,
    title: String,
    text: String,
    subText: String,
    isAutoCancel: Boolean,
    isOnGoing: Boolean
  ): NotificationCompat.Builder {
    return NotificationCompat.Builder(context!!, channelID)
      .setContentTitle(title)
      .setContentText(text)
      .setSubText(subText)
      .setPriority(NotificationCompat.PRIORITY_HIGH)
      .setAutoCancel(isAutoCancel)
      .setOngoing(isOnGoing)
      .setSmallIcon(mipmap.eatnus_launcher)
      .setColor(mipmap.eatnus_launcher)
      .setLargeIcon(BitmapFactory.decodeResource(context.resources, mipmap.eatnus_launcher))
  }

  override fun createNotification(
    context: Context,
    channelID: String,
    title: String,
    text: String,
    subText: String,
    pendingIntent: PendingIntent,
    isAutoCancel: Boolean,
    isOnGoing: Boolean
  ): NotificationCompat.Builder {
    return NotificationCompat.Builder(context!!, channelID)
      .setContentTitle(title)
      .setContentText(text)
      .setSubText(subText)
      .setPriority(NotificationCompat.PRIORITY_HIGH)
      .setContentIntent(pendingIntent)
      .setAutoCancel(isAutoCancel)
      .setOngoing(isOnGoing)
      .setSmallIcon(mipmap.eatnus_launcher)
      .setColor(mipmap.eatnus_launcher)
      .setLargeIcon(BitmapFactory.decodeResource(context.resources, mipmap.eatnus_launcher))
  }

  override fun postNotifications(context: Context, orderID: Int, notification: Notification) {
    NotificationManagerCompat.from(context).notify(orderID, notification)
  }
}
