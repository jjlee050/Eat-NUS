package com.eatnus.utils.notification

import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.NotificationCompat
import android.view.View

interface MyNotificationManager {
  fun createNotification(
    context: Context,
    channelID: String,
    title: String,
    text: String,
    subText: String,
    isAutoCancel: Boolean,
    isOnGoing: Boolean
  ): NotificationCompat.Builder

  fun createNotification(
    context: Context,
    channelID: String,
    title: String,
    text: String,
    subText: String,
    pendingIntent: PendingIntent,
    isAutoCancel: Boolean,
    isOnGoing: Boolean
  ): NotificationCompat.Builder

  fun postNotifications(context: Context, orderID: Int, notification: Notification)
}