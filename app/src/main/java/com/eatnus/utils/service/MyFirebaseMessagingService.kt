package com.eatnus.utils.service

import android.util.Log
import com.eatnus.model.User
import com.eatnus.utils.api.EatNUSAPI
import com.eatnus.utils.helper.AppPreferencesHelper
import com.eatnus.utils.notification.EatNUSNotificationManager
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class MyFirebaseMessagingService : FirebaseMessagingService() {
  private var TAG = "MyFirebaseMessagingService"

  override fun onNewToken(newToken: String?) {
    super.onNewToken(newToken)
    Log.d(TAG, newToken)
    if (!AppPreferencesHelper().getUsername().isNullOrEmpty()) {
      val userRef = EatNUSAPI.getUserRef(AppPreferencesHelper().getUsername()!!)
      userRef.addListenerForSingleValueEvent(object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) {
          Log.e(TAG, "No user found.")
        }

        override fun onDataChange(p0: DataSnapshot) {
          if (p0.exists())
            EatNUSAPI.getUserRef(AppPreferencesHelper().getUsername()!!).child("androidDeviceToken").setValue(
              FirebaseInstanceId.getInstance().token!!
            )
        }
      })
    }
  }

  override fun onMessageReceived(remoteMessage: RemoteMessage) {

    Log.d(TAG, "FROM : " + remoteMessage.from)

    //Verify if the message contains data
    if (remoteMessage.data.isNotEmpty()) {
      Log.d(TAG, "Message data : " + remoteMessage.data)
    }

    //Verify if the message contains notification
    if (remoteMessage.notification != null) {
      Log.d(TAG, "Message title : " + remoteMessage.notification!!.title)
      Log.d(TAG, "Message body : " + remoteMessage.notification!!.body)
      sendNotification(remoteMessage.notification!!.title, remoteMessage.notification!!.body)
    }
  }

  private fun sendNotification(title: String?, body: String?) {
    val orderID = title?.substring(title.indexOf('#') + 1, title.trim().indexOf('i'))
    val notification =
      EatNUSNotificationManager().createNotification(this, "0", title!!, body!!, "", true, false)
    EatNUSNotificationManager().postNotifications(
      this,
      orderID!!.trim().toInt(),
      notification.build()
    )
  }
}