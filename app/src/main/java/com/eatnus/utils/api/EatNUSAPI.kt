package com.eatnus.utils.api

import com.eatnus.App
import com.eatnus.utils.helper.AppPreferencesHelper
import com.google.gson.JsonObject
import com.koushikdutta.ion.Ion
import android.accounts.NetworkErrorException
import com.eatnus.BuildConfig
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.TaskCompletionSource
import com.google.firebase.database.*
import javax.net.ssl.HttpsURLConnection
import java.io.*
import java.net.*
import android.content.pm.ApplicationInfo


//EatNUSAPI.kt
/**
 * To store information relating to EatNUSAPI.
 *
 * @author Joseph Lee
 */
class EatNUSAPI {
  companion object {

    private lateinit var self: EatNUSAPI
    private const val IvleApiKey = BuildConfig.IvleKey
    const val dbsClientId = "6be143e2-79d3-49f6-8165-89daaafe8c6d"
    private var ivleAuthToken = AppPreferencesHelper().getAccessToken()
    private val ivleLogin = "https://ivle.nus.edu.sg/api/login/?apikey=$IvleApiKey"
    private var ivleValidate =
      "https://ivle.nus.edu.sg/api/Lapi.svc/Validate?APIKey=$IvleApiKey&Token=$ivleAuthToken"

    var Debug = true
    val StripePublishableKey: String =
      when {
        (Debug) -> BuildConfig.StripePublishTestKey
        else -> BuildConfig.StripePublishLiveKey
      }
    val StripeSecretKey: String =
      when {
        (Debug) -> BuildConfig.StripeSecretTestKey
        else -> BuildConfig.StripeSecretLiveKey
      }

    /**
     * Configures app given authentication token.
     */
    private fun authenticate(ivleAuthToken: String) {
      updateToken(ivleAuthToken)
      self = EatNUSAPI()
      println("token $ivleAuthToken")
    }

    /**
     * Ensures variables using authToken are updated from SharedPrefs after receiving token
     * this takes care of the null pointer issue when app is relaunched after successful login.
     */
    fun updateToken(token: String) {
      ivleAuthToken = token
      ivleValidate =
          "https://ivle.nus.edu.sg/api/Lapi.svc/Validate?APIKey=$IvleApiKey&Token=$ivleAuthToken"
    }

    /**
     * To validate current token and update token if new token received.
     *
     * @return string based on validation result for action to be taken accordingly.
     */
    fun validate(): String {

      var result: JsonObject? = null
      try {
        result = Ion.with(App.context)
          .load(ivleValidate)
          //.setLogging("MyLogs", Log.VERBOSE)
          .asJsonObject()
          .get()
      } catch (ex: Exception) {
        println("Error " + ex.toString())
      }

      return when {
        result == null -> //no object returned
          "Error"
        result.get("Success").toString() == "true" -> {

          val helper = AppPreferencesHelper()

          if (result.get("Token").asString != helper.getAccessToken()) {
            helper.setAccessToken(result.get("Token").asString)
            updateToken(helper.getAccessToken()!!)
          }
          authenticate(helper.getAccessToken()!!)
          "Successful"
        }
        else -> //validate unsuccessful
          "Fail"
      }
    }

    fun login(username: String, password: String): String? {
      // Set up the CookieManager.
      val cm = CookieManager()
      CookieHandler.setDefault(cm)
      // Set up the url.
      try {
        var query: String =
          "userid=" + URLEncoder.encode(username, "UTF-8") + "&password=" + URLEncoder.encode(
            password,
            "UTF-8"
          )
        query += "&__VIEWSTATE=/wEPDwULLTEzODMyMDQxNjEPFgIeE1ZhbGlkYXRlUmVxdWVzdE1vZGUCARYCAgEPZBYEAgEPD2QWAh4Gb25ibHVyBQ91c2VySWRUb1VwcGVyKClkAgkPD2QWBB4Lb25tb3VzZW92ZXIFNWRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdsb2dpbmltZzEnKS5zcmM9b2ZmaW1nLnNyYzE7Hgpvbm1vdXNlb3V0BTRkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbG9naW5pbWcxJykuc3JjPW9uaW1nLnNyYzE7ZBgBBR5fX0NvbnRyb2xzUmVxdWlyZVBvc3RCYWNrS2V5X18WAQUJbG9naW5pbWcxYTg4Q/LO3lNCB13iJpTeINmF1JQmGv61ni1TVgDIOII="

        try {
          var url = URL(ivleLogin)
          var conn: HttpsURLConnection = url.openConnection() as HttpsURLConnection
          conn.instanceFollowRedirects = false

          // We want a POST request.
          conn.requestMethod = "POST"
          conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
          conn.doInput = true
          conn.doOutput = true

          // Send request.
          val out = DataOutputStream(conn.outputStream)
          out.writeBytes(query)
          out.flush()
          out.close()

          // Read response.
          val redirect = conn.getHeaderField("Location") ?: throw Exception()

          // Follow the redirect.
          url = URL("https://ivle.nus.edu.sg$redirect")
          conn = url.openConnection() as HttpsURLConnection
          conn.instanceFollowRedirects = false

          // A GET request will suffice this time.
          conn.doInput = true
          conn.doOutput = false

          // Read response.
          var authToken = ""
          val inReader = BufferedReader(InputStreamReader(conn.inputStream))
          for (buffer in inReader.readLine()) {
            authToken += buffer
          }

          inReader.close()
          conn.inputStream.close()
          conn.disconnect()

          val helper = AppPreferencesHelper()
          helper.setAccessToken(authToken.trim())
          EatNUSAPI.authenticate(authToken.trim())
          return authToken.trim()
        } catch (mue: MalformedURLException) {
          throw IllegalArgumentException("Invalid apiKey. ")
        } catch (ioe: IOException) {
          throw NetworkErrorException()

        }
      } catch (uee: UnsupportedEncodingException) {
        throw IllegalArgumentException("Invalid username or password. ")
      }
    }

    fun getAmenitie(): DatabaseReference {
      return FirebaseDatabase.getInstance().reference.child("amenities")
    }

    fun getAmenities(): Task<DataSnapshot> {
      val dbSource: TaskCompletionSource<DataSnapshot> = TaskCompletionSource()
      FirebaseDatabase.getInstance().reference.child("amenities")
        .addListenerForSingleValueEvent(object : ValueEventListener {
          override fun onCancelled(databaseError: DatabaseError) {
            dbSource.setException(databaseError.toException())
          }

          override fun onDataChange(dataSnapShot: DataSnapshot) {
            dbSource.setResult(dataSnapShot)
          }
        })
      return dbSource.task
    }

    fun getAmenity(amenitiesID: String): Task<DataSnapshot> {
      val dbSource: TaskCompletionSource<DataSnapshot> = TaskCompletionSource()
      FirebaseDatabase.getInstance().reference.child("amenities").child(amenitiesID)
        .addListenerForSingleValueEvent(object : ValueEventListener {
          override fun onCancelled(databaseError: DatabaseError) {
            dbSource.setException(databaseError.toException())
          }

          override fun onDataChange(dataSnapShot: DataSnapshot) {
            dbSource.setResult(dataSnapShot)
          }
        })
      return dbSource.task
    }

    fun getAmenityRef(amenitiesID: String): DatabaseReference {
      return FirebaseDatabase.getInstance().getReference("stall").child(amenitiesID)
    }

    fun getOrdere(): DatabaseReference {
      return FirebaseDatabase.getInstance().reference.child("orders")
    }

    fun getOrders(): Task<DataSnapshot> {
      val dbSource: TaskCompletionSource<DataSnapshot> = TaskCompletionSource()
      FirebaseDatabase.getInstance().reference.child("orders").orderByKey()
        .addListenerForSingleValueEvent(object : ValueEventListener {
          override fun onCancelled(databaseError: DatabaseError) {
            dbSource.setException(databaseError.toException())
          }

          override fun onDataChange(dataSnapShot: DataSnapshot) {
            dbSource.setResult(dataSnapShot)
          }
        })
      return dbSource.task
    }

    fun getOrder(orderID: String): Task<DataSnapshot> {
      val dbSource: TaskCompletionSource<DataSnapshot> = TaskCompletionSource()
      FirebaseDatabase.getInstance().reference.child("orders").child(orderID)
        .addListenerForSingleValueEvent(object : ValueEventListener {
          override fun onCancelled(databaseError: DatabaseError) {
            dbSource.setException(databaseError.toException())
          }

          override fun onDataChange(dataSnapShot: DataSnapshot) {
            dbSource.setResult(dataSnapShot)
          }
        })
      return dbSource.task
    }

    fun getOrderRef(orderID: String): DatabaseReference {
      return FirebaseDatabase.getInstance().reference.child("orders").child(orderID)
    }

    fun getFoodBasketsRef(): DatabaseReference {
      return FirebaseDatabase.getInstance().reference.child("food_basket")
    }

    fun getFoodBaskets(): Task<DataSnapshot> {
      val dbSource: TaskCompletionSource<DataSnapshot> = TaskCompletionSource()
      FirebaseDatabase.getInstance().reference.child("food_basket")
        .addListenerForSingleValueEvent(object : ValueEventListener {
          override fun onCancelled(databaseError: DatabaseError) {
            dbSource.setException(databaseError.toException())
          }

          override fun onDataChange(dataSnapShot: DataSnapshot) {
            dbSource.setResult(dataSnapShot)
          }
        })
      return dbSource.task
    }

    fun getFoodBasket(foodBasketID: String): Task<DataSnapshot> {
      val dbSource: TaskCompletionSource<DataSnapshot> = TaskCompletionSource()
      FirebaseDatabase.getInstance().reference.child("food_basket").child(foodBasketID)
        .addListenerForSingleValueEvent(object : ValueEventListener {
          override fun onCancelled(databaseError: DatabaseError) {
            dbSource.setException(databaseError.toException())
          }

          override fun onDataChange(dataSnapShot: DataSnapshot) {
            dbSource.setResult(dataSnapShot)
          }
        })
      return dbSource.task
    }

    fun getFoodBasketRef(foodBasketID: String): DatabaseReference {
      return FirebaseDatabase.getInstance().reference.child("food_basket").child(foodBasketID)
    }

    fun getStallsRef(): DatabaseReference {
      return FirebaseDatabase.getInstance().reference.child("stall")
    }

    fun getStalls(): Task<DataSnapshot> {
      val dbSource: TaskCompletionSource<DataSnapshot> = TaskCompletionSource()
      FirebaseDatabase.getInstance().reference.child("stall")
        .addListenerForSingleValueEvent(object : ValueEventListener {
          override fun onCancelled(databaseError: DatabaseError) {
            dbSource.setException(databaseError.toException())
          }

          override fun onDataChange(dataSnapShot: DataSnapshot) {
            dbSource.setResult(dataSnapShot)
          }
        })
      return dbSource.task
    }

    fun getStall(stallID: String): Task<DataSnapshot> {
      val dbSource: TaskCompletionSource<DataSnapshot> = TaskCompletionSource()
      FirebaseDatabase.getInstance().reference.child("stall").child(stallID)
        .addListenerForSingleValueEvent(object : ValueEventListener {
          override fun onCancelled(databaseError: DatabaseError) {
            dbSource.setException(databaseError.toException())
          }

          override fun onDataChange(dataSnapShot: DataSnapshot) {
            dbSource.setResult(dataSnapShot)
          }
        })
      return dbSource.task
    }

    fun getStallRef(stallID: String): DatabaseReference {
      return FirebaseDatabase.getInstance().getReference("stall").child(stallID)
    }

    fun getFoode(): DatabaseReference {
      return FirebaseDatabase.getInstance().reference.child("food")
    }

    fun getFoods(): Task<DataSnapshot> {
      val dbSource: TaskCompletionSource<DataSnapshot> = TaskCompletionSource()
      FirebaseDatabase.getInstance().reference.child("food")
        .addListenerForSingleValueEvent(object : ValueEventListener {
          override fun onCancelled(databaseError: DatabaseError) {
            dbSource.setException(databaseError.toException())
          }

          override fun onDataChange(dataSnapShot: DataSnapshot) {
            dbSource.setResult(dataSnapShot)
          }
        })
      return dbSource.task
    }

    fun getFood(foodID: String): Task<DataSnapshot> {
      val dbSource: TaskCompletionSource<DataSnapshot> = TaskCompletionSource()
      FirebaseDatabase.getInstance().reference.child("food").child(foodID)
        .addListenerForSingleValueEvent(object : ValueEventListener {
          override fun onCancelled(databaseError: DatabaseError) {
            dbSource.setException(databaseError.toException())
          }

          override fun onDataChange(dataSnapShot: DataSnapshot) {
            dbSource.setResult(dataSnapShot)
          }
        })
      return dbSource.task
    }

    fun getFoodRef(foodID: String): DatabaseReference {
      return FirebaseDatabase.getInstance().reference.child("food").child(foodID)
    }

    fun getUser(username: String): Task<DataSnapshot> {
      val dbSource: TaskCompletionSource<DataSnapshot> = TaskCompletionSource()
      FirebaseDatabase.getInstance().reference.child("user").child(username)
        .addListenerForSingleValueEvent(object : ValueEventListener {
          override fun onCancelled(databaseError: DatabaseError) {
            dbSource.setException(databaseError.toException())
          }

          override fun onDataChange(dataSnapShot: DataSnapshot) {
            dbSource.setResult(dataSnapShot)
          }
        })
      return dbSource.task
    }

    fun getUsersRef(): DatabaseReference {
      return FirebaseDatabase.getInstance().reference.child("user")
    }

    fun getUserRef(username: String): DatabaseReference {
      return getUsersRef().child(username)
    }

    fun getLevels(): Task<DataSnapshot> {
      val dbSource: TaskCompletionSource<DataSnapshot> = TaskCompletionSource()
      FirebaseDatabase.getInstance().reference.child("levels")
        .addListenerForSingleValueEvent(object : ValueEventListener {
          override fun onCancelled(databaseError: DatabaseError) {
            dbSource.setException(databaseError.toException())
          }

          override fun onDataChange(dataSnapShot: DataSnapshot) {
            dbSource.setResult(dataSnapShot)
          }
        })
      return dbSource.task
    }

    fun getLevel(level: String): Task<DataSnapshot> {
      val dbSource: TaskCompletionSource<DataSnapshot> = TaskCompletionSource()
      FirebaseDatabase.getInstance().reference.child("levels").child(level)
        .addListenerForSingleValueEvent(object : ValueEventListener {
          override fun onCancelled(databaseError: DatabaseError) {
            dbSource.setException(databaseError.toException())
          }

          override fun onDataChange(dataSnapShot: DataSnapshot) {
            dbSource.setResult(dataSnapShot)
          }
        })
      return dbSource.task
    }

    fun getCoupons(): Task<DataSnapshot> {
      val dbSource: TaskCompletionSource<DataSnapshot> = TaskCompletionSource()
      FirebaseDatabase.getInstance().reference.child("coupons")
        .addListenerForSingleValueEvent(object : ValueEventListener {
          override fun onCancelled(databaseError: DatabaseError) {
            dbSource.setException(databaseError.toException())
          }

          override fun onDataChange(dataSnapShot: DataSnapshot) {
            dbSource.setResult(dataSnapShot)
          }
        })
      return dbSource.task
    }

    fun getCoupon(couponID: String): Task<DataSnapshot> {
      val dbSource: TaskCompletionSource<DataSnapshot> = TaskCompletionSource()
      FirebaseDatabase.getInstance().reference.child("coupons").child(couponID)
        .addListenerForSingleValueEvent(object : ValueEventListener {
          override fun onCancelled(databaseError: DatabaseError) {
            dbSource.setException(databaseError.toException())
          }

          override fun onDataChange(dataSnapShot: DataSnapshot) {
            dbSource.setResult(dataSnapShot)
          }
        })
      return dbSource.task
    }

    fun getFeedbacks(): Task<DataSnapshot> {
      val dbSource: TaskCompletionSource<DataSnapshot> = TaskCompletionSource()
      FirebaseDatabase.getInstance().reference.child("feedbacks")
        .addListenerForSingleValueEvent(object : ValueEventListener {
          override fun onCancelled(databaseError: DatabaseError) {
            dbSource.setException(databaseError.toException())
          }

          override fun onDataChange(dataSnapShot: DataSnapshot) {
            dbSource.setResult(dataSnapShot)
          }
        })
      return dbSource.task
    }

    fun getFeedbacksRef(): DatabaseReference {
      return FirebaseDatabase.getInstance().reference.child("feedbacks")
    }
  }
}
