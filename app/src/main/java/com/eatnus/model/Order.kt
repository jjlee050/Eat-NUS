package com.eatnus.model

import android.os.Parcelable
import com.eatnus.utils.OrderStatus
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Order (
  var orderID: Int = 0,
  var stallID: Int = 0,
  var username: String = "",
  var foodBasketID: Int = 0,
  var details: String = "",
  var timeOfCollection: String = "",
  var isLaterCollection: Boolean = false,
  var isTakeaway: Boolean = false,
  var dateOfConfirmation: String = "",
  var collectedTime: String = "",
  var orderReadyTime: String = "",
  var status: OrderStatus = OrderStatus.ORDER_CONFIRMED,
  var isCash: Boolean = false,
  var dbsRefID: String = "",
  var googlePayOrderID: String = "",
  var rejectReasons: String = "",
  var takeawayFee: Float = 0.0f
  //Takeaway fee
): Parcelable