package com.eatnus.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FoodBasket(
        var foodBasketID: Int = 0,
        var foodID: Int = 0,
        var qty: Int = 0,
        //Food Price
        var price: Float = 0.0f
): Parcelable