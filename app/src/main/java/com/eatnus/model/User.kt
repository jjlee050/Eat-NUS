package com.eatnus.model

open class User(
        open var username: String = "",
        open var email: String = "",
        open var androidDeviceToken: String = ""
)