package com.eatnus.model

data class Amenities (
        var name: String = "",
        var loc: String = "",
        var id: Int = 0
)