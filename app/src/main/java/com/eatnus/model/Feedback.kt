package com.eatnus.model

data class Feedback(
  var feedbackID: Int = 0,
  val username: String = "",
  val title: String = "",
  val desc: String = "",
  val sentFeedbackDate: String = ""
)