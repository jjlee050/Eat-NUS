package com.eatnus.model

data class Customer(
  override var username: String = "",
  override var email: String = "",
  override var androidDeviceToken: String = "",
  var currentPts: Int = 0,
  var currentLevel: Int = 1
): User(username, email, androidDeviceToken)