package com.eatnus.model

data class Coupon (
  var id: Int = 0,
  var title: String = "",
  var desc: String = "",
  var ptsRequired: Int = 0,
  var expiryDate: String = ""
)