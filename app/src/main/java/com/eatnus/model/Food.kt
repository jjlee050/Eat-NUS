package com.eatnus.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Food (
    var id: Int = 0,
    var stallID: Int = 0,
    var name: String = "",
    var desc: String = "",
    var price: Float = 0.0f,
    //May change
    var img: String = "",
    var totalQty: Int = 0
): Parcelable
