package com.eatnus.model

data class Stall (
        var id: Int = 0,
        var amenitiesId: Int = 0,
        var name: String = "",
        var username: String = "",
        var phone: String = "",
        var takeAwayPrice: Float = 0.0f,
        var allowCash: Boolean = false,
        //Use arraylist
        var inactivityPeriodList: List<String> = ArrayList<String>(),
        var operatingHrStart: String = "",
        var operatingHrEnd: String = ""
)