package com.eatnus

import android.app.Application
import android.content.Context
import android.content.pm.ApplicationInfo
import com.eatnus.utils.api.EatNUSAPI
import com.google.firebase.database.FirebaseDatabase

//App.kt
/**
 * The main application for the android project.
 *
 * @author Joseph Lee
 */
class App : Application() {

  override fun onCreate() {
    super.onCreate()
    App.context = this
    EatNUSAPI.Debug = applicationInfo.flags and
        ApplicationInfo.FLAG_DEBUGGABLE !== 0;
    //FirebaseDatabase.getInstance().setPersistenceEnabled(true)
  }

  companion object {

    var context: Context? = null
      private set
  }

}